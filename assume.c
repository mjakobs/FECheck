//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2022 Maik Wiesner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
* implementation of VERIFIER_assume as suggested
* by rules of SV-COMP.
* For example found under: https://sv-comp.sosy-lab.org/2020/rules.php
*/
void __VERIFIER_assume(int expression) {
	if (!expression) {
	LOOP: goto LOOP;
	};
	return;
}