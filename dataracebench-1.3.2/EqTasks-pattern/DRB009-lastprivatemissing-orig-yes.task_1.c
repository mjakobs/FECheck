#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int len;
  int x;
  int i;
  int x_s;
  int i_s;
//#pragma omp parallel for private (i) 
  x_s = __VERIFIER_nondet_int();
  x = x_s;
  len = __VERIFIER_nondet_int();
// (First) sequential code segment
  for (i_s = 0; i_s < len; i_s++) {
    x_s = i_s;
  }
  
#pragma omp parallel for private (i)
  for (i = 0; i < len; i++) {
    x = i;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  return 0;
}
