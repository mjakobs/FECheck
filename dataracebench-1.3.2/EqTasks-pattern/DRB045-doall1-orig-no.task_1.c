#include <assert.h> 
extern int __VERIFIER_nondet_int();
int a[100];
int a_s[100];

int main()
{
  int i;
  int i_s;
//#pragma omp parallel for
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    a_s[_f_ct0] = __VERIFIER_nondet_int();
    a[_f_ct0] = a_s[_f_ct0];
  }
// (First) sequential code segment
  for (i_s = 0; i_s < 100; i_s++) {
    a_s[i_s] = a_s[i_s] + 1;
  }
  
#pragma omp parallel for
  for (i = 0; i < 100; i++) {
    a[i] = a[i] + 1;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    equal = equal && a_s[_f_ct0] == a[_f_ct0];
  }
  assert(equal);
  return 0;
}
