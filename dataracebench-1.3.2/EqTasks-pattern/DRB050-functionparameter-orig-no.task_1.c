#include <assert.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();

int main()
{
  int _size_st1_1 = __VERIFIER_nondet_int();
  int _size_st0_1 = __VERIFIER_nondet_int();
  int i;
  int len;
  double c[_size_st1_1];
  double o1[_size_st0_1];
  int i_s;
  double o1_s[_size_st0_1];
//#pragma omp parallel for
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    o1_s[_f_ct0] = __VERIFIER_nondet_double();
    o1[_f_ct0] = o1_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1_1; _f_ct0++) {
    c[_f_ct0] = __VERIFIER_nondet_double();
  }
  len = __VERIFIER_nondet_int();
// (First) sequential code segment
  for (i_s = 0; i_s < len; ++i_s) {
    double volnew_o8 = 0.5 * c[i_s];
    o1_s[i_s] = volnew_o8;
  }
  
#pragma omp parallel for
  for (i = 0; i < len; ++i) {
    double volnew_o8 = 0.5 * c[i];
    o1[i] = volnew_o8;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    equal = equal && o1_s[_f_ct0] == o1[_f_ct0];
  }
  assert(equal);
  return 0;
}
