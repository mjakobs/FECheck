#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern int __VERIFIER_nondet_int();
int _size_st0_1;

int main()
{
  int len;
  len = __VERIFIER_nondet_int();
  int x;
  int i;
  int a[len];
  _size_st0_1 = len;
  int x_s;
  int i_s;
  int a_s[len];
//#pragma omp parallel for 
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    a_s[_f_ct0] = __VERIFIER_nondet_int();
    a[_f_ct0] = a_s[_f_ct0];
  }
  x_s = __VERIFIER_nondet_int();
  x = x_s;
// (First) sequential code segment
  for (i_s = 0; i_s < len; i_s++) {
    a_s[i_s] = x_s;
    x_s = i_s;
  }
  
#pragma omp parallel for
  for (i = 0; i < len; i++) {
    a[i] = x;
    x = i;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    equal = equal && a_s[_f_ct0] == a[_f_ct0];
  }
  assert(equal);
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  return 0;
}
