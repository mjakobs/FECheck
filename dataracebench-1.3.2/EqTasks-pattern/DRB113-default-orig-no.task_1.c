#include <assert.h> 
extern int __VERIFIER_nondet_int();
int a[100][100];
int a_s[100][100];

int main()
{
  int j;
  int i;
  int j_s;
  int i_s;
//#pragma omp parallel for default(none) shared(a) private(i,j)
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 100; _f_ct1++) {
      a_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_int();
      a[_f_ct0][_f_ct1] = a_s[_f_ct0][_f_ct1];
    }
  }
  j_s = __VERIFIER_nondet_int();
  j = j_s;
// (First) sequential code segment
  for (i_s = 0; i_s < 100; i_s++) {
    for (j_s = 0; j_s < 100; j_s++) {
      a_s[i_s][j_s] = a_s[i_s][j_s] + 1;
    }
  }
  
#pragma omp parallel for default(none) shared(a) private(i,j)
  for (i = 0; i < 100; i++) {
    for (j = 0; j < 100; j++) {
      a[i][j] = a[i][j] + 1;
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 100; _f_ct1++) {
      equal = equal && a_s[_f_ct0][_f_ct1] == a[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
