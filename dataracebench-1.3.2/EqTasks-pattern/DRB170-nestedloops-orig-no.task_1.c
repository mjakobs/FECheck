#include <assert.h> 
#include <stdio.h> 
#include <omp.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();

int main()
{
  double a[12][12][12];
  double tmp1;
  int m;
  int k;
  int j;
  int i;
  double a_s[12][12][12];
  double tmp1_s;
  int k_s;
  int j_s;
  int i_s;
//#pragma omp parallel for private(j,k,tmp1)   
  j_s = __VERIFIER_nondet_int();
  j = j_s;
  k_s = __VERIFIER_nondet_int();
  k = k_s;
  m = __VERIFIER_nondet_int();
  tmp1_s = __VERIFIER_nondet_double();
  tmp1 = tmp1_s;
  for (int _f_ct0 = 0; _f_ct0 < 12; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 12; _f_ct1++) {
      for (int _f_ct2 = 0; _f_ct2 < 12; _f_ct2++) {
        a_s[_f_ct0][_f_ct1][_f_ct2] = __VERIFIER_nondet_double();
        a[_f_ct0][_f_ct1][_f_ct2] = a_s[_f_ct0][_f_ct1][_f_ct2];
      }
    }
  }
// (First) sequential code segment
  for (i_s = 0; i_s < 12; i_s++) {
    for (j_s = 0; j_s < 12; j_s++) {
      for (k_s = 0; k_s < 12; k_s++) {
        tmp1_s = 6.0 / ((double )m);
        a_s[i_s][j_s][k_s] = tmp1_s + ((double )4);
      }
    }
  }
  
#pragma omp parallel for private(j,k,tmp1)
  for (i = 0; i < 12; i++) {
    for (j = 0; j < 12; j++) {
      for (k = 0; k < 12; k++) {
        tmp1 = 6.0 / ((double )m);
        a[i][j][k] = tmp1 + ((double )4);
      }
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 12; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 12; _f_ct1++) {
      for (int _f_ct2 = 0; _f_ct2 < 12; _f_ct2++) {
        equal = equal && a_s[_f_ct0][_f_ct1][_f_ct2] == a[_f_ct0][_f_ct1][_f_ct2];
      }
    }
  }
  assert(equal);
  return 0;
}
