#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
int _size_st1_1;
int _size_st0_1;

int main()
{
  int inLen;
  inLen = __VERIFIER_nondet_int();
  int output[inLen];
  _size_st1_1 = inLen;
  int input[inLen];
  _size_st0_1 = inLen;
  int outLen;
  int i;
  int output_s[inLen];
  int outLen_s;
  int i_s;
//#pragma omp parallel for
  outLen_s = __VERIFIER_nondet_int();
  outLen = outLen_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    input[_f_ct0] = __VERIFIER_nondet_int();
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1_1; _f_ct0++) {
    output_s[_f_ct0] = __VERIFIER_nondet_int();
    output[_f_ct0] = output_s[_f_ct0];
  }
// (First) sequential code segment
  for (i_s = 0; i_s < inLen; ++i_s) {
    output_s[outLen_s++] = input[i_s];
  }
  
#pragma omp parallel for
  for (i = 0; i < inLen; ++i) {
    output[outLen++] = input[i];
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1_1; _f_ct0++) {
    equal = equal && output_s[_f_ct0] == output[_f_ct0];
  }
  assert(equal);
  return 0;
}
