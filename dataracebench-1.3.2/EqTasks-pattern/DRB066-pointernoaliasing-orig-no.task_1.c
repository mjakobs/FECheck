#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int _size_st1;
int _size_st0;

int main()
{
  double *m_nvol;
  _size_st1 = __VERIFIER_nondet_int();
  m_nvol = ((double *)(malloc(_size_st1 * sizeof(double ))));
  double *m_pdv_sum;
  _size_st0 = __VERIFIER_nondet_int();
  m_pdv_sum = ((double *)(malloc(_size_st0 * sizeof(double ))));
  int N;
  double *m_nvol_s;
  m_nvol_s = ((double *)(malloc(_size_st1 * sizeof(double ))));
  double *m_pdv_sum_s;
  m_pdv_sum_s = ((double *)(malloc(_size_st0 * sizeof(double ))));
//#pragma omp parallel for schedule(static)
  N = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    m_pdv_sum_s[_f_ct0] = __VERIFIER_nondet_double();
    m_pdv_sum[_f_ct0] = m_pdv_sum_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    m_nvol_s[_f_ct0] = __VERIFIER_nondet_double();
    m_nvol[_f_ct0] = m_nvol_s[_f_ct0];
  }
// (First) sequential code segment
  for (int i = 0; i < N; ++i) {
    m_pdv_sum_s[i] = 0.0;
    m_nvol_s[i] = ((double )i) * 2.5;
  }
  
#pragma omp parallel for schedule(static)
  for (int i = 0; i < N; ++i) {
    m_pdv_sum[i] = 0.0;
    m_nvol[i] = ((double )i) * 2.5;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    equal = equal && m_pdv_sum_s[_f_ct0] == m_pdv_sum[_f_ct0];
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    equal = equal && m_nvol_s[_f_ct0] == m_nvol[_f_ct0];
  }
  assert(equal);
{
    free(m_pdv_sum_s);
    free(m_nvol_s);
    free(m_pdv_sum);
    free(m_nvol);
  }
  return 0;
}
