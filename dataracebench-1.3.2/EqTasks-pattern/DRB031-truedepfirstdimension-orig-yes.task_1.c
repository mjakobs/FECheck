#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();

int main()
{
  double b[1000][1000];
  int m;
  int n;
  int j;
  int i;
  double b_s[1000][1000];
  int j_s;
  int i_s;
//#pragma omp parallel for private(j)
  j_s = __VERIFIER_nondet_int();
  j = j_s;
  n = __VERIFIER_nondet_int();
  m = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < 1000; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 1000; _f_ct1++) {
      b_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      b[_f_ct0][_f_ct1] = b_s[_f_ct0][_f_ct1];
    }
  }
// (First) sequential code segment
  for (i_s = 1; i_s < n; i_s++) {
    for (j_s = 1; j_s < m; j_s++) {
      b_s[i_s][j_s] = b_s[i_s - 1][j_s - 1];
    }
  }
  
#pragma omp parallel for private(j)
  for (i = 1; i < n; i++) {
    for (j = 1; j < m; j++) {
      b[i][j] = b[i - 1][j - 1];
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 1000; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 1000; _f_ct1++) {
      equal = equal && b_s[_f_ct0][_f_ct1] == b[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
