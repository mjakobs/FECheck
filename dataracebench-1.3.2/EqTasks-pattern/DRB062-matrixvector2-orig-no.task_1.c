#include <assert.h> 
extern float __VERIFIER_nondet_float();
extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();
double v[1000];
double a[1000][1000];

int main()
{
  float sum;
  int j;
  int i;
  float sum_s;
  int j_s;
//#pragma omp parallel for reduction(+:sum)
  for (int _f_ct0 = 0; _f_ct0 < 1000; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 1000; _f_ct1++) {
      a[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < 1000; _f_ct0++) {
    v[_f_ct0] = __VERIFIER_nondet_double();
  }
  i = __VERIFIER_nondet_int();
  j_s = __VERIFIER_nondet_int();
  j = j_s;
  sum_s = __VERIFIER_nondet_float();
  sum = sum_s;
// (First) sequential code segment
  for (j_s = 0; j_s < 1000; j_s++) {
    sum_s += a[i][j_s] * v[j_s];
  }
  
#pragma omp parallel for reduction(+:sum)
  for (j = 0; j < 1000; j++) {
    sum += a[i][j] * v[j];
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}
