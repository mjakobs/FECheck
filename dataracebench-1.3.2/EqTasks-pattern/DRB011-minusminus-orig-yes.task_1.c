#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int x[100];
  int numNodes2;
  int numNodes;
  int i;
  int numNodes2_s;
  int i_s;
//#pragma omp parallel for
  numNodes = __VERIFIER_nondet_int();
  numNodes2_s = __VERIFIER_nondet_int();
  numNodes2 = numNodes2_s;
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    x[_f_ct0] = __VERIFIER_nondet_int();
  }
// (First) sequential code segment
  for (i_s = numNodes - 1; i_s > - 1; --i_s) {
    if (x[i_s] <= 0) {
      numNodes2_s--;
    }
  }
  
#pragma omp parallel for
  for (i = numNodes - 1; i > - 1; --i) {
    if (x[i] <= 0) {
      numNodes2--;
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && numNodes2_s == numNodes2;
  assert(equal);
  return 0;
}
