#include <assert.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int _size_st2_1;
int _size_st1_1;
int _size_st0_1;

int main()
{
  int len;
  len = __VERIFIER_nondet_int();
  int j;
  int i;
  double c[len];
  _size_st2_1 = len;
  double b[len];
  _size_st1_1 = len;
  double a[len];
  _size_st0_1 = len;
  int j_s;
  int i_s;
  double c_s[len];
//#pragma omp parallel for linear(j)
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    a[_f_ct0] = __VERIFIER_nondet_double();
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1_1; _f_ct0++) {
    b[_f_ct0] = __VERIFIER_nondet_double();
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st2_1; _f_ct0++) {
    c_s[_f_ct0] = __VERIFIER_nondet_double();
    c[_f_ct0] = c_s[_f_ct0];
  }
  j_s = __VERIFIER_nondet_int();
  j = j_s;
// (First) sequential code segment
  for (i_s = 0; i_s < len; i_s++) {
    c_s[j_s] += a[i_s] * b[i_s];
    j_s++;
  }
  
#pragma omp parallel for linear(j)
  for (i = 0; i < len; i++) {
    c[j] += a[i] * b[i];
    j++;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2_1; _f_ct0++) {
    equal = equal && c_s[_f_ct0] == c[_f_ct0];
  }
  assert(equal);
  return 0;
}
