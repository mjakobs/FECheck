#include <assert.h> 
#include <stdio.h> 
#include <omp.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();

int main()
{
  double r[8][8][8];
  double r1[8];
  int k;
  int j;
  int i;
  double r1_s[8];
  int k_s;
  int j_s;
  int i_s;
//#pragma omp parallel for default(shared) private(j,k)
  j_s = __VERIFIER_nondet_int();
  j = j_s;
  k_s = __VERIFIER_nondet_int();
  k = k_s;
  for (int _f_ct0 = 0; _f_ct0 < 8; _f_ct0++) {
    r1_s[_f_ct0] = __VERIFIER_nondet_double();
    r1[_f_ct0] = r1_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < 8; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 8; _f_ct1++) {
      for (int _f_ct2 = 0; _f_ct2 < 8; _f_ct2++) {
        r[_f_ct0][_f_ct1][_f_ct2] = __VERIFIER_nondet_double();
      }
    }
  }
// (First) sequential code segment
  for (i_s = 1; i_s < 8 - 1; i_s++) {
    for (j_s = 1; j_s < 8 - 1; j_s++) {
      for (k_s = 0; k_s < 8; k_s++) {
        r1_s[k_s] = r[i_s][j_s - 1][k_s] + r[i_s][j_s + 1][k_s] + r[i_s - 1][j_s][k_s] + r[i_s + 1][j_s][k_s];
      }
    }
  }
  
#pragma omp parallel for default(shared) private(j,k)
  for (i = 1; i < 8 - 1; i++) {
    for (j = 1; j < 8 - 1; j++) {
      for (k = 0; k < 8; k++) {
        r1[k] = r[i][j - 1][k] + r[i][j + 1][k] + r[i - 1][j][k] + r[i + 1][j][k];
      }
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 8; _f_ct0++) {
    equal = equal && r1_s[_f_ct0] == r1[_f_ct0];
  }
  assert(equal);
  return 0;
}
