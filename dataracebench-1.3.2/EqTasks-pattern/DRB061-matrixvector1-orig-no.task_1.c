#include <assert.h> 
extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();
double v_out[100];
double v[100];
double a[100][100];
double v_out_s[100];

int main()
{
  int j;
  int i;
  int j_s;
  int i_s;
//#pragma omp parallel for private (i,j)
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 100; _f_ct1++) {
      a[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    v[_f_ct0] = __VERIFIER_nondet_double();
  }
  j_s = __VERIFIER_nondet_int();
  j = j_s;
// (First) sequential code segment
  for (i_s = 0; i_s < 100; i_s++) {
    float sum = (float )0.0;
    for (j_s = 0; j_s < 100; j_s++) {
      sum += a[i_s][j_s] * v[j_s];
    }
    v_out_s[i_s] = ((double )sum);
  }
  
#pragma omp parallel for private (i,j)
  for (i = 0; i < 100; i++) {
    float sum = (float )0.0;
    for (j = 0; j < 100; j++) {
      sum += a[i][j] * v[j];
    }
    v_out[i] = ((double )sum);
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    equal = equal && v_out_s[_f_ct0] == v_out[_f_ct0];
  }
  assert(equal);
  return 0;
}
