#include <assert.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();

int main()
{
  double a[20][20];
  int len;
  int j;
  int i;
  double a_s[20][20];
  int j_s;
  int i_s;
//#pragma omp parallel for private(j)
  j_s = __VERIFIER_nondet_int();
  j = j_s;
  len = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < 20; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 20; _f_ct1++) {
      a_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      a[_f_ct0][_f_ct1] = a_s[_f_ct0][_f_ct1];
    }
  }
// (First) sequential code segment
  for (i_s = 0; i_s < len - 1; i_s += 1) {
    for (j_s = 0; j_s < len; j_s += 1) {
      a_s[i_s][j_s] += a_s[i_s + 1][j_s];
    }
  }
  
#pragma omp parallel for private(j)
  for (i = 0; i < len - 1; i += 1) {
    for (j = 0; j < len; j += 1) {
      a[i][j] += a[i + 1][j];
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 20; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 20; _f_ct1++) {
      equal = equal && a_s[_f_ct0][_f_ct1] == a[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
