#include <assert.h> 
#include <stdlib.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int _size_st0_2;
int _size_st0_1;

int main()
{
  int len;
  len = __VERIFIER_nondet_int();
  double a[len][len];
  _size_st0_2 = len;
  _size_st0_1 = len;
  int j;
  int i;
  double a_s[len][len];
  int j_s;
  int i_s;
//#pragma omp parallel for private(j)
  j_s = __VERIFIER_nondet_int();
  j = j_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st0_2; _f_ct1++) {
      a_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      a[_f_ct0][_f_ct1] = a_s[_f_ct0][_f_ct1];
    }
  }
// (First) sequential code segment
  for (i_s = 0; i_s < len - 1; i_s += 1) {
    for (j_s = 0; j_s < len; j_s += 1) {
      a_s[i_s][j_s] += a_s[i_s + 1][j_s];
    }
  }
  
#pragma omp parallel for private(j)
  for (i = 0; i < len - 1; i += 1) {
    for (j = 0; j < len; j += 1) {
      a[i][j] += a[i + 1][j];
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st0_2; _f_ct1++) {
      equal = equal && a_s[_f_ct0][_f_ct1] == a[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
