#include <assert.h> 
extern int __VERIFIER_nondet_int();
int b[100][100];
int b_s[100][100];

int main()
{
  int j;
  int i;
  int j_s;
  int i_s;
//#pragma omp parallel for default(shared) private(i,j)
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 100; _f_ct1++) {
      b_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_int();
      b[_f_ct0][_f_ct1] = b_s[_f_ct0][_f_ct1];
    }
  }
  j_s = __VERIFIER_nondet_int();
  j = j_s;
// (First) sequential code segment
  for (i_s = 0; i_s < 100; i_s++) {
    for (j_s = 0; j_s < 100; j_s++) {
      b_s[i_s][j_s] = b_s[i_s][j_s] + 1;
    }
  }
  
#pragma omp parallel for default(shared) private(i,j)
  for (i = 0; i < 100; i++) {
    for (j = 0; j < 100; j++) {
      b[i][j] = b[i][j] + 1;
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 100; _f_ct1++) {
      equal = equal && b_s[_f_ct0][_f_ct1] == b[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
