#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
int _size_st2;
int _size_st1;
int _size_st0;

int main()
{
  int i;
  int * __restrict__ c;
  _size_st2 = __VERIFIER_nondet_int();
  c = ((int * __restrict__ )(malloc(_size_st2 * sizeof(int ))));
  int * __restrict__ b;
  _size_st1 = __VERIFIER_nondet_int();
  b = ((int * __restrict__ )(malloc(_size_st1 * sizeof(int ))));
  int * __restrict__ a;
  _size_st0 = __VERIFIER_nondet_int();
  a = ((int * __restrict__ )(malloc(_size_st0 * sizeof(int ))));
  int n;
  int i_s;
  int * __restrict__ a_s;
  a_s = ((int * __restrict__ )(malloc(_size_st0 * sizeof(int ))));
//#pragma omp parallel for 
  n = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    a_s[_f_ct0] = __VERIFIER_nondet_int();
    a[_f_ct0] = a_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    b[_f_ct0] = __VERIFIER_nondet_int();
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    c[_f_ct0] = __VERIFIER_nondet_int();
  }
// (First) sequential code segment
  for (i_s = 0; i_s < n; i_s++) {
    a_s[i_s] = b[i_s] + c[i_s];
  }
  
#pragma omp parallel for
  for (i = 0; i < n; i++) {
    a[i] = b[i] + c[i];
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    equal = equal && a_s[_f_ct0] == a[_f_ct0];
  }
  assert(equal);
{
    free(a_s);
    free(a);
    free(b);
    free(c);
  }
  return 0;
}
