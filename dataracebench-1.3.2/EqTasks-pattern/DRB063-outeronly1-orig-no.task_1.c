#include <assert.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
double b[100][100];
int m;
int n;
double b_s[100][100];

int main()
{
  int j;
  int i;
  int j_s;
  int i_s;
//#pragma omp parallel for private(j)
  n = __VERIFIER_nondet_int();
  m = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 100; _f_ct1++) {
      b_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      b[_f_ct0][_f_ct1] = b_s[_f_ct0][_f_ct1];
    }
  }
  j_s = __VERIFIER_nondet_int();
  j = j_s;
// (First) sequential code segment
  for (i_s = 0; i_s < n; i_s++) {
    for (j_s = 0; j_s < m - 1; j_s++) 
// Be careful about bounds of j
{
      b_s[i_s][j_s] = b_s[i_s][j_s + 1];
    }
  }
  
#pragma omp parallel for private(j)
  for (i = 0; i < n; i++) {
    for (j = 0; j < m - 1; j++) 
// Be careful about bounds of j
{
      b[i][j] = b[i][j + 1];
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 100; _f_ct1++) {
      equal = equal && b_s[_f_ct0][_f_ct1] == b[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
