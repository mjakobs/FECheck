#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();
double b[1000][1000];
double b_s[1000][1000];

int main()
{
  int m;
  int j;
  int i;
  int j_s;
//#pragma omp parallel for
  for (int _f_ct0 = 0; _f_ct0 < 1000; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 1000; _f_ct1++) {
      b_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      b[_f_ct0][_f_ct1] = b_s[_f_ct0][_f_ct1];
    }
  }
  i = __VERIFIER_nondet_int();
  j_s = __VERIFIER_nondet_int();
  j = j_s;
  m = __VERIFIER_nondet_int();
// (First) sequential code segment
  for (j_s = 1; j_s < m; j_s++) {
    b_s[i][j_s] = b_s[i][j_s - 1];
  }
  
#pragma omp parallel for
  for (j = 1; j < m; j++) {
    b[i][j] = b[i][j - 1];
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 1000; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 1000; _f_ct1++) {
      equal = equal && b_s[_f_ct0][_f_ct1] == b[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
