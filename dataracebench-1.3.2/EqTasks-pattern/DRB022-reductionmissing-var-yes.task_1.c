#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern float __VERIFIER_nondet_float();
extern int __VERIFIER_nondet_int();
int _size_st0_2;
int _size_st0_1;

int main()
{
  int len;
  len = __VERIFIER_nondet_int();
  float u[len][len];
  _size_st0_2 = len;
  _size_st0_1 = len;
  float sum;
  float temp;
  int j;
  int i;
  float sum_s;
  float temp_s;
  int j_s;
  int i_s;
//#pragma omp parallel for private (temp,i,j)
  j_s = __VERIFIER_nondet_int();
  j = j_s;
  temp_s = __VERIFIER_nondet_float();
  temp = temp_s;
  sum_s = __VERIFIER_nondet_float();
  sum = sum_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st0_2; _f_ct1++) {
      u[_f_ct0][_f_ct1] = __VERIFIER_nondet_float();
    }
  }
// (First) sequential code segment
  for (i_s = 0; i_s < len; i_s++) {
    for (j_s = 0; j_s < len; j_s++) {
      temp_s = u[i_s][j_s];
      sum_s = sum_s + temp_s * temp_s;
    }
  }
  
#pragma omp parallel for private (temp,i,j)
  for (i = 0; i < len; i++) {
    for (j = 0; j < len; j++) {
      temp = u[i][j];
      sum = sum + temp * temp;
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}
