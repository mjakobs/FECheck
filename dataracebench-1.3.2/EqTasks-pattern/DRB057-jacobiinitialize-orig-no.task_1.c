#include <assert.h> 
#include <stdio.h> 
#include <math.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
double dy;
double dx;
double f[200][200];
double u[200][200];
double alpha;
int m;
int n;
double f_s[200][200];
double u_s[200][200];

int main()
{
  int yy;
  int xx;
  int j;
  int i;
  int yy_s;
  int xx_s;
  int j_s;
  int i_s;
//#pragma omp parallel for private(i,j,xx,yy)
  n = __VERIFIER_nondet_int();
  m = __VERIFIER_nondet_int();
  alpha = __VERIFIER_nondet_double();
  for (int _f_ct0 = 0; _f_ct0 < 200; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 200; _f_ct1++) {
      u_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      u[_f_ct0][_f_ct1] = u_s[_f_ct0][_f_ct1];
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < 200; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 200; _f_ct1++) {
      f_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      f[_f_ct0][_f_ct1] = f_s[_f_ct0][_f_ct1];
    }
  }
  dx = __VERIFIER_nondet_double();
  dy = __VERIFIER_nondet_double();
  j_s = __VERIFIER_nondet_int();
  j = j_s;
  xx_s = __VERIFIER_nondet_int();
  xx = xx_s;
  yy_s = __VERIFIER_nondet_int();
  yy = yy_s;
// (First) sequential code segment
  for (i_s = 0; i_s < n; i_s++) {
    for (j_s = 0; j_s < m; j_s++) {
      xx_s = ((int )(- 1.0 + dx * ((double )(i_s - 1))));
/* -1 < x < 1 */
      yy_s = ((int )(- 1.0 + dy * ((double )(j_s - 1))));
/* -1 < y < 1 */
      u_s[i_s][j_s] = 0.0;
      f_s[i_s][j_s] = - 1.0 * alpha * (1.0 - ((double )(xx_s * xx_s))) * (1.0 - ((double )(yy_s * yy_s))) - 2.0 * (1.0 - ((double )(xx_s * xx_s))) - 2.0 * (1.0 - ((double )(yy_s * yy_s)));
    }
  }
  
#pragma omp parallel for private(i,j,xx,yy)
  for (i = 0; i < n; i++) {
    for (j = 0; j < m; j++) {
      xx = ((int )(- 1.0 + dx * ((double )(i - 1))));
/* -1 < x < 1 */
      yy = ((int )(- 1.0 + dy * ((double )(j - 1))));
/* -1 < y < 1 */
      u[i][j] = 0.0;
      f[i][j] = - 1.0 * alpha * (1.0 - ((double )(xx * xx))) * (1.0 - ((double )(yy * yy))) - 2.0 * (1.0 - ((double )(xx * xx))) - 2.0 * (1.0 - ((double )(yy * yy)));
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 200; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 200; _f_ct1++) {
      equal = equal && u_s[_f_ct0][_f_ct1] == u[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 200; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 200; _f_ct1++) {
      equal = equal && f_s[_f_ct0][_f_ct1] == f[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
