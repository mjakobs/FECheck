#include <assert.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();

int main()
{
  double interval_width;
  double x;
  long i;
  double pi;
  double x_s;
  long i_s;
  double pi_s;
//#pragma omp parallel for reduction(+:pi) private(x)
  pi_s = __VERIFIER_nondet_double();
  pi = pi_s;
  x_s = __VERIFIER_nondet_double();
  x = x_s;
  interval_width = __VERIFIER_nondet_double();
// (First) sequential code segment
  for (i_s = ((long )0); i_s < ((long )2000000000); i_s++) {
    x_s = (((double )i_s) + 0.5) * interval_width;
    pi_s += 1.0 / (x_s * x_s + 1.0);
  }
  
#pragma omp parallel for reduction(+:pi) private(x)
  for (i = ((long )0); i < ((long )2000000000); i++) {
    x = (((double )i) + 0.5) * interval_width;
    pi += 1.0 / (x * x + 1.0);
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && pi_s == pi;
  assert(equal);
  return 0;
}
