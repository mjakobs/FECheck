#include <assert.h> 
extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();
double c[100][100];
double b[100][100];
double a[100][100];
double c_s[100][100];

int main()
{
  int k;
  int j;
  int i;
  int k_s;
  int j_s;
  int i_s;
//#pragma omp parallel for private(j,k)
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 100; _f_ct1++) {
      a[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 100; _f_ct1++) {
      b[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 100; _f_ct1++) {
      c_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      c[_f_ct0][_f_ct1] = c_s[_f_ct0][_f_ct1];
    }
  }
  j_s = __VERIFIER_nondet_int();
  j = j_s;
  k_s = __VERIFIER_nondet_int();
  k = k_s;
// (First) sequential code segment
  for (i_s = 0; i_s < 100; i_s++) {
    for (k_s = 0; k_s < 100; k_s++) {
      for (j_s = 0; j_s < 100; j_s++) {
        c_s[i_s][j_s] = c_s[i_s][j_s] + a[i_s][k_s] * b[k_s][j_s];
      }
    }
  }
  
#pragma omp parallel for private(j,k)
  for (i = 0; i < 100; i++) {
    for (k = 0; k < 100; k++) {
      for (j = 0; j < 100; j++) {
        c[i][j] = c[i][j] + a[i][k] * b[k][j];
      }
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 100; _f_ct1++) {
      equal = equal && c_s[_f_ct0][_f_ct1] == c[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
