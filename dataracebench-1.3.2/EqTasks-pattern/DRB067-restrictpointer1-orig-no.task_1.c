/*
Copyright (c) 2017, Lawrence Livermore National Security, LLC.
Produced at the Lawrence Livermore National Laboratory
Written by Chunhua Liao, Pei-Hung Lin, Joshua Asplund,
Markus Schordan, and Ian Karlin
(email: liao6@llnl.gov, lin32@llnl.gov, asplund1@llnl.gov,
schordan1@llnl.gov, karlin1@llnl.gov)
LLNL-CODE-732144
All rights reserved.
This file is part of DataRaceBench. For details, see
https://github.com/LLNL/dataracebench. Please also see the LICENSE-DRB file
for our additional BSD notice.
Redistribution and use in source and binary forms, with
or without modification, are permitted provided that the following
conditions are met:
* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the disclaimer below.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the disclaimer (as noted below)
  in the documentation and/or other materials provided with the
  distribution.
* Neither the name of the LLNS/LLNL nor the names of its contributors
  may be used to endorse or promote products derived from this
  software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL LAWRENCE LIVERMORE NATIONAL
SECURITY, LLC, THE U.S. DEPARTMENT OF ENERGY OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.
*/
/* 
Modifications for equivalence checking:
- Renamed to represent sequential file. Get original file name by replacing seq.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
- Comment OMP pragmas
*/
/*
restrict pointers: no aliasing
Array initialization using assignments. 
C99 is needed to compile this code
e.g. gcc -std=c99 -c Stress-1.c
*/
#include <stdlib.h>
#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
typedef double real8;
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int _size_st1;
int _size_st0;

int main()
{
  int i;
  int length;
  real8 * __restrict__ newSyy;
  _size_st1 = __VERIFIER_nondet_int();
  newSyy = ((real8 * __restrict__ )(malloc(_size_st1 * sizeof(real8 ))));
  real8 * __restrict__ newSxx;
  _size_st0 = __VERIFIER_nondet_int();
  newSxx = ((real8 * __restrict__ )(malloc(_size_st0 * sizeof(real8 ))));
  int i_s;
  real8 * __restrict__ newSyy_s;
  newSyy_s = ((real8 * __restrict__ )(malloc(_size_st1 * sizeof(real8 ))));
  real8 * __restrict__ newSxx_s;
  newSxx_s = ((real8 * __restrict__ )(malloc(_size_st0 * sizeof(real8 ))));
//#pragma omp parallel for private (i) firstprivate (length)
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    newSxx_s[_f_ct0] = __VERIFIER_nondet_double();
    newSxx[_f_ct0] = newSxx_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    newSyy_s[_f_ct0] = __VERIFIER_nondet_double();
    newSyy[_f_ct0] = newSyy_s[_f_ct0];
  }
  length = __VERIFIER_nondet_int();
// (First) sequential code segment
  for (i_s = 0; i_s <= length - 1; i_s += 1) {
    newSxx_s[i_s] = 0.0;
    newSyy_s[i_s] = 0.0;
  }
  
#pragma omp parallel for private (i) firstprivate (length)
  for (i = 0; i <= length - 1; i += 1) {
    newSxx[i] = 0.0;
    newSyy[i] = 0.0;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    equal = equal && newSxx_s[_f_ct0] == newSxx[_f_ct0];
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    equal = equal && newSyy_s[_f_ct0] == newSyy[_f_ct0];
  }
  assert(equal);
{
    free(newSxx_s);
    free(newSyy_s);
    free(newSxx);
    free(newSyy);
  }
  return 0;
}
