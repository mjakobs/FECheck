#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int a[100];
  int len;
  int tmp;
  int i;
  int a_s[100];
  int tmp_s;
  int i_s;
//#pragma omp parallel for
  tmp_s = __VERIFIER_nondet_int();
  tmp = tmp_s;
  len = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    a_s[_f_ct0] = __VERIFIER_nondet_int();
    a[_f_ct0] = a_s[_f_ct0];
  }
// (First) sequential code segment
  for (i_s = 0; i_s < len; i_s++) {
    tmp_s = a_s[i_s] + i_s;
    a_s[i_s] = tmp_s;
  }
  
#pragma omp parallel for
  for (i = 0; i < len; i++) {
    tmp = a[i] + i;
    a[i] = tmp;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    equal = equal && a_s[_f_ct0] == a[_f_ct0];
  }
  assert(equal);
  return 0;
}
