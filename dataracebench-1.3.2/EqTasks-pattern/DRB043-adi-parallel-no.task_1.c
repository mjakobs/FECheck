#include <assert.h> 
#include <stdio.h> 
#include <unistd.h> 
#include <string.h> 
#include <math.h> 
#include "../../polybench/polybench.h" 
#include "../../polybench/adi.h" 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();

int main()
{
  int c2;
  int c1;
  double B[500][500];
  double A[500][500];
  double X[500][500];
  int n;
  int c2_s;
  int c1_s;
  double B_s[500][500];
  double A_s[500][500];
  double X_s[500][500];
//#pragma omp parallel for private(c2)
  n = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      X_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      X[_f_ct0][_f_ct1] = X_s[_f_ct0][_f_ct1];
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      A_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      A[_f_ct0][_f_ct1] = A_s[_f_ct0][_f_ct1];
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      B_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      B[_f_ct0][_f_ct1] = B_s[_f_ct0][_f_ct1];
    }
  }
  c2_s = __VERIFIER_nondet_int();
  c2 = c2_s;
// (First) sequential code segment
  for (c1_s = 0; c1_s <= n + - 1; c1_s++) {
    for (c2_s = 0; c2_s <= n + - 1; c2_s++) {
      X_s[c1_s][c2_s] = (((double )c1_s) * ((double )(c2_s + 1)) + ((double )1)) / ((double )n);
      A_s[c1_s][c2_s] = (((double )c1_s) * ((double )(c2_s + 2)) + ((double )2)) / ((double )n);
      B_s[c1_s][c2_s] = (((double )c1_s) * ((double )(c2_s + 3)) + ((double )3)) / ((double )n);
    }
  }
  
#pragma omp parallel for private(c2)
  for (c1 = 0; c1 <= n + - 1; c1++) {
    for (c2 = 0; c2 <= n + - 1; c2++) {
      X[c1][c2] = (((double )c1) * ((double )(c2 + 1)) + ((double )1)) / ((double )n);
      A[c1][c2] = (((double )c1) * ((double )(c2 + 2)) + ((double )2)) / ((double )n);
      B[c1][c2] = (((double )c1) * ((double )(c2 + 3)) + ((double )3)) / ((double )n);
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      equal = equal && X_s[_f_ct0][_f_ct1] == X[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      equal = equal && A_s[_f_ct0][_f_ct1] == A[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      equal = equal && B_s[_f_ct0][_f_ct1] == B[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
