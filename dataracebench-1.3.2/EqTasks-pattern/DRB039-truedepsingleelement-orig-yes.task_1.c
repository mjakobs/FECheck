#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int a[1000];
  int i;
  int len;
  int a_s[1000];
  int i_s;
//#pragma omp parallel for
  len = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < 1000; _f_ct0++) {
    a_s[_f_ct0] = __VERIFIER_nondet_int();
    a[_f_ct0] = a_s[_f_ct0];
  }
// (First) sequential code segment
  for (i_s = 0; i_s < len; i_s++) {
    a_s[i_s] = a_s[i_s] + a_s[0];
  }
  
#pragma omp parallel for
  for (i = 0; i < len; i++) {
    a[i] = a[i] + a[0];
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 1000; _f_ct0++) {
    equal = equal && a_s[_f_ct0] == a[_f_ct0];
  }
  assert(equal);
  return 0;
}
