/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
*/

/*This example is referred from DRACC by Adrian Schmitz et al.
The distribute parallel for directive at line 24 will execute loop using multiple teams.
The loop iterations are distributed across the teams in chunks in round robin fashion.
The missing lock enclosing var@26:5 leads to data race. Data Race Pairs, var@26:5 and var@26:5.
*/
/* 
Modifications for equivalence checking:
- Renamed to represent sequential file. Get original file name by replacing seq.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
- Deleted OMP pragmas, etc.
*/
#include <omp.h>
#include <stdio.h>

int main(){

  int var=0,i;

  #pragma scope_1
  for (int i=0; i<100; i++){
    var++;
  }
  #pragma epocs_1
  
  printf("%d\n",var);
  return 0;
}
