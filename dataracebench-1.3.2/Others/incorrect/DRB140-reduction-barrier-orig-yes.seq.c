/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
 */


/* The assignment to a@25:7 is  not synchronized with the update of a@29:11 as a result of the
 * reduction computation in the for loop.
 * Data Race pair: a@25:5 and a@27:33
 * */
/* 
Modifications for equivalence checking:
- Renamed to represent sequential file. Get original file name by replacing seq.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
- Deleted OMP pragmas, etc.
*/
#include <stdio.h>
#include <omp.h>

int main(){
  int a, i;
  #pragma scope_1
  {
    a = 0;

    for (i=0; i<10; i++){
      a = a + i;
    }

    printf("Sum is %d\n", a);
  }
  #pragma epocs_1
  return 0;
}
