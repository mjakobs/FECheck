/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
 */

/*
The distribute parallel for directive at line 27 will execute loop using multiple teams.
The loop iterations are distributed across the teams in chunks in round robin fashion.
The omp lock is only guaranteed for a contention group, i.e, within a team. Data Race Pair, var@30:5 and var@30:5.
*/

/* 
Modifications for equivalence checking:
- Renamed to represent sequential file. Get original file name by replacing seq.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
- Deleted OMP pragmas, etc.
*/
#include <omp.h>
#include <stdio.h>

int main(){

  //omp_lock_t lck;
  int var=0,i;
  #pragma scope_1
  //omp_init_lock(&lck);

  for (int i=0; i<100; i++){
    //omp_set_lock(&lck);
    var++;
    //omp_unset_lock(&lck);
  }
  #pragma epocs_1
  
  //omp_destroy_lock(&lck);

  printf("%d\n",var);
  return 0;
}
