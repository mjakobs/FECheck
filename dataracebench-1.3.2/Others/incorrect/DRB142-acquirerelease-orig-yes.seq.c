/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
 */

/* The below program will fail to order the write to x on thread 0 before the read from x on thread 1.
 * The implicit release flush on exit from the critical region will not synchronize with the acquire
 * flush that occurs on the atomic read operation performed by thread 1. This is because implicit
 * release flushes that occur on a given construct may only synchronize with implicit acquire flushes
 * on a compatible construct (and vice-versa) that internally makes use of the same synchronization
 * variable.
 *
 * Implicit flush must be used after critical construct, after line:34 and before line:35 to avoid data race.
 * Data Race pair: x@34:9 and x@34:9
 * */

/* 
Modifications for equivalence checking:
- Renamed to represent sequential file. Get original file name by replacing seq.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
- Deleted OMP pragmas, etc.
*/
#include <stdio.h>
#include <omp.h>

int main(){

  int x = 0, y;
  #pragma scope_1

  {
    int thrd = omp_get_thread_num();
    if (thrd == 0) {

      { x = 10; }

      y = 1;
    } else {
      int tmp = 0;
      while (tmp == 0) {

        tmp = y;
    }

    { if (x!=10) printf("x = %d\n", x); }
    }
  }
   #pragma epocs_1 
  return 0;
}
