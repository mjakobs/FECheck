/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
 */

/*
 * Taken from OpenMP Examples 5.0, example tasking.12.c
 * The created task will access different instances of the variable x if the task is not merged,
 * as x is firstprivate, but it will access the same variable x if the task is merged. It can
 * print two different values for x depending on the decisions taken by the implementation.
 * Data Race Pairs, x@27:5 and x@27:5
 */
/* 
Modifications for equivalence checking:
- Renamed to represent sequential file. Get original file name by replacing seq.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
- Deleted OMP pragmas, etc.
*/
#include <omp.h>
#include <stdio.h>


int main(){
  int x = 2;
  #pragma scope_1
  {
    x++;
  }
  #pragma epocs_1
  
  printf("%d\n",x);
  return 0;
}
