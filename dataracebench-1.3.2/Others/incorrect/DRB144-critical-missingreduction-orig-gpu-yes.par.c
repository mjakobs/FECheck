/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
 */

/*
The increment at line number 26 is critical for the variable
var@26:5. Therefore, there is a possible Data Race pair var@26:5 and var@26:5.
*/
/* 
Modifications for equivalence checking:
- Renamed to represent parallelized file. Get original file name by replacing par.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
*/
#include <stdio.h>
#include <omp.h>
#define N 100

int var = 0;

int main(){
  #pragma scope_1
  #pragma omp target map(tofrom:var) device(0)
  #pragma omp teams distribute parallel for
  for(int i=0; i<N*2; i++){
    #pragma omp critical
    var++;
  }
  #pragma epocs_1
  printf("%d\n ",var);

  return 0;
}
