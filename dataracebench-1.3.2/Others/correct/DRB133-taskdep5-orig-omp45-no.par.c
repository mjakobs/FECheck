/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
 */

/* The first two tasks are serialized, because a dependence on the first child is produced
 * by x with the in dependence type in the depend clause of the second task. Generating task
 * at the first taskwait only waits for the first child task to complete. The second taskwait
 * guarantees completion of the second task before y is accessed. Therefore there is no race
 * condition.
 * */

/* 
Modifications for equivalence checking:
- Renamed to represent parallelized file. Get original file name by replacing par.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
*/
#include <stdio.h>
#include <omp.h>

void foo(){
  int x = 0, y = 2;

  #pragma omp task depend(inout: x) shared(x)
  x++;                                                                  // 1st child task

  #pragma omp task depend(in: x) depend(inout: y) shared(x, y)
  y = y-x;                                                              //2nd child task

  #pragma omp task depend(in: x) if(0)                                  // 1st taskwait
  {}

  printf("x=%d\n",x);

  #pragma omp taskwait                                                  // 2nd taskwait

  printf("y=%d\n",y);
}

int main(){
  #pragma scope_1
  #pragma omp parallel
  #pragma omp single
  foo();
#pragma epocs_1
  return 0;
}

