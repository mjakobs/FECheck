/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
 */

 /*
 * The scheduling constraints prohibit a thread in the team from executing
 * a new task that modifies tp while another such task region tied to
 * the same thread is suspended. Therefore, the value written will
 * persist across the task scheduling point.
 * No Data Race at var@35:7
 */
/*
Modifications for equivalence checking:
- Renamed to represent sequential file. Get original file name by replacing seq.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
- Comment OMP pragmas, etc.
*/

#include <omp.h>
#include <stdio.h>

int tp;
//#pragma omp threadprivate(tp)
int var;

int main(){
  #pragma scope_1
  //#pragma omp task
  {
    //#pragma omp task
    {
      tp = 1;
      //#pragma omp task
      {
      }
      var = tp;
    }
  }
  #pragma epocs_1
  return 0;
}
