/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
 */

/* The second taskwait ensures that the second child task has completed; hence it is safe to access
 * the y variable in the following print statement.
 * */

/*
Modifications for equivalence checking:
- Renamed to represent sequential file. Get original file name by replacing seq.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
- Comment OMP pragmas, etc.
*/
#include <stdio.h>
#include <omp.h>

void foo(){

  int x = 0, y = 2;

 // #pragma omp task depend(inout: x) shared(x)
  x++;                                                //1st Child Task

//  #pragma omp task shared(y)
  y--;                                                // 2nd child task

//  #pragma omp task depend(in: x) if(0)                // 1st taskwait
  {}

  printf("x=%d\n",x);

 // #pragma omp taskwait                                // 2nd taskwait

  printf("y=%d\n",y);
}


int main(){
  #pragma scope_1
//  #pragma omp parallel
//  #pragma omp single
  foo();
#pragma epocs_1
  return 0;
}

