/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
 */

/* This kernel imitates the nature of a program from the NAS Parallel Benchmarks 3.0 MG suit.
 * The private(i) inline 27 and explicit barrier inline 35 will ensure synchronized behavior.
 * No Data Race Pairs.
 */
/* 
Modifications for equivalence checking:
- Renamed to represent parallelized file. Get original file name by replacing par.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
*/
#include <omp.h>
#include <stdio.h>

int main(){
  int i;
  double q[10], qq[10];

  for (i = 0; i < 10; i++) qq[i] = (double)i;
  for (i = 0; i < 10; i++) q[i] = (double)i;

  #pragma scope_1
  #pragma omp parallel default(shared)
  {
    #pragma omp for private(i)
    for (i = 0; i < 10; i++)
    {
      q[i] += qq[i];
    }

    #pragma omp critical
    {
      q[9] += 1.0;
    }
    #pragma omp barrier
    #pragma omp single
    {
      q[9] = q[9] - 1.0;
    }

  } 
#pragma epocs_1
  for (i = 0; i < 10; i++)printf("%f %f\n",qq[i],q[i]);

  return 0;
}

