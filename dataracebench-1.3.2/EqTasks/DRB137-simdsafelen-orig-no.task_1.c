#include <assert.h> 
#include <stdio.h> 
#include <omp.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int b[4];
  int n;
  int m;
  int i;
  int b_s[4];
  int i_s;
// #pragma omp simd safelen(2)
  m = __VERIFIER_nondet_int();
  n = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < 4; _f_ct0++) {
    b_s[_f_ct0] = __VERIFIER_nondet_int();
    b[_f_ct0] = b_s[_f_ct0];
  }
// (First) sequential code segment
  for (i_s = m; i_s < n; i_s++) {
    b_s[i_s] = ((int )(((float )b_s[i_s - m]) - 1.0f));
  }
  
#pragma omp simd safelen(2)
  for (i = m; i < n; i++) {
    b[i] = ((int )(((float )b[i - m]) - 1.0f));
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 4; _f_ct0++) {
    equal = equal && b_s[_f_ct0] == b[_f_ct0];
  }
  assert(equal);
  return 0;
}
