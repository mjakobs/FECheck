#include <assert.h> 
#include <stdio.h> 
extern unsigned int __VERIFIER_nondet_uint();
int fib(unsigned int n);
int fib_s(unsigned int n);
unsigned int input;

int main()
{
  int result;
  int result_s;
//#pragma omp parallel
  input = __VERIFIER_nondet_uint();
// (First) sequential code segment
{
//  #pragma omp single
{
      result_s = fib_s(input);
    }
  }
  
#pragma omp parallel
{
    
#pragma omp single
{
      result = fib(input);
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && result_s == result;
  assert(equal);
  return 0;
}

int fib_s(unsigned int n)
{
{
    if (n < ((unsigned int )2)) {
      return (int )n;
    }
     else {
      int i;
      int j;
//#pragma omp task shared(i)
      i = fib_s(n - ((unsigned int )1));
//#pragma omp task shared(j)
      j = fib_s(n - ((unsigned int )2));
//#pragma omp taskwait
      return i + j;
    }
  }
}

int fib(unsigned int n)
{
{
    if (n < ((unsigned int )2)) {
      return (int )n;
    }
     else {
      int i;
      int j;
      
#pragma omp task shared(i)
      i = fib(n - ((unsigned int )1));
      
#pragma omp task shared(j)
      j = fib(n - ((unsigned int )2));
      
#pragma omp taskwait
      return i + j;
    }
  }
}
