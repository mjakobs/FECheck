#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int _size_st1;
int _size_st0;

int main()
{
  int i;
  int N;
  double *b;
  _size_st1 = __VERIFIER_nondet_int();
  b = ((double *)(malloc(_size_st1 * sizeof(double ))));
  double *a;
  _size_st0 = __VERIFIER_nondet_int();
  a = ((double *)(malloc(_size_st0 * sizeof(double ))));
  int i_s;
  double *b_s;
  b_s = ((double *)(malloc(_size_st1 * sizeof(double ))));
//#pragma omp target map(to:a[0:N]) map(from:b[0:N])
//#pragma omp parallel for
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    a[_f_ct0] = __VERIFIER_nondet_double();
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    b_s[_f_ct0] = __VERIFIER_nondet_double();
    b[_f_ct0] = b_s[_f_ct0];
  }
  N = __VERIFIER_nondet_int();
// (First) sequential code segment
  for (i_s = 0; i_s < N; i_s++) {
    b_s[i_s] = a[i_s] * ((double )i_s);
  }
  
#pragma omp target map(to:a[0:N]) map(from:b[0:N])
  
#pragma omp parallel for
  for (i = 0; i < N; i++) {
    b[i] = a[i] * ((double )i);
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    equal = equal && b_s[_f_ct0] == b[_f_ct0];
  }
  assert(equal);
{
    free(b_s);
    free(a);
    free(b);
  }
  return 0;
}
