#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int var[8];
  int var_s[8];
  for (int _f_ct0 = 0; _f_ct0 < 8; _f_ct0++) {
    var_s[_f_ct0] = __VERIFIER_nondet_int();
    var[_f_ct0] = var_s[_f_ct0];
  }
// (First) sequential code segment
  for (int i = 0; i < 20; i++) {
    for (int i = 0; i < 8; i++) {
      var_s[i]++;
    }
  }
  
#pragma omp target map(tofrom:var) device(0)
  
#pragma omp teams num_teams(1) thread_limit(1048)
  
#pragma omp distribute parallel for
  for (int i = 0; i < 20; i++) {
    
#pragma omp simd
    for (int i = 0; i < 8; i++) {
      var[i]++;
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 8; _f_ct0++) {
    equal = equal && var_s[_f_ct0] == var[_f_ct0];
  }
  assert(equal);
  return 0;
}
