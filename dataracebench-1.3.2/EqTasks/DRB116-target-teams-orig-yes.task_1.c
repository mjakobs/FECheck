#include <assert.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int _size_st0_1;

int main()
{
  int len;
  len = __VERIFIER_nondet_int();
  double a[len];
  _size_st0_1 = len;
  double a_s[len];
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    a_s[_f_ct0] = __VERIFIER_nondet_double();
    a[_f_ct0] = a_s[_f_ct0];
  }
// (First) sequential code segment
{
    a_s[50] *= 2.0;
  }
  
#pragma omp target map(tofrom: a[0:len])
  
#pragma omp teams num_teams(2)
{
    a[50] *= 2.0;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    equal = equal && a_s[_f_ct0] == a[_f_ct0];
  }
  assert(equal);
  return 0;
}
