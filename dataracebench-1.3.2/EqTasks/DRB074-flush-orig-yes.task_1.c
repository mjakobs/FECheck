#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
void f1(int *q);
void f1_s(int *q);

int main()
{
  int sum;
  int i;
  int sum_s;
  int i_s;
  i_s = __VERIFIER_nondet_int();
  i = i_s;
  sum_s = __VERIFIER_nondet_int();
  sum = sum_s;
// (First) sequential code segment
{
    f1_s(&i_s);
    sum_s += i_s;
  }
  
#pragma omp parallel reduction(+:sum) num_threads(10)
{
    f1(&i);
    sum += i;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}

void f1_s(int *q)
{
{
     *q = 1;
  }
}

void f1(int *q)
{
{
    
#pragma omp critical
     *q = 1;
    
#pragma omp flush
  }
}
