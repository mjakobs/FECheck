#include <assert.h> 
#include <stdio.h> 
#include <math.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
double uold[200][200];
double f[200][200];
double u[200][200];
int m;
int n;
double uold_s[200][200];
double u_s[200][200];

int main()
{
  double b;
  double ay;
  double ax;
  double resid;
  double error;
  int j;
  int i;
  double omega;
  double resid_s;
  double error_s;
  int j_s;
  int i_s;
//#pragma omp parallel
  n = __VERIFIER_nondet_int();
  m = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < 200; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 200; _f_ct1++) {
      u_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      u[_f_ct0][_f_ct1] = u_s[_f_ct0][_f_ct1];
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < 200; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 200; _f_ct1++) {
      f[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < 200; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 200; _f_ct1++) {
      uold_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      uold[_f_ct0][_f_ct1] = uold_s[_f_ct0][_f_ct1];
    }
  }
  omega = __VERIFIER_nondet_double();
  i_s = __VERIFIER_nondet_int();
  i = i_s;
  j_s = __VERIFIER_nondet_int();
  j = j_s;
  error_s = __VERIFIER_nondet_double();
  error = error_s;
  resid_s = __VERIFIER_nondet_double();
  resid = resid_s;
  ax = __VERIFIER_nondet_double();
  ay = __VERIFIER_nondet_double();
  b = __VERIFIER_nondet_double();
// (First) sequential code segment
{
//#pragma omp for private(i,j)
    for (i_s = 0; i_s < n; i_s++) {
      for (j_s = 0; j_s < m; j_s++) {
        uold_s[i_s][j_s] = u_s[i_s][j_s];
      }
    }
//#pragma omp for private(i,j,resid) reduction(+:error) nowait
    for (i_s = 1; i_s < n - 1; i_s++) {
      for (j_s = 1; j_s < m - 1; j_s++) {
        resid_s = (ax * (uold_s[i_s - 1][j_s] + uold_s[i_s + 1][j_s]) + ay * (uold_s[i_s][j_s - 1] + uold_s[i_s][j_s + 1]) + b * uold_s[i_s][j_s] - f[i_s][j_s]) / b;
        u_s[i_s][j_s] = uold_s[i_s][j_s] - omega * resid_s;
        error_s = error_s + resid_s * resid_s;
      }
    }
  }
  
#pragma omp parallel
{
    
#pragma omp for private(i,j)
    for (i = 0; i < n; i++) {
      for (j = 0; j < m; j++) {
        uold[i][j] = u[i][j];
      }
    }
    
#pragma omp for private(i,j,resid) reduction(+:error) nowait
    for (i = 1; i < n - 1; i++) {
      for (j = 1; j < m - 1; j++) {
        resid = (ax * (uold[i - 1][j] + uold[i + 1][j]) + ay * (uold[i][j - 1] + uold[i][j + 1]) + b * uold[i][j] - f[i][j]) / b;
        u[i][j] = uold[i][j] - omega * resid;
        error = error + resid * resid;
      }
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 200; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 200; _f_ct1++) {
      equal = equal && u_s[_f_ct0][_f_ct1] == u[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 200; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 200; _f_ct1++) {
      equal = equal && uold_s[_f_ct0][_f_ct1] == uold[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  equal = equal && error_s == error;
  assert(equal);
  return 0;
}
