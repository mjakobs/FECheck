#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int a;
  int a_s;
//#pragma omp parallel 
  a_s = __VERIFIER_nondet_int();
  a = a_s;
// (First) sequential code segment
{
//#pragma omp atomic
    a_s += 1;
  }
  
#pragma omp parallel
{
    
#pragma omp atomic
    a += 1;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && a_s == a;
  assert(equal);
  return 0;
}
