#include <assert.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int _size_st3_1;
int _size_st2_1;

int main()
{
  int len;
  len = __VERIFIER_nondet_int();
  double b[len];
  _size_st3_1 = len;
  double a[len];
  _size_st2_1 = len;
  double sum2;
  int i;
  double sum2_s;
  int i_s;
//#pragma omp parallel for reduction (+:sum2)
  sum2_s = __VERIFIER_nondet_double();
  sum2 = sum2_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2_1; _f_ct0++) {
    a[_f_ct0] = __VERIFIER_nondet_double();
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st3_1; _f_ct0++) {
    b[_f_ct0] = __VERIFIER_nondet_double();
  }
// (First) sequential code segment
  for (i_s = 0; i_s < len; i_s++) {
    sum2_s += a[i_s] * b[i_s];
  }
  
#pragma omp parallel for reduction (+:sum2)
  for (i = 0; i < len; i++) 
    sum2 += a[i] * b[i];
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum2_s == sum2;
  assert(equal);
  return 0;
}
