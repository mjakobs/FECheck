#include <assert.h> 
#include <stdio.h> 
extern unsigned int __VERIFIER_nondet_uint();
int fib(unsigned int n);
int fib_s(unsigned int n);
unsigned int input;

int main()
{
  int result;
  int result_s;
  input = __VERIFIER_nondet_uint();
// (First) sequential code segment
{
{
      result_s = fib_s(input);
    }
  }
  
#pragma omp parallel
{
    
#pragma omp single
{
      result = fib(input);
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && result_s == result;
  assert(equal);
  return 0;
}

int fib_s(unsigned int n)
{
{
    if (n < ((unsigned int )2)) {
      return (int )n;
    }
     else {
      int i;
      int j;
      i = fib_s(n - ((unsigned int )1));
      j = fib_s(n - ((unsigned int )2));
      int res = i + j;
/* We move the original taskwait to a location after i+j to 
 * simulate the missing taskwait mistake.
 * Directly removing the taskwait may cause a child task to write to i or j
 * within the stack of a parent task which may already be gone, causing seg fault.
 * This change is suggested by Joachim Protze @RWTH-Aachen. 
 * */
      return res;
    }
  }
}

int fib(unsigned int n)
{
{
    if (n < ((unsigned int )2)) {
      return (int )n;
    }
     else {
      int i;
      int j;
      
#pragma omp task shared(i)
      i = fib(n - ((unsigned int )1));
      
#pragma omp task shared(j)
      j = fib(n - ((unsigned int )2));
      int res = i + j;
/* We move the original taskwait to a location after i+j to 
 * simulate the missing taskwait mistake.
 * Directly removing the taskwait may cause a child task to write to i or j
 * within the stack of a parent task which may already be gone, causing seg fault.
 * This change is suggested by Joachim Protze @RWTH-Aachen. 
 * */
      
#pragma omp taskwait
      return res;
    }
  }
}
