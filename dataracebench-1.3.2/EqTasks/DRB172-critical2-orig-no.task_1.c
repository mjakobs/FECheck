#include <assert.h> 
#include <omp.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();

int main()
{
  double qq[10];
  double q[10];
  int i;
  double q_s[10];
  int i_s;
//  #pragma omp parallel default(shared)
  for (int _f_ct0 = 0; _f_ct0 < 10; _f_ct0++) {
    q_s[_f_ct0] = __VERIFIER_nondet_double();
    q[_f_ct0] = q_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < 10; _f_ct0++) {
    qq[_f_ct0] = __VERIFIER_nondet_double();
  }
// (First) sequential code segment
{
//  #pragma omp for private(i)
    for (i_s = 0; i_s < 10; i_s++) {
      q_s[i_s] += qq[i_s];
    }
//   #pragma omp critical
{
      q_s[9] += 1.0;
    }
//   #pragma omp barrier
//   #pragma omp single
{
      q_s[9] = q_s[9] - 1.0;
    }
  }
  
#pragma omp parallel default(shared)
{
    
#pragma omp for private(i)
    for (i = 0; i < 10; i++) {
      q[i] += qq[i];
    }
    
#pragma omp critical
{
      q[9] += 1.0;
    }
    
#pragma omp barrier
    
#pragma omp single
{
      q[9] = q[9] - 1.0;
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 10; _f_ct0++) {
    equal = equal && q_s[_f_ct0] == q[_f_ct0];
  }
  assert(equal);
  return 0;
}
