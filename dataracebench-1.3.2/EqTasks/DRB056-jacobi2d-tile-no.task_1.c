#include <assert.h> 
#include <stdio.h> 
#include <unistd.h> 
#include <string.h> 
#include <math.h> 
#include "../../polybench/polybench.h" 
#include "../../polybench/jacobi-2d-imper.h" 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();

int main()
{
  int c3;
  int c4;
  int c2;
  int c1;
  double B[500][500];
  double A[500][500];
  int n;
  int c3_s;
  int c4_s;
  int c2_s;
  int c1_s;
  double B_s[500][500];
  double A_s[500][500];
//#pragma omp parallel for private(c3, c4, c2)
  n = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      A_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      A[_f_ct0][_f_ct1] = A_s[_f_ct0][_f_ct1];
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      B_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      B[_f_ct0][_f_ct1] = B_s[_f_ct0][_f_ct1];
    }
  }
  c2_s = __VERIFIER_nondet_int();
  c2 = c2_s;
  c4_s = __VERIFIER_nondet_int();
  c4 = c4_s;
  c3_s = __VERIFIER_nondet_int();
  c3 = c3_s;
// (First) sequential code segment
  for (c1_s = 0; c1_s <= (((n + - 1) * 16 < 0?((16 < 0?-((-(n + - 1) + 16 + 1) / 16) : -((-(n + - 1) + 16 - 1) / 16))) : (n + - 1) / 16)); c1_s++) {
    for (c2_s = 0; c2_s <= (((n + - 1) * 16 < 0?((16 < 0?-((-(n + - 1) + 16 + 1) / 16) : -((-(n + - 1) + 16 - 1) / 16))) : (n + - 1) / 16)); c2_s++) {
      for (c3_s = 16 * c2_s; c3_s <= ((16 * c2_s + 15 < n + - 1?16 * c2_s + 15 : n + - 1)); c3_s++) {
//#pragma omp simd
        for (c4_s = 16 * c1_s; c4_s <= ((16 * c1_s + 15 < n + - 1?16 * c1_s + 15 : n + - 1)); c4_s++) {
          A_s[c4_s][c3_s] = (((double )c4_s) * ((double )(c3_s + 2)) + ((double )2)) / ((double )n);
          B_s[c4_s][c3_s] = (((double )c4_s) * ((double )(c3_s + 3)) + ((double )3)) / ((double )n);
        }
      }
    }
  }
  
#pragma omp parallel for private(c3, c4, c2)
  for (c1 = 0; c1 <= (((n + - 1) * 16 < 0?((16 < 0?-((-(n + - 1) + 16 + 1) / 16) : -((-(n + - 1) + 16 - 1) / 16))) : (n + - 1) / 16)); c1++) {
    for (c2 = 0; c2 <= (((n + - 1) * 16 < 0?((16 < 0?-((-(n + - 1) + 16 + 1) / 16) : -((-(n + - 1) + 16 - 1) / 16))) : (n + - 1) / 16)); c2++) {
      for (c3 = 16 * c2; c3 <= ((16 * c2 + 15 < n + - 1?16 * c2 + 15 : n + - 1)); c3++) {
        
#pragma omp simd
        for (c4 = 16 * c1; c4 <= ((16 * c1 + 15 < n + - 1?16 * c1 + 15 : n + - 1)); c4++) {
          A[c4][c3] = (((double )c4) * ((double )(c3 + 2)) + ((double )2)) / ((double )n);
          B[c4][c3] = (((double )c4) * ((double )(c3 + 3)) + ((double )3)) / ((double )n);
        }
      }
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      equal = equal && A_s[_f_ct0][_f_ct1] == A[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      equal = equal && B_s[_f_ct0][_f_ct1] == B[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
