#include <assert.h> 
#include <omp.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int sum2;
  int sum1;
  int i;
  int var;
  int sum2_s;
  int sum1_s;
  int i_s;
  int var_s;
//  #pragma omp parallel reduction(+: var)
  sum1_s = __VERIFIER_nondet_int();
  sum1 = sum1_s;
  sum2_s = __VERIFIER_nondet_int();
  sum2 = sum2_s;
// (First) sequential code segment
{
//  #pragma omp for schedule(static) reduction(+: sum1)
    for (i_s = 0; i_s < 5; i_s++) {
      sum1_s += i_s;
    }
//    #pragma omp for schedule(static) reduction(+: sum2)
    for (i_s = 0; i_s < 5; i_s++) {
      sum2_s += i_s;
    }
    var_s = sum1_s + sum2_s;
  }
  
#pragma omp parallel reduction(+: var)
{
    
#pragma omp for schedule(static) reduction(+: sum1)
    for (i = 0; i < 5; i++) {
      sum1 += i;
    }
    
#pragma omp for schedule(static) reduction(+: sum2)
    for (i = 0; i < 5; i++) {
      sum2 += i;
    }
    var = sum1 + sum2;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && var_s == var;
  assert(equal);
  return 0;
}
