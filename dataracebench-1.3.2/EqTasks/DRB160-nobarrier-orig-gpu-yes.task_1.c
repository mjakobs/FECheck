#include <assert.h> 
#include <stdio.h> 
#include <omp.h> 
#include <stdlib.h> 
extern int __VERIFIER_nondet_int();
int temp[16];
int c[16];
int b[16];
int a;
int temp_s[16];
int b_s[16];

int main()
{
  a = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < 16; _f_ct0++) {
    b_s[_f_ct0] = __VERIFIER_nondet_int();
    b[_f_ct0] = b_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < 16; _f_ct0++) {
    c[_f_ct0] = __VERIFIER_nondet_int();
  }
  for (int _f_ct0 = 0; _f_ct0 < 16; _f_ct0++) {
    temp_s[_f_ct0] = __VERIFIER_nondet_int();
    temp[_f_ct0] = temp_s[_f_ct0];
  }
// (First) sequential code segment
{
    for (int i = 0; i < 100; i++) {
      for (int i = 0; i < 16; i++) {
        temp_s[i] = b_s[i] + c[i];
      }
      for (int i = 16 - 1; i >= 0; i--) {
        b_s[i] = temp_s[i] * a;
      }
    }
  }
  
#pragma omp target map(tofrom:b[0:16]) map(to:c[0:16],temp[0:16],a) device(0)
{
    
#pragma omp teams
    for (int i = 0; i < 100; i++) {
      
#pragma omp distribute
      for (int i = 0; i < 16; i++) {
        temp[i] = b[i] + c[i];
      }
      
#pragma omp distribute
      for (int i = 16 - 1; i >= 0; i--) {
        b[i] = temp[i] * a;
      }
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 16; _f_ct0++) {
    equal = equal && b_s[_f_ct0] == b[_f_ct0];
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 16; _f_ct0++) {
    equal = equal && temp_s[_f_ct0] == temp[_f_ct0];
  }
  assert(equal);
  return 0;
}
