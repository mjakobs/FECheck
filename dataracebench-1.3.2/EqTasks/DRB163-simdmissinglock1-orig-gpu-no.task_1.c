#include <assert.h> 
#include <stdio.h> 
#include <omp.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int var[64];
  int var_s[64];
//#pragma omp target map(tofrom:var[0:C]) device(0)
//#pragma omp teams distribute parallel for reduction(+:var) 
  for (int _f_ct0 = 0; _f_ct0 < 64; _f_ct0++) {
    var_s[_f_ct0] = __VERIFIER_nondet_int();
    var[_f_ct0] = var_s[_f_ct0];
  }
// (First) sequential code segment
  for (int i = 0; i < 100; i++) {
//#pragma omp simd
    for (int i = 0; i < 64; i++) {
      var_s[i]++;
    }
  }
  
#pragma omp target map(tofrom:var[0:64]) device(0)
  
#pragma omp teams distribute parallel for reduction(+:var)
  for (int i = 0; i < 100; i++) {
    
#pragma omp simd
    for (int i = 0; i < 64; i++) {
      var[i]++;
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 64; _f_ct0++) {
    equal = equal && var_s[_f_ct0] == var[_f_ct0];
  }
  assert(equal);
  return 0;
}
