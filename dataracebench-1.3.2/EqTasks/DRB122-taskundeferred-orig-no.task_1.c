#include <assert.h> 
#include <omp.h> 
#include <stdio.h> 
#include <unistd.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int i;
  int var;
  int i_s;
  int var_s;
//  #pragma omp parallel sections 
  var_s = __VERIFIER_nondet_int();
  var = var_s;
// (First) sequential code segment
{
    for (i_s = 0; i_s < 10; i_s++) {
//     #pragma omp task shared(var) if(0)
{
        var_s++;
      }
    }
  }
  
#pragma omp parallel sections
{
    for (i = 0; i < 10; i++) {
      
#pragma omp task shared(var) if(0)
{
        var++;
      }
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && var_s == var;
  assert(equal);
  return 0;
}
