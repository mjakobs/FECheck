#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
int _size_st0_1;

int main()
{
  int len;
  len = __VERIFIER_nondet_int();
  int b;
  int a[len];
  _size_st0_1 = len;
  int error;
  int i;
  int a_s[len];
  int error_s;
  int i_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    a_s[_f_ct0] = __VERIFIER_nondet_int();
    a[_f_ct0] = a_s[_f_ct0];
  }
  b = __VERIFIER_nondet_int();
// (First) sequential code segment
{
    for (i_s = 0; i_s < len; i_s++) {
      a_s[i_s] = b + a_s[i_s] * 5;
    }
    error_s = a_s[9] + 1;
  }
  
#pragma omp parallel shared(b, error)
{
    
#pragma omp for nowait
    for (i = 0; i < len; i++) {
      a[i] = b + a[i] * 5;
    }
    
#pragma omp single
    error = a[9] + 1;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && error_s == error;
  assert(equal);
  return 0;
}
