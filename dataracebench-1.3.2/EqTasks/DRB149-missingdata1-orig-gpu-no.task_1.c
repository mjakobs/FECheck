#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <omp.h> 
extern int __VERIFIER_nondet_int();
int *c;
int _size_st2;
int *b;
int _size_st1;
int *a;
int _size_st0;
int *c_s;

int main()
{
  _size_st2 = __VERIFIER_nondet_int();
  c = ((int *)(malloc(_size_st2 * sizeof(int ))));
  _size_st1 = __VERIFIER_nondet_int();
  b = ((int *)(malloc(_size_st1 * sizeof(int ))));
  _size_st0 = __VERIFIER_nondet_int();
  a = ((int *)(malloc(_size_st0 * sizeof(int ))));
  c_s = ((int *)(malloc(_size_st2 * sizeof(int ))));
// #pragma omp target map(to:a[0:C],b[0:C*C]) map(tofrom:c[0:C]) device(0)
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    a[_f_ct0] = __VERIFIER_nondet_int();
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    b[_f_ct0] = __VERIFIER_nondet_int();
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    c_s[_f_ct0] = __VERIFIER_nondet_int();
    c[_f_ct0] = c_s[_f_ct0];
  }
// (First) sequential code segment
{
//  #pragma omp teams distribute parallel for
    for (int i = 0; i < 100; i++) {
      for (int j = 0; j < 100; j++) {
        c_s[i] += b[j + i * 100] * a[j];
      }
    }
  }
  
#pragma omp target map(to:a[0:100],b[0:100*100]) map(tofrom:c[0:100]) device(0)
{
    
#pragma omp teams distribute parallel for
    for (int i = 0; i < 100; i++) {
      for (int j = 0; j < 100; j++) {
        c[i] += b[j + i * 100] * a[j];
      }
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    equal = equal && c_s[_f_ct0] == c[_f_ct0];
  }
  assert(equal);
{
    free(c_s);
    free(a);
    free(b);
    free(c);
  }
  return 0;
}
