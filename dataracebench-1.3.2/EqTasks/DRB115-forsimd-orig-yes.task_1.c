#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int b[100];
  int a[100];
  int len;
  int i;
  int a_s[100];
  int i_s;
  len = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    a_s[_f_ct0] = __VERIFIER_nondet_int();
    a[_f_ct0] = a_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    b[_f_ct0] = __VERIFIER_nondet_int();
  }
// (First) sequential code segment
  for (i_s = 0; i_s < len - 1; i_s++) {
    a_s[i_s + 1] = a_s[i_s] + b[i_s];
  }
  
#pragma omp parallel for simd
  for (i = 0; i < len - 1; i++) {
    a[i + 1] = a[i] + b[i];
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    equal = equal && a_s[_f_ct0] == a[_f_ct0];
  }
  assert(equal);
  return 0;
}
