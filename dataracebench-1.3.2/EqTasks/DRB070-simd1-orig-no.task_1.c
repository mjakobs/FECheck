#include <assert.h> 
extern int __VERIFIER_nondet_int();
int c[100];
int b[100];
int a[100];
int a_s[100];

int main()
{
  int i;
  int i_s;
//#pragma omp simd
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    b[_f_ct0] = __VERIFIER_nondet_int();
  }
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    c[_f_ct0] = __VERIFIER_nondet_int();
  }
// (First) sequential code segment
  for (i_s = 0; i_s < 100; i_s++) {
    a_s[i_s] = b[i_s] * c[i_s];
  }
  
#pragma omp simd
  for (i = 0; i < 100; i++) {
    a[i] = b[i] * c[i];
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    equal = equal && a_s[_f_ct0] == a[_f_ct0];
  }
  assert(equal);
  return 0;
}
