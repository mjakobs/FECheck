#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern int __VERIFIER_nondet_int();
void foo();
void foo_s();
int *counter;
int _size_st0;
int *counter_s;

int main()
{
  _size_st0 = __VERIFIER_nondet_int();
  counter = ((int *)(malloc(_size_st0 * sizeof(int ))));
  counter_s = ((int *)(malloc(_size_st0 * sizeof(int ))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    counter_s[_f_ct0] = __VERIFIER_nondet_int();
    counter[_f_ct0] = counter_s[_f_ct0];
  }
// (First) sequential code segment
{
    foo_s();
  }
  
#pragma omp parallel
{
    foo();
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    equal = equal && counter_s[_f_ct0] == counter[_f_ct0];
  }
  assert(equal);
{
    free(counter_s);
    free(counter);
  }
  return 0;
}

void foo_s()
{
{
    ( *counter_s)++;
  }
}

void foo()
{
{
    ( *counter)++;
  }
}
