#include <assert.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int _size_st2_2;
int _size_st2_1;
int _size_st1_2;
int _size_st1_1;
int _size_st0_2;
int _size_st0_1;

int main()
{
  int len;
  len = __VERIFIER_nondet_int();
  int j;
  int i;
  double c[len][len];
  _size_st2_2 = len;
  _size_st2_1 = len;
  double b[len][len];
  _size_st1_2 = len;
  _size_st1_1 = len;
  double a[len][len];
  _size_st0_2 = len;
  _size_st0_1 = len;
  int j_s;
  int i_s;
  double c_s[len][len];
//#pragma omp simd collapse(2)
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st0_2; _f_ct1++) {
      a[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st1_2; _f_ct1++) {
      b[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st2_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st2_2; _f_ct1++) {
      c_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      c[_f_ct0][_f_ct1] = c_s[_f_ct0][_f_ct1];
    }
  }
  j_s = __VERIFIER_nondet_int();
  j = j_s;
// (First) sequential code segment
  for (i_s = 0; i_s < len; i_s++) {
    for (j_s = 0; j_s < len; j_s++) {
      c_s[i_s][j_s] = a[i_s][j_s] * b[i_s][j_s];
    }
  }
  
#pragma omp simd collapse(2)
  for (i = 0; i < len; i++) {
    for (j = 0; j < len; j++) {
      c[i][j] = a[i][j] * b[i][j];
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st2_2; _f_ct1++) {
      equal = equal && c_s[_f_ct0][_f_ct1] == c[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
