#include <assert.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int _size_st1_1;
int _size_st0_1;

int main()
{
  int len;
  len = __VERIFIER_nondet_int();
  double b[len];
  _size_st1_1 = len;
  double a[len];
  _size_st0_1 = len;
  double sum;
  int i2;
  int i;
  double sum_s;
  int i2_s;
  int i_s;
//#pragma omp target map(to: a[0:len], b[0:len]) map(tofrom: sum)
//#pragma omp teams num_teams(10) thread_limit(256) reduction (+:sum) 
//#pragma omp distribute
  i_s = __VERIFIER_nondet_int();
  i = i_s;
  sum_s = __VERIFIER_nondet_double();
  sum = sum_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    a[_f_ct0] = __VERIFIER_nondet_double();
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1_1; _f_ct0++) {
    b[_f_ct0] = __VERIFIER_nondet_double();
  }
// (First) sequential code segment
  for (i2_s = 0; i2_s < len; i2_s += 256) {
//#pragma omp parallel for reduction (+:sum)
    for (i_s = i2_s; i_s < ((i2_s + 256 < len?i2_s + 256 : len)); i_s++) {
      sum_s += a[i_s] * b[i_s];
    }
  }
  
#pragma omp target map(to: a[0:len], b[0:len]) map(tofrom: sum)
  
#pragma omp teams num_teams(10) thread_limit(256) reduction (+:sum)
  
#pragma omp distribute
  for (i2 = 0; i2 < len; i2 += 256) {
    
#pragma omp parallel for reduction (+:sum)
    for (i = i2; i < ((i2 + 256 < len?i2 + 256 : len)); i++) {
      sum += a[i] * b[i];
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}
