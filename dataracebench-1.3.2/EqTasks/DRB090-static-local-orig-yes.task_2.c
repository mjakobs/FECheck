#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
int _size_st1_1;

int main()
{
  int len;
  len = __VERIFIER_nondet_int();
  int b[len];
  _size_st1_1 = len;
  int i;
  int b_s[len];
  int i_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1_1; _f_ct0++) {
    b_s[_f_ct0] = __VERIFIER_nondet_int();
    b[_f_ct0] = b_s[_f_ct0];
  }
// (First) sequential code segment
{
    int tmp;
    for (i_s = 0; i_s < len; i_s++) {
      tmp = b_s[i_s] + i_s;
      b_s[i_s] = tmp;
    }
  }
  
#pragma omp parallel
{
    int tmp;
    
#pragma omp for
    for (i = 0; i < len; i++) {
      tmp = b[i] + i;
      b[i] = tmp;
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1_1; _f_ct0++) {
    equal = equal && b_s[_f_ct0] == b[_f_ct0];
  }
  assert(equal);
  return 0;
}
