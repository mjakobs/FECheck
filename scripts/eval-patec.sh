#!/bin/bash
# SPDX-License-Identifier: Apache-2.0

# Copyright 2021 Marie-Christine Jakobs
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

cd "$( dirname "${BASH_SOURCE[0]}" )"/..

folder="examples/"
files=$folder"reduction/*_seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  cmd="./FECheck -type=REDUCTION $seqProg $parProg"
  echo $cmd
  time $cmd
done

folder="FEVS-examples/"

files=$folder"doall/*_seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  cmd="./FECheck -type=DOALL $seqProg $parProg"
  echo $cmd
  time $cmd
done

files=$folder"reduction/*_seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  cmd="./FECheck -type=REDUCTION $seqProg $parProg"
  echo $cmd
  time $cmd
done

folder="dataracebench-1.3.2/"

subfolder="DoAll/correct/"
files=$folder$subfolder"*.seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  if [[ $parProg == *"DRB041-3mm-parallel-no.par.c" ]]
  then
    continue
  fi
  if [[ $parProg == *"DRB055-jacobi2d-parallel-no.par.c" ]]
  then
    continue
  fi
  if [[ $parProg == *"DRB112-linear-orig-no.par.c" ]]
  then
    continue
  fi
  cmd="./FECheck -type=DOALL $seqProg $parProg -fopenmp"
  echo $cmd
  time $cmd
done

subfolder="DoAll/incorrect/"
files=$folder$subfolder"*.seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  cmd="./FECheck -type=DOALL $seqProg $parProg"
  echo $cmd
  time $cmd
done

subfolder="Reduction/"
files=$folder$subfolder"*.seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  cmd="./FECheck -type=REDUCTION $seqProg $parProg"
  echo $cmd
  time $cmd
done

cd scripts

MILCmkLOC=$(cat "MILCmk-location.txt")
if [[ -d $MILCmkLOC"equivalence_checking/" ]]
then
 ./MILCmk-patec.sh
fi
