#!/bin/bash
# SPDX-License-Identifier: Apache-2.0

# Copyright 2021 Marie-Christine Jakobs
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# annotaterLoc=$(readlink -e "$( dirname "${BASH_SOURCE[0]}" )/../OMPSegmentAnnotater")
folder=$(cat "MILCmk-location.txt") 

# cd $annotaterLoc
# make

cd "$folder"

if [[ ! -d equivalence_checking ]]
then
  mkdir equivalence_checking
fi
  
# generate tasks 
for prog in $(ls *.c)
do
  parProg="equivalence_checking/"${prog:0:${#prog}-2}"_par.c"
  seqProg="equivalence_checking/"${prog:0:${#prog}-2}"_seq.c"

  if [[ -e $parProg ]]
  then
    continue
  fi

  
  cp $prog $parProg
 
  # insert pragmas
  # $annotaterLoc/Annotater $temp -I$folder
  # mv $temp $parProg
 
  if [[ $parProg == *"QLA_D3_c1_veq_V_dot_V_par.c" ]]
  then
    sed -i -e '21i#pragma scope_1' -e '36i#pragma epocs_1' $parProg ##
  fi
 
  if [[ $parProg == *"QLA_D3_D_vpeq_spproj_M_times_pD_par.c" ]]
  then
    sed -i -e '21i#pragma scope_1' -e '55i#pragma epocs_1' -e '57i#pragma scope_2' -e '91i#pragma epocs_2' -e '93i#pragma scope_3' -e '127i#pragma epocs_3' -e '129i#pragma scope_4' -e '163i#pragma epocs_4' -e '165i#pragma scope_5' -e '197i#pragma epocs_5' -e '203i#pragma scope_6' -e '237i#pragma epocs_6' -e '239i#pragma scope_7' -e '273i#pragma epocs_7' -e '275i#pragma scope_8' -e '309i#pragma epocs_8' -e '311i#pragma scope_9' -e '345i#pragma epocs_9' -e '347i#pragma scope_10' -e '379i#pragma epocs_10' $parProg  ##
  fi
 
  if [[ $parProg == *"QLA_D3_M_veq_M_times_pM_par.c" ]]
  then
    sed -i -e '18i#pragma scope_1' -e '34i#pragma epocs_1' $parProg  ##
  fi 
 
  if [[ $parProg == *"QLA_D3_r1_veq_norm2_V_par.c" ]]
  then
    sed -i -e '20i#pragma scope_1' -e '28i#pragma epocs_1' $parProg  ##
  fi
 
  if [[ $parProg == *"QLA_D3_V_veq_Ma_times_V_par.c" ]]
  then
     sed -i -e '19i#pragma scope_1' -e '30i#pragma epocs_1' $parProg ##
  fi
 
  if [[ $parProg == *"QLA_D3_V_vmeq_pV_par.c" ]]
  then
    sed -i -e '17i#pragma scope_1' -e '26i#pragma epocs_1' $parProg ##
  fi
  
  if [[ $parProg == *"QLA_D3_V_vpeq_M_times_pV_par.c" ]]
  then
    sed -i -e '18i#pragma scope_1' -e '32i#pragma epocs_1' $parProg  ##
  fi
  
  if [[ $parProg == *"QLA_F3_c1_veq_V_dot_V_par.c" ]]
  then
    sed -i -e '21i#pragma scope_1' -e '36i#pragma epocs_1' $parProg  ##
  fi  
  
  if [[ $parProg == *"QLA_F3_D_vpeq_spproj_M_times_pD_par.c" ]]
  then
    sed -i -e '21i#pragma scope_1' -e '55i#pragma epocs_1' -e '57i#pragma scope_2' -e '91i#pragma epocs_2' -e '93i#pragma scope_3' -e '127i#pragma epocs_3' -e '129i#pragma scope_4' -e '163i#pragma epocs_4' -e '165i#pragma scope_5' -e '197i#pragma epocs_5' -e '203i#pragma scope_6' -e '237i#pragma epocs_6' -e '239i#pragma scope_7' -e '273i#pragma epocs_7' -e '275i#pragma scope_8' -e '309i#pragma epocs_8' -e '311i#pragma scope_9' -e '345i#pragma epocs_9' -e '347i#pragma scope_10' -e '379i#pragma epocs_10' $parProg  
  fi
  
  if [[ $parProg == *"QLA_F3_M_veq_M_times_pM_par.c" ]]
  then
    sed -i -e '18i#pragma scope_1' -e '34i#pragma epocs_1' $parProg  ##
  fi
  
  if [[ $parProg == *"QLA_F3_r1_veq_norm2_V_par.c" ]]
  then
    sed -i -e '20i#pragma scope_1' -e '28i#pragma epocs_1' $parProg  ##
  fi
  
  if [[ $parProg == *"QLA_F3_V_veq_Ma_times_V_par.c" ]]
  then
     sed -i -e '19i#pragma scope_1' -e '30i#pragma epocs_1' $parProg ##
  fi
  
  if [[ $parProg == *"QLA_F3_V_vmeq_pV_par.c" ]]
  then
    sed -i -e '17i#pragma scope_1' -e '26i#pragma epocs_1' $parProg  ##
  fi
  
  if [[ $parProg == *"QLA_F3_V_vpeq_M_times_pV_par.c" ]]
  then
    sed -i -e '18i#pragma scope_1' -e '32i#pragma epocs_1' $parProg  ##
  fi       
 
  temp="temp.c"
  cp $parProg $temp

  
  # delete OpenMP pragmas
  sed -e '/^#pragma omp/d' -e '/#pragma disjoint/d' $temp > $seqProg
  rm $temp
done
  
# generate symbolic links for pattern
cd equivalence_checking

if [[ ! -d doall ]]
then
  mkdir doall
fi
cd doall

files="../*_seq.c"
for seqProg in $(ls $files) 
do 
  seqLink=${seqProg:3:${#seqProg}}
  if [[ -e $seqLink ]]
  then
    continue
  fi
  if [[ $seqProg == *"QLA_D3_c1_veq_V_dot_V_seq.c" ]]
  then
    continue
  fi
  if [[ $seqProg == *"QLA_F3_c1_veq_V_dot_V_seq.c" ]]
  then
    continue
  fi
  if [[ $seqProg == *"QLA_D3_r1_veq_norm2_V_seq.c" ]]
  then
    continue
  fi
  if [[ $seqProg == *"QLA_F3_r1_veq_norm2_V_seq.c" ]]
  then
    continue
  fi 
  ln -s $seqProg $seqLink
done

files="../*_par.c"
for parProg in $(ls $files) 
do 
  parLink=${parProg:3:${#parProg}}
  if [[ -e $parLink ]]
  then
    continue
  fi
  if [[ $parProg == *"QLA_D3_c1_veq_V_dot_V_par.c" ]]
  then
    continue
  fi
  if [[ $parProg == *"QLA_F3_c1_veq_V_dot_V_par.c" ]]
  then
    continue
  fi
  if [[ $parProg == *"QLA_D3_r1_veq_norm2_V_par.c" ]]
  then
    continue
  fi
  if [[ $parProg == *"QLA_F3_r1_veq_norm2_V_par.c" ]]
  then
    continue
  fi 
  ln -s $parProg $parLink 
done

cd ..
if [[ ! -d reduction ]]
then
  mkdir reduction
fi
cd reduction

  if [[ ! -e "QLA_D3_r1_veq_norm2_V_seq.c" ]]
  then
    ln -s "../QLA_D3_r1_veq_norm2_V_seq.c" "QLA_D3_r1_veq_norm2_V_seq.c"
  fi
  if [[ ! -e "QLA_D3_r1_veq_norm2_V_par.c" ]]
  then
    ln -s "../QLA_D3_r1_veq_norm2_V_par.c" "QLA_D3_r1_veq_norm2_V_par.c"
  fi
    
  if [[ ! -e "QLA_F3_r1_veq_norm2_V_seq.c" ]]
  then
    ln -s "../QLA_F3_r1_veq_norm2_V_seq.c" "QLA_F3_r1_veq_norm2_V_seq.c"
  fi 
    if [[ ! -e "QLA_F3_r1_veq_norm2_V_par.c" ]]
  then
    ln -s "../QLA_F3_r1_veq_norm2_V_par.c" "QLA_F3_r1_veq_norm2_V_par.c"
  fi 
