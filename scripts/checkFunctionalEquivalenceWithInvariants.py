# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: 2022 Marie-Christine Jakobs, Maik Wiesner
#
# Copyright 2022 Marie-Christine Jakobs, Maik Wiesner
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import glob
import os
import re
import subprocess
import sys

SCOPE_BEGIN_REGEX = "\s*#pragma scope_\d*"
SCOPE_END_REGEX = "\s*#pragma epocs_\d*"

def determine_invariant_locations(cprogram, onlyInvariantsAtStart):
    linenumber = 0
    pragma_lines = []
    start_pattern = re.compile(SCOPE_BEGIN_REGEX)
    end_pattern = re.compile(SCOPE_END_REGEX)
    
    with open(cprogram, "r") as sourcecode:
        for line in sourcecode:
            linenumber = linenumber + 1
            if start_pattern.match(line):
                pragma_line = str(linenumber+1) # segment starts after pragma
            elif end_pattern.match(line):
              if not onlyInvariantsAtStart:
                  pragma_line = pragma_line + '-' + str(linenumber-1) #segment ends before pragma
              pragma_lines.append(pragma_line)

    return ",".join(pragma_lines) # remove all string delimiters 

def generate_invariants_for(cprogram, entryFunction, invGenConfig, onlyInvariantsAtStart, CPAcheckerMainDir, script_dir):
    if(invGenConfig == "None"):
        return cprogram
    
    outputDir = "./output/"
    cprogramWithInvariants = os.path.join(outputDir, os.path.basename(cprogram))
    print(cprogramWithInvariants)
    
    # delete output file if exists
    if os.path.exists(cprogramWithInvariants):
        os.remove(cprogramWithInvariants)
            
    # build command
    cmd = [CPAcheckerMainDir + "scripts/cpa.sh"]
    
    cmd.append("-benchmark")
    cmd.append("-heap")
    cmd.append("1200M") # default heap size
    cmd.append("-config")
    if(invGenConfig == "val-DFA"):
        cmd.append(script_dir+"/configs/valueAnalysis-DFA.properties")
    elif(invGenConfig == "interval-DFA"):
        cmd.append(script_dir+"/configs/intervalAnalysis-DFA.properties")
    else:
        print("Unsupported invariant generator " + invGenConfig)
        return cprogram
    
    cmd.append("-setprop")
    cmd.append("cinvariants.forLines=" + determine_invariant_locations(cprogram, onlyInvariantsAtStart))
    cmd.append("-setprop")
    cmd.append("analysis.entryFunction=" + entryFunction)

    cmd.append(cprogram)
    
    # run command
    print(" ".join(cmd))
    subprocess.run(cmd)
    
    # check if invariants inserted
    if os.path.exists(cprogramWithInvariants):
      return cprogramWithInvariants
    else:
        print("Invariant generation not successful, continue with original file")
        return cprogram
    
def generate_verification_tasks(programOld, programNew, allowNoOut, script_dir):
    delete_tasks = glob.glob(script_dir + "/../checker_*.c")
    for to_delete in delete_tasks:
      os.remove(to_delete)
    
    cmd = ["./FECheck"]
    if allowNoOut:
        cmd.append("-allowNoOut")
    cmd+= [os.path.abspath(programOld), os.path.abspath(programNew)]
    print(" ".join(cmd))
    try:
      subprocess.run(cmd, cwd=script_dir+"/..", timeout=100)
    except subprocess.TimeoutExpired:
        print("Generation of verification tasks timed out.")
        sys.exit()
    
    return glob.glob(script_dir + "/../checker_*.c")
    
def check_functional_equivalence_sequential(seqProgramOld, seqProgramNew, entryFunction, invGenConfig, onlyInvariantsAtStart, onlyInvariantsAtOld, allowNoOut, CPAcheckerMainDir, script_dir):
    # generate and insert invariants
    seqProgramOld = generate_invariants_for(seqProgramOld, entryFunction, invGenConfig, onlyInvariantsAtStart, CPAcheckerMainDir, script_dir)
    if not onlyInvariantsAtOld:
      seqProgramNew = generate_invariants_for(seqProgramNew, entryFunction, invGenConfig, onlyInvariantsAtStart, CPAcheckerMainDir, script_dir)
    
    # generate verification tasks
    tasks = generate_verification_tasks(seqProgramOld, seqProgramNew, allowNoOut, script_dir)        
    
    # verify verification tasks
    baseCmdList = [CPAcheckerMainDir + "scripts/cpa.sh"] 
    baseCmdList.append("-default")
    baseCmdList.append("-spec")
    baseCmdList.append(str(CPAcheckerMainDir)+"config/specification/Assertion.spc")
    baseCmdList.append("-setprop")
    baseCmdList.append("log.consoleLevel=SEVERE")
    baseCmdList.append("-timelimit")
    baseCmdList.append("30s") #previously 300s
    baseCmdList.append("-preprocess")
    baseCmdList.append("-benchmark")
    baseCmdList.append("-heap")
    baseCmdList.append("1200M") # default heap size
    baseCmd = " ".join(baseCmdList)
    if len(tasks) > 0:
        print(baseCmd)
    for task in tasks:
        subprocess.run(baseCmdList + [task])

def check_functional_equivalence_parallelized(seqProgram, parallelizedProgram, entryFunction, invGenConfig, onlyInvariantsAtStart, allowNoOut, CPAcheckerMainDir, script_dir):
    # generate and insert invariants
    seqProgram = generate_invariants_for(seqProgram, entryFunction, invGenConfig, onlyInvariantsAtStart, CPAcheckerMainDir, script_dir)
    
    # generate verification tasks
    tasks = generate_verification_tasks(seqProgram, parallelizedProgram, allowNoOut, script_dir)
    
    # verify verification tasks
    baseCmdList = ["civl verify"]
    baseCmdList.append("-input_omp_thread_max=2")
    baseCmdList.append("-checkDivisionByZero=false")
    baseCmdList.append("-checkMemoryLeak=false")
    baseCmdList.append("-timeout=30") # previously 300
    baseCmdList.append(str(script_dir)+"/../nondet_funs.c")
    baseCmdList.append(str(script_dir)+"/../assume.c")
    baseCmd = " ".join(baseCmdList)
    if len(tasks)>0:
        print(baseCmd)
    for task in tasks:
        subprocess.run(baseCmd + " " + task, shell=True)

def get_parser():
    parser = argparse.ArgumentParser(prog='checkFunctionalEquivalenceWithInvariants.py', description='Generate')
    parser.add_argument('-invGenConfig', required=True, choices=['val-DFA', 'interval-DFA', 'None'], help='Choose which configuration of CPAchecker to use to generate invariants.')
    parser.add_argument('-onlyInvariantsAtOld', action='store_true', help='Only insert invariants into original program')
    parser.add_argument('-onlyInvariantsAtStart', action='store_true', help='Only insert invariants at the beginning of compared segments')
    parser.add_argument('-cpachecker', default='../../CPAchecker/', help='path to cpachecker main directory')
    parser.add_argument('-progOld', required=True, help='path to original C program file')
    parser.add_argument('-progNew', required=True, help='path to modified C program file')
    parser.add_argument('-checkParallelization', action='store_true', help='Inspect functional equivalence of a sequential and OpenMP program instead of two sequential programs')
    parser.add_argument('-allowNoOut', action='store_true', help='Do not fail if equivalence checker does not detect common output.')
    parser.add_argument('-entryFunction', required=True, help='Entry function for invariant generation')
    return parser

if __name__ == "__main__":
    script_dir = os.path.dirname(sys.argv[0])
    # print(script_dir)
    args = get_parser().parse_args()
    
    CPAcheckerMainDir = args.cpachecker
    if not os.path.exists(CPAcheckerMainDir):
        print("Abort: CPAchecker directory " + CPAcheckerMainDir + " not found.")
        exit(0)
        
    if CPAcheckerMainDir[-1:] != "/":
        CPAcheckerMainDir = CPAcheckerMainDir + "/"
    
    if args.checkParallelization:
        check_functional_equivalence_parallelized(args.progOld, args.progNew, args.entryFunction, args.invGenConfig, args.onlyInvariantsAtStart, args.allowNoOut, CPAcheckerMainDir, script_dir)
    else:
        check_functional_equivalence_sequential(args.progOld, args.progNew, args.entryFunction, args.invGenConfig, args.onlyInvariantsAtStart, args.onlyInvariantsAtOld, args.allowNoOut, CPAcheckerMainDir, script_dir)
