# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: 2022 Marie-Christine Jakobs, Maik Wiesner
#
# Copyright 2023 Marie-Christine Jakobs
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import glob
import os
import re
import subprocess
import sys  
        
def generate_difference_program(CPAcheckerMainDir, entryFunction, progOld, progNew):
    outputDir = "./output/"
    diffProgName = "diff_" + os.path.basename(progOld)
    cDiffProgram = os.path.join(outputDir, diffProgName)
    print(cDiffProgram)
    
    # delete output file if exists
    if os.path.exists(cDiffProgram):
        os.remove(cDiffProgram)
            
    # build command
    cmdList = [CPAcheckerMainDir + "scripts/cpa.sh"] 
    cmdList.append("-differentialProgramGenerator")
    cmdList.append("-setprop")
    cmdList.append("analysis.entryFunction=" + entryFunction)
    cmdList.append("-setprop")
    cmdList.append("residualprogram.file=" + diffProgName)
    cmdList.append("-setprop")
    cmdList.append("differential.program="+os.path.abspath(progNew))
    cmdList.append("-setprop")
    cmdList.append("log.consoleLevel=SEVERE")
    cmdList.append("-timelimit")
    cmdList.append("30s") #previously 300s
    cmdList.append("-benchmark")
    cmdList.append("-heap")
    cmdList.append("1200M") # default heap size
    cmdList.append(progOld)
    cmd = " ".join(cmdList)
 
    # run command
    print(cmd)
    subprocess.run(cmd)
    
    # check if difference program generated
    if os.path.exists(cDiffProgram):
      return cDiffProgram
    else:
        print("Difference computation failed, continue with original file")
        return progOld

    
def generate_verification_tasks(programOld, programNew, allowNoOut, script_dir):
    delete_tasks = glob.glob(script_dir + "/../checker_*.c")
    for to_delete in delete_tasks:
      os.remove(to_delete)
    
    cmd = ["./FECheck"]
    if allowNoOut:
        cmd.append("-allowNoOut")
    cmd+= [os.path.abspath(programOld), os.path.abspath(programNew)]
    print(" ".join(cmd))
    try:
      subprocess.run(cmd, cwd=script_dir+"/..", timeout=100)
    except subprocess.TimeoutExpired:
        print("Generation of verification tasks timed out.")
        sys.exit()
    
    return glob.glob(script_dir + "/../checker_*.c")


def check_functional_equivalence(progOld, progNew, entryFunction, CPAcheckerMainDir, script_dir):
    # generate differences
    progDiffOld = generate_difference_program(CPAcheckerMainDir, entryFunction, progOld, progNew)
    progDiffNew = generate_difference_program(CPAcheckerMainDir, entryFunction, progNew, progOld)
    
    # generate verification tasks
    tasks = generate_verification_tasks(progDiffOld, progDiffNew, False, script_dir)            
    
    # verify verification tasks
    cmdList = [CPAcheckerMainDir + "scripts/cpa.sh"] 
    cmdList.append("-default")
    cmdList.append("-spec")
    cmdList.append(str(CPAcheckerMainDir)+"config/specification/Assertion.spc")
    cmdList.append("-setprop")
    cmdList.append("log.consoleLevel=SEVERE")
    cmdList.append("-timelimit")
    cmdList.append("30s") #previously 300s
    cmdList.append("-preprocess")
    cmdList.append("-noout")
    cmd = " ".join(cmdList)
    if len(tasks) > 0:
        print(cmd)
    for task in tasks:
        subprocess.run(cmdList + [task]) 
        
    # clean up, remove task programs and diff programs (if not original programs)
    for task in tasks:
      os.remove(task)
      
    if not os.path.samefile(progOld, progDiffOld):
      os.remove(progDiffOld)
      
    if not os.path.samefile(progNew, progDiffNew):
      os.remove(progDiffNew)    


def get_parser():
    parser = argparse.ArgumentParser(prog='checkFunctionalEquivalenceOfDifference.py', description='Generate')
#    parser.add_argument('-invGenConfig', required=True, choices=['val-DFA', 'interval-DFA', 'None'], help='Choose which configuration of CPAchecker to use to generate invariants.')
#    parser.add_argument('-onlyInvariantsAtOld', action='store_true', help='Only insert invariants into original program')
#    parser.add_argument('-onlyInvariantsAtStart', action='store_true', help='Only insert invariants at the beginning of compared segments')
    parser.add_argument('-cpachecker', default='../../CPAchecker/', help='path to cpachecker main directory')
    parser.add_argument('-progOld', required=True, help='path to original C program file')
    parser.add_argument('-progNew', required=True, help='path to modified C program file')
    parser.add_argument('-entryFunction', required=True, help='Entry function for invariant generation')
    parser.add_argument('-allowNoOut', action='store_true', help='Do not fail if equivalence checker does not detect common output.')
    return parser


if __name__ == "__main__":
    script_dir = os.path.dirname(sys.argv[0])
    args = get_parser().parse_args()
    
    CPAcheckerMainDir = args.cpachecker
    if not os.path.exists(CPAcheckerMainDir):
        print("Abort: CPAchecker directory " + CPAcheckerMainDir + " not found.")
        exit(0)
        
    if CPAcheckerMainDir[-1:] != "/":
        CPAcheckerMainDir = CPAcheckerMainDir + "/"
    
    check_functional_equivalence(args.progOld, args.progNew, args.entryFunction, CPAcheckerMainDir, script_dir)
