#!/bin/bash
# SPDX-License-Identifier: Apache-2.0

# Copyright 2021 Marie-Christine Jakobs
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


cd "$( dirname "${BASH_SOURCE[0]}" )"/..

folder="examples/"
regFolder="examples/"
files=$folder"*_seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  cmd="./FECheck $seqProg $parProg"
  echo $cmd
  time $cmd

  for checkProg in $(ls checker_*.c)
  do
     cmd="gcc -c -fopenmp $checkProg"
     out=`$cmd`
     echo $out

    index=${checkProg:8:${#checkProg}}
    prefixFile=${prefixPath:${#folder}:${#prefixPath}}
    diff $checkProg $regFolder$prefixFile"task_"$index

  done
  rm checker_?.c
  rm checker_?.o
done


folder="examples/asOneTask/"
regFolder="examples/asOneTask/"
files=$folder"*_seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  cmd="./FECheck $seqProg $parProg"
  echo $cmd
  time $cmd

  for checkProg in $(ls checker_*.c)
  do
     cmd="gcc -c -fopenmp $checkProg"
     out=`$cmd`
     echo $out

    index=${checkProg:8:${#checkProg}}
    prefixFile=${prefixPath:${#folder}:${#prefixPath}}
    diff $checkProg $regFolder$prefixFile"task_"$index

  done
  rm checker_?.c
  rm checker_?.o
done



folder="FEVS-examples/"
regFolder="FEVS-examples/"
files=$folder"*_seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  cmd="./FECheck $seqProg $parProg"
  echo $cmd

  time $cmd
  for checkProg in $(ls checker_*.c)
  do
     cmd="gcc -c -fopenmp $checkProg"
     out=`$cmd`
     echo $out

    index=${checkProg:8:${#checkProg}}
    prefixFile=${prefixPath:${#folder}:${#prefixPath}}
    diff $checkProg $regFolder$prefixFile"task_"$index
  done
  rm checker_?.c
  rm checker_?.o
done

folder="FEVS-examples/asOneTask/"
regFolder="FEVS-examples/asOneTask/"
files=$folder"*_seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  cmd="./FECheck $seqProg $parProg"
  echo $cmd

  time $cmd
  for checkProg in $(ls checker_*.c)
  do
     cmd="gcc -c -fopenmp $checkProg"
     out=`$cmd`
     echo $out

    index=${checkProg:8:${#checkProg}}
    prefixFile=${prefixPath:${#folder}:${#prefixPath}}
    diff $checkProg $regFolder$prefixFile"task_"$index
  done
  rm checker_?.c
  rm checker_?.o
done
