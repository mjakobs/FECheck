#!/bin/bash
# SPDX-License-Identifier: Apache-2.0

# Copyright 2021 Marie-Christine Jakobs
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

cd "$( dirname "${BASH_SOURCE[0]}" )"/..s

folder="sequential-examples/"
regFolder="sequential-examples/"
files=$folder"*_1.c"
for seqProg in $(ls $files) 
do 
  if [[ $seqProg == *"task_1.c" ]]
  then
    continue
  fi
  prefixPath=${seqProg:0:${#seqProg}-3}
  parProg=$prefixPath"2.c"
  cmd="./FECheck $seqProg $parProg"
  echo $cmd

  time $cmd
  for checkProg in $(ls checker_*.c)
  do
     cmd="gcc -c $checkProg"
     out=`$cmd`
     echo $out

    index=${checkProg:8:${#checkProg}}
    prefixFile=${prefixPath:${#folder}:${#prefixPath}}
    diff $checkProg $regFolder$prefixFile"task_"$index
  done
  rm checker_*.c
  rm checker_*.o
done

folder="sequential-examples/asOneTask/"
regFolder="sequential-examples/asOneTask/"
files=$folder"*_1.c"
for seqProg in $(ls $files) 
do 
  if [[ $seqProg == *"task_1.c" ]]
  then
    continue
  fi
  prefixPath=${seqProg:0:${#seqProg}-3}
  parProg=$prefixPath"2.c"
  cmd="./FECheck $seqProg $parProg"
  echo $cmd

  time $cmd
  for checkProg in $(ls checker_*.c)
  do
     cmd="gcc -c $checkProg"
     out=`$cmd`
     echo $out

    index=${checkProg:8:${#checkProg}}
    prefixFile=${prefixPath:${#folder}:${#prefixPath}}
    diff $checkProg $regFolder$prefixFile"task_"$index
  done
  rm checker_*.c
  rm checker_*.o
done
