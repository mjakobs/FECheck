#!/bin/bash
# SPDX-License-Identifier: Apache-2.0

# Copyright 2021 Marie-Christine Jakobs
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# adapt to your installation of CPAchecker
CPAchecker=../../CPAchecker-2.0-unix

cd "$( dirname "${BASH_SOURCE[0]}" )"

for task in $(ls ../sequential-examples/*_task_?.c) 
do 
  cmd="$CPAchecker/scripts/cpa.sh -default -spec $CPAchecker/config/specification/Assertion.spc -setprop log.consoleLevel=SEVERE -timelimit 300s -preprocess -benchmark -heap 1200M $task"
  echo $cmd
  time $cmd

done

for task in $(ls ../sequential-examples/asOneTask/*_task_?.c) 
do 
  cmd="$CPAchecker/scripts/cpa.sh -default -spec $CPAchecker/config/specification/Assertion.spc -setprop log.consoleLevel=SEVERE -timelimit 300s -preprocess -benchmark -heap 1200M $task"
  echo $cmd
  time $cmd

done


