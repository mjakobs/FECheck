#!/bin/bash
# SPDX-License-Identifier: Apache-2.0

# Copyright 2021 Marie-Christine Jakobs
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

cd "$( dirname "${BASH_SOURCE[0]}" )"/..

folder="dataracebench-1.3.2/Others/correct/"
regFolder="dataracebench-1.3.2/EqTasks/"
files=$folder"*.seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-6}
  parProg=$prefixPath".par.c"
  
  if [[ $parProg == *"DRB042-3mm-tile-no.par.c" ]] || [[ $parProg == *"DRB044-adi-tile-no.par.c" ]] # timeout
  then
    continue
  fi
  
   if [[ $parProg == *"DRB085-threadprivate-orig-no.par.c" ]] || [[ $parProg == *"DRB091-threadprivate2-orig-no.par.c" ]] || [[ $parProg == *"DRB102-copyprivate-orig-no.par.c" ]] ||  [[ $parProg == *"DRB128-tasking-threadprivate2-orig-no.par.c" ]] || [[ $parProg == *"DRB171-threadprivate3-orig-no.par.c" ]]  # threadprivate
  then
    continue
  fi
  
  if [[ $parProg == *"DRB094-doall2-ordered-orig-no.par.c" ]] || [[ $parProg == *"DRB096-doall2-taskloop-collapse-orig-no.par.c" ]]   # >= OpenMP 4.5
  then
    continue
  fi

 #  if [[ $parProg == *"DRB042-3mm-tile-no.par.c"  ]] || [[ $parProg == *"DRB044-adi-tile-no.par.c" ]] || [[ $parProg == *"DRB056-jacobi2d-tile-no.par.c" ]] ||  [[ $parProg == *"DRB058-jacobikernel-orig-no.par.c" ]] ||  [[ $parProg == *"DRB070-simd1-orig-no.par.c" ]] ||  [[ $parProg == *"DRB071-targetparallelfor-orig-no.par.c" ]] ||  [[ $parProg == *"DRB093-doall2-collapse-orig-no.par.c" ]] ||  [[ $parProg == *"DRB097-target-teams-distribute-orig-no.par.c" ]] ||  [[ $parProg == *"DRB098-simd2-orig-no.par.c" ]]  ||  [[ $parProg == *"DRB099-targetparallelfor2-orig-no.par.c" ]] ||  [[ $parProg == *"DRB104-nowait-barrier-orig-no.par.c" ]] ||  [[ $parProg == *"DRB121-reduction-orig-no.par.c" ]] ||  [[ $parProg == *"DRB137-simdsafelen-orig-no.par.c" ]] ||  [[ $parProg == *"DRB141-reduction-barrier-orig-no.par.c" ]] ||  [[ $parProg == *"DRB172-critical2-orig-no.par.c" ]] # only equivalent live vars
 # then
 #   continue
 # fi
 
 
# if [[ $parProg == *"DRB094-doall2-ordered-orig-no.par.c" ]] || [[ $parProg == *"DRB132-taskdep4-orig-omp45-no.par.c" ]] || [[ $parProg == *"DRB133-taskdep5-orig-omp45-no.par.c" ]] || [[ $parProg == *"DRB141-reduction-barrier-orig-no.par.c" ]] || [[ $parProg == *"DRB158-missingtaskbarrier-orig-gpu-no.par.c" ]] || [[ $parProg == *"DRB166-taskdep4-orig-omp50-no.par.c" ]] || [[ $parProg == *"DRB167-taskdep4-orig-omp50-no.par.c" ]] # I/O (printf, fprint)
#  then
#    continue
#  fi
 
  cmd="./FECheck $seqProg $parProg"
  echo $cmd
  time $cmd

  for checkProg in $(ls checker_*.c)
  do
     cmd="gcc -c -fopenmp $checkProg"
     out=`$cmd`
     echo $out

    index=${checkProg:8:${#checkProg}}
    prefixFile=${prefixPath:${#folder}:${#prefixPath}}
    diff $checkProg $regFolder$prefixFile".task_"$index

  done
  rm checker_*.c
  rm checker_*.o
done

folder="dataracebench-1.3.2/Others/incorrect/"
files=$folder"*.seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-6}
  parProg=$prefixPath".par.c"
  
  if  [[ $parProg == *"DRB127-tasking-threadprivate1-orig-no.par.c" ]]    # threadprivate
  then
    continue
  fi  
  
  if [[ $parProg == *"DRB095-doall2-taskloop-orig-yes.par.c" ]]   # >= OpenMP 4.5
  then
    continue
  fi 
  
#  if [[ $parProg == *"DRB075-getthreadnum-orig-yes.par.c" ]] || [[ $parProg == *"DRB103-master-orig-no.par.c" ]] || [[ $parProg == *"DRB126-firstprivatesections-orig-no.par.c" ]] || [[ $parProg == *"DRB131-taskdep4-orig-omp45-yes.par.c" ]] || [[ $parProg == *"DRB134-taskdep5-orig-omp45-yes.par.c" ]] || [[ $parProg == *"DRB142-acquirerelease-orig-yes.par.c" ]] || [[ $parProg == *"DRB143-acquirerelease-orig-no.par.c" ]] || [[ $parProg == *"DRB165-taskdep4-orig-omp50-yes.par.c" ]] || [[ $parProg == *"DRB168-taskdep5-orig-omp50-yes.par.c" ]] # I/O (printf, fprint)
#  then
#    continue
#  fi
 
  
  cmd="./FECheck $seqProg $parProg"
  echo $cmd
  time $cmd

  for checkProg in $(ls checker_*.c)
  do
     cmd="gcc -c -fopenmp $checkProg"
     out=`$cmd`
     echo $out

    index=${checkProg:8:${#checkProg}}
    prefixFile=${prefixPath:${#folder}:${#prefixPath}}
    diff $checkProg $regFolder$prefixFile".task_"$index

  done
  rm checker_*.c
  rm checker_*.o
done

prefixfolder="dataracebench-1.3.2/"
regFolder=$prefixfolder"EqTasks-pattern/"
folder=$prefixfolder"DoAll/correct/"
files=$folder"*.seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  if [[ $parProg == *"DRB041-3mm-parallel-no.par.c" ]] || [[ $parProg == *"DRB055-jacobi2d-parallel-no.par.c" ]] #time out
  then
    continue
  fi
#if  [[ $parProg == *"DRB043-adi-parallel-no.par.c" ]] ||  [[ $parProg == *"DRB045-doall1-orig-no.par.c" ]] ||  [[ $parProg == *"DRB046-doall2-orig-no.par.c" ]] ||  [[ $parProg == *"DRB047-doallchar-orig-no.par.c" ]] ||  [[ $parProg == *"DRB048-firstprivate-orig-no.par.c" ]] ||  [[ $parProg == *"DRB050-functionparameter-orig-no.par.c" ]] ||  [[ $parProg == *"DRB052-indirectaccesssharebase-orig-no.par.c" ]] ||  [[ $parProg == *"DRB053-inneronly1-orig-no.par.c" ]] ||  [[ $parProg == *"DRB054-inneronly2-orig-no.par.c" ]] ||  [[ $parProg == *"DRB057-jacobiinitialize-orig-no.par.c" ]] ||  [[ $parProg == *"DRB059-lastprivate-orig-no.par.c" ]] ||  [[ $parProg == *"DRB060-matrixmultiply-orig-no.par.c" ]] ||  [[ $parProg == *"DRB061-matrixvector1-orig-no.par.c" ]] ||  [[ $parProg == *"DRB063-outeronly1-orig-no.par.c" ]] ||  [[ $parProg == *"DRB064-outeronly2-orig-no.par.c" ]] ||  [[ $parProg == *"DRB067-restrictpointer1-orig-no.par.c" ]] ||  [[ $parProg == *"DRB068-restrictpointer2-orig-no.par.c" ]] ||  [[ $parProg == *"DRB112-linear-orig-no.par.c" ]] ||  [[ $parProg == *"DRB113-default-orig-no.par.c" ]] ||  [[ $parProg == *"DRB170-nestedloops-orig-no.par.c" ]] # only equivalent live vars
 # then
 #   continue
 # fi

  cmd="./FECheck $seqProg $parProg -fopenmp"
  echo $cmd
  time $cmd

  for checkProg in $(ls checker_*.c)
  do
     cmd="gcc -c -fopenmp $checkProg"
     out=`$cmd`
     echo $out

    index=${checkProg:8:${#checkProg}}
    prefixFile=${prefixPath:${#folder}:${#prefixPath}}
    diff $checkProg $regFolder$prefixFile"task_"$index

  done
  rm checker_?.c
  rm checker_?.o
done

folder=$prefixfolder"DoAll/incorrect/"
files=$folder"*.seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  
 # if [[ $parProg == *"DRB049-fprintf-orig-no.par.c" ]]  # fprintf order
 # then
#    continue
#  fi
  
  
  cmd="./FECheck $seqProg $parProg"
  echo $cmd
  time $cmd

  for checkProg in $(ls checker_*.c)
  do
     cmd="gcc -c -fopenmp $checkProg"
     out=`$cmd`
     echo $out

    index=${checkProg:8:${#checkProg}}
    prefixFile=${prefixPath:${#folder}:${#prefixPath}}
    diff $checkProg $regFolder$prefixFile"task_"$index

  done
  rm checker_?.c
  rm checker_?.o
done

folder=$prefixfolder"Reduction/"
files=$folder"*.seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  
 # if [[ $parProg == *"DRB062-matrixvector2-orig-no.par.c" ]] ||  [[ $parProg == *"DRB065-pireduction-orig-no.par.c" ]] # only equivalent live vars
 # then
 #   continue
 # fi
    
  cmd="./FECheck $seqProg $parProg"
  echo $cmd
  time $cmd

  for checkProg in $(ls checker_*.c)
  do
     cmd="gcc -c -fopenmp $checkProg"
     out=`$cmd`
     echo $out

    index=${checkProg:8:${#checkProg}}
    prefixFile=${prefixPath:${#folder}:${#prefixPath}}
    diff $checkProg $regFolder$prefixFile"task_"$index

  done
  rm checker_?.c
  rm checker_?.o
done

cd scripts
