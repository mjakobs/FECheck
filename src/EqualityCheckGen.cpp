//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "EqualityCheckGen.h"
#include "PECConstants.h"
#include <assert.h>
#include <limits>

  void EqualityCheckGen::add_eq_checks(std::vector<std::tuple<std::string, std::string, SgType*>>& varPairs, bool assertPerPair, std::map<std::string, std::string>& pVarNameToSizeVar, TypeManager* pTypeMgr)
  {
    varNameToSizeVar = &pVarNameToSizeVar;

    if(!assertPerPair)
    {
      add_equal_init();
    }

    for(std::vector<std::tuple<std::string, std::string, SgType*>>::iterator it = varPairs.begin(); it != varPairs.end(); it++)
    {
      count = 0;

      if(assertPerPair)
      {
        add_equal_init();
      }

      add_equal_block(SageBuilder::buildVarRefExp(SgName(std::get<0>(*it))), SageBuilder::buildVarRefExp(SgName(std::get<1>(*it))), std::get<2>(*it), pTypeMgr);

      if(assertPerPair)
      {
        add_assert();
      }
    }

    if(!assertPerPair)
    {
      add_assert();
    }

  }

  void EqualityCheckGen::add_assert()
  {
    SageInterface::appendStatement(SageBuilder::buildFunctionCallStmt (SgName("assert"), SageBuilder::buildVoidType(), SageBuilder::buildExprListExp(varEq)));
  }

  void EqualityCheckGen::add_equal_init()
  {
    SageInterface::appendStatement(SageBuilder::buildAssignStatement (varEq, SageBuilder::buildIntVal(1)));
  }

  void EqualityCheckGen::add_equal_block(SgExpression* exprS, SgExpression* exprP, SgType* type, TypeManager* pTypeMgr)
  { 
      type = unwrapType(type);

      switch(get_eq_type(type, pTypeMgr))
      {
        case ARRAY:
          add_array_eq_check(exprS, exprP, isSgArrayType(type), pTypeMgr);
          break;
        case POINTER:
          add_equality_ptr(exprS, exprP, type, pTypeMgr);
          break;
        case SIMPLE:
          add_equality_expr(exprS, exprP);
          break;
        case STRUCT:
          add_equality_struct(exprS, exprP, type, pTypeMgr);
          break;
        default:
          assert(0);
      }
  }

  SgType* EqualityCheckGen::unwrapType(SgType* type)
  {
    while(type && (isSgModifierType(type) || isSgTypedefType(type)))
    {
      if(isSgModifierType(type))
      {
        type = isSgModifierType(type)->get_base_type(); 
      }

      while(isSgTypedefType(type))
      {
        type = isSgTypedefDeclaration(isSgTypedefType(type)->get_declaration())->get_base_type();
      }
    }
    return type;
  }

  void EqualityCheckGen::add_equality_expr(SgExpression* lhs, SgExpression* rhs)
  {
    SageInterface::appendStatement(SageBuilder::buildAssignStatement (varEq, SageBuilder::buildAndOp(varEq, SageBuilder::buildEqualityOp(lhs, rhs))));
  }

  void EqualityCheckGen::add_array_eq_check(SgExpression* varS, SgExpression* varP, SgArrayType* type, TypeManager* pTypeMgr)
  {
    std::vector<SgExpression*> dimLen = SageInterface::get_C_array_dimensions(type);
    assert((int)dimLen.size()==SageInterface::getDimensionCount(type)+1);	
    SgName counter;
    SgBasicBlock* body;
    SgExpression* arrVS = varS;
    SgExpression* arrVP = varP;
    SgType* baseType = type;

    std::string* upperBoundVarName;

    if(varNameToSizeVar->find(varP->unparseToString())!= varNameToSizeVar->end())
    {
      upperBoundVarName = &((*varNameToSizeVar)[varP->unparseToString()]);
    }
    else
    {
      upperBoundVarName = NULL;
    }

    for(unsigned int i=1; i<dimLen.size(); i++)
    {

      counter = SgName(prefixForCounter + std::to_string(count++));
      arrVS = SageBuilder::buildPntrArrRefExp(arrVS, SageBuilder::buildVarRefExp(counter));
      arrVP = SageBuilder::buildPntrArrRefExp(arrVP, SageBuilder::buildVarRefExp(counter));
      body = SageBuilder::buildBasicBlock_nfi();

      SageInterface::const_int_expr_t  arr_size = SageInterface::evaluateConstIntegerExpression(dimLen[i]);
      if(!isSgNullExpression(dimLen[i]) && arr_size.hasValue_)// TODO else part, 
      {
        assert(arr_size.value_>=0);
        SgType* counterType;
        SgExpression* upperBound;  
               
        if(arr_size.value_ <= INT_MAX)
        {
          counterType = SageBuilder::buildIntType();
          upperBound = SageBuilder::buildIntVal(arr_size.value_);
        }
        else if (arr_size.value_ <= UINT_MAX)
        {
          counterType = SageBuilder::buildUnsignedIntType();
          upperBound = SageBuilder::buildUnsignedIntVal(arr_size.value_);
        }
        else if (arr_size.value_ <= LONG_MAX)
        {
          counterType = SageBuilder::buildLongType();
          upperBound = SageBuilder::buildLongIntVal(arr_size.value_);
        }
        else if (arr_size.value_ <= ULONG_MAX)
        {
          counterType = SageBuilder::buildUnsignedLongType();
          upperBound = SageBuilder::buildUnsignedLongVal(arr_size.value_);
        }
        else if (arr_size.value_ <= LLONG_MAX)
        {
          counterType = SageBuilder::buildLongLongType();
          upperBound = SageBuilder::buildLongLongIntVal(arr_size.value_);
        }
        else if (arr_size.value_ <= ULLONG_MAX)
        {
          counterType = SageBuilder::buildUnsignedLongLongType();
          upperBound = SageBuilder::buildUnsignedLongLongIntVal(arr_size.value_);
        }
        else
        {
          assert(0);
        }

        SageInterface::appendStatement(helper.constructForLoop(counter, upperBound, body, counterType));
      }
      else 
      {
        // assume that we can reuse initializer expression (TODO check what happens if none exists)
        assert(upperBoundVarName);
        SageInterface::appendStatement(helper.constructForLoop(counter, SageBuilder::buildVarRefExp(*upperBoundVarName+"_"+std::to_string(i)), body, SageBuilder::buildIntType()));
      }

      baseType = isSgArrayType(baseType)->get_base_type();
      SageBuilder::pushScopeStack(body);
    }

    add_equal_block(arrVS, arrVP, baseType, pTypeMgr);

    for(unsigned int i=1; i<dimLen.size(); i++)
    {
      SageBuilder::popScopeStack();
      delete dimLen[i];
    }
  }

  void EqualityCheckGen::add_equality_ptr(SgExpression* varS, SgExpression* varP, SgType* type, TypeManager* pTypeMgr)
  {
    if(isSgPointerType(type))
    {
      SgPointerType* ptrType = isSgPointerType(type);
      SgBasicBlock* body = SageBuilder::buildBasicBlock_nfi();

      std::string* upperBoundVarName = NULL;

      if(varNameToSizeVar->find(varP->unparseToString())!= varNameToSizeVar->end())
      {
        upperBoundVarName = &((*varNameToSizeVar)[varP->unparseToString()]);
      }
      assert(upperBoundVarName);

      SgName counter = SgName(prefixForCounter+std::to_string(count++));
      SageInterface::appendStatement(helper.constructForLoop(counter, SageBuilder::buildVarRefExp(*upperBoundVarName), body, SageBuilder::buildIntType()));

      SageBuilder::pushScopeStack(body);
      add_equal_block(SageBuilder::buildPntrArrRefExp(varS, SageBuilder::buildVarRefExp(counter)), SageBuilder::buildPntrArrRefExp(varP, SageBuilder::buildVarRefExp(counter)), ptrType->get_base_type(), pTypeMgr);
      SageBuilder::popScopeStack();
    }
    else
    {
      // TODO handle typedefs, etc.
      assert(0);
    }
  }

  void EqualityCheckGen::add_equality_struct(SgExpression* exprS, SgExpression* exprP, SgType* type, TypeManager* pTypeMgr)
  {
    SgClassDeclaration* decl =  isSgClassDeclaration(isSgClassType(type)->get_declaration());
    if(!decl->get_definition())
    {
      decl = pTypeMgr->getClassDeclaration(decl->get_name().getString(), true);
    }

    std::vector<SgDeclarationStatement*>& classMembers = decl->get_definition()->get_members();
    for(std::vector<SgDeclarationStatement*>::iterator memIt = classMembers.begin(); memIt != classMembers.end(); memIt++)
    {
      std::vector<SgInitializedName*>& vars = isSgVariableDeclaration(*memIt)->get_variables();
      for(std::vector<SgInitializedName*>::iterator varIt = vars.begin(); varIt != vars.end(); varIt++)
      {  
        add_equal_block(SageBuilder::buildDotExp(exprS, SageBuilder::buildVarRefExp((*varIt)->get_name())), SageBuilder::buildDotExp(exprP, SageBuilder::buildVarRefExp((*varIt)->get_name())), (*varIt)->get_type(), pTypeMgr);
      }
    }
  }

  EqualType EqualityCheckGen::get_eq_type(SgType* varType, TypeManager* pTypeMgr) 
  {
    if(is_simple_eq_type(varType))
    {
      return SIMPLE;
    }
    if(isSgArrayType(varType))
    {
      return ARRAY;
    }
    if(SageInterface::isPointerType(varType))
    {
      return POINTER;
    }
    if(isSgClassType(varType))
    {
      SgClassDeclaration* decl =  isSgClassDeclaration(isSgClassType(varType)->get_declaration());
      if(!decl->get_definition())
      {
        decl = pTypeMgr->getClassDeclaration(decl->get_name().getString(), true);
      }
      assert(decl->get_definition());

      if(decl->get_class_type() == SgClassDeclaration::class_types::e_struct)
      {
        return STRUCT;
      }
    }
    return UNSUPPORTED;
  }

  bool EqualityCheckGen::is_simple_eq_type(SgType* varType)
  {
    // might forget some types, e.g., primitive types hidden by typedef?
    return varType->isPrimativeType();
  }

