//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2023 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "AbstractCheckGen.h"

  void AbstractCheckGen::delete_tmp_checker_file(std::string& checker_file) 
  {
      std::string tempFileName = dummyFilePrefix + checker_file;
      std::remove(tempFileName.c_str()); // delete temporary checker file
  }

  void AbstractCheckGen::write_checker_file(SgSourceFile* file, std::string fileName, SgProject* seqProject)
  {
    file->set_unparse_output_filename (fileName);
    assert(file->get_project() && seqProject);
    // check generated program(s)
    AstTests::runAllTests(file->get_project());   
    // Generate source code from AST, ensure that modified sequential project okay -> renaming okay 
    AstTests::runAllTests(seqProject);
    file->unparse();
  }
  
  void AbstractCheckGen::init_include_Mgr(SgSourceFile* seqProg, SgSourceFile* parProg)
  {
    inclMgr.reset();
    inclMgr.include_assert();
    inclMgr.get_includes_in(seqProg);
    inclMgr.get_includes_in(parProg);
  }

