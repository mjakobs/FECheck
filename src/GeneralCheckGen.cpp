//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <assert.h>
#include <limits.h>
#include <cstdio>

#include "GeneralCheckGen.h"
#include "PECConstants.h"
#include "InitializationGen.h"
#include "EqualityCheckGen.h"

#include "DefUseAnalysis.h"
#include "DefUseAnalysis_perFunction.h"

// Currently, do not provide aliasing support or support for function pointers.
// Sometimes, renaming of variables does not work out, e.g., problems with renaming when variable is used in dimension in array declaration because it is used in the type and no reference to name exist
// TODO properly deal with arrays, e.g., arrays and pointers should always be initialized (cannot be sure if completely read)

  void GeneralCheckGen::extract_nonshared_vars(std::set<std::string>& non_shared_vars, SgSourceFile* seqProg, SgSourceFile* parProg) 
  {
    std::set<std::string> seqVar;
    std::set<std::string> parVar;
    Rose_STL_Container<SgNode*> seqVars = NodeQuery::querySubTree (seqProg, V_SgVariableDeclaration);
    Rose_STL_Container<SgNode*> parVars = NodeQuery::querySubTree (parProg, V_SgVariableDeclaration);
    for (Rose_STL_Container<SgNode*>::iterator i = seqVars.begin(); i != seqVars.end(); i++)
    {
      SgInitializedNamePtrList declVars = isSgVariableDeclaration(*i)->get_variables();
      for(SgInitializedNamePtrList::iterator vars = declVars.begin(); vars != declVars.end(); vars++)
      {
        if(!isSgClassDefinition((*vars)->get_scope()))
        {
          seqVar.insert((*vars)->get_name().getString());
        }
      }
    }
    for (Rose_STL_Container<SgNode*>::iterator i = parVars.begin(); i != parVars.end(); i++)
    {
      SgInitializedNamePtrList declVars = isSgVariableDeclaration(*i)->get_variables();
      for(SgInitializedNamePtrList::iterator vars = declVars.begin(); vars != declVars.end(); vars++)
      {
        if(!isSgClassDefinition((*vars)->get_scope()))
        {
          parVar.insert((*vars)->get_name().getString());
        }
      }
    }
    
    for(std::set<std::string>::iterator i = seqVar.begin(); i!= seqVar.end();i++) 
    {
        if(parVar.find(*i) == parVar.end())
        {
          non_shared_vars.insert(*i);
        }
    }
    
    for(std::set<std::string>::iterator i = parVar.begin(); i!= parVar.end();i++) 
    {
        if(seqVar.find(*i) == seqVar.end())
        {
          non_shared_vars.insert(*i);
        }
    }
  }


  void GeneralCheckGen::generate_checker_programs(SgSourceFile* seqProg, SgSourceFile* parProg, std::string checkFilePrefix)
  {

    if(contains_openmp_synch_stmt(parProg))
    {
      std::cout << "Parallelized program uses explicit synchronization. Need to perform deadlock and livelock check." << std::endl;
    }

    init_include_Mgr(seqProg, parProg);
    typeMgr.init_type_Mgr(seqProg, parProg);

    int numChecks = get_num_checks(seqProg);
    
    std::set<std::string> non_shared_vars;
    extract_nonshared_vars(non_shared_vars, seqProg, parProg);

    for(int i = 1; i<=numChecks; i++)
    {
      generate_single_checker_program(seqProg, parProg, non_shared_vars, checkFilePrefix + "_" + std::to_string(i) + ".c", i);
      std::string tempFileName = dummyFilePrefix + checkFilePrefix + "_" + std::to_string(i) + ".c";
      std::remove(tempFileName.c_str()); // delete temporary checker file
    }

  }

  void GeneralCheckGen::generate_single_checker_program(SgSourceFile* seqProg, SgSourceFile* parProg, std::set<std::string>& non_shared_vars, std::string fileName, int checkID)
  { 
    SgSourceFile* checkerFile = SageBuilder::buildSourceFile(fileName); 

    std::vector<SgInitializedName*>* renamedVars = build_main_for_id(checkerFile, seqProg, parProg, non_shared_vars, checkID);

    inclMgr.insert_includes(checkerFile);
    write_checker_file(checkerFile, fileName, seqProg->get_project());

    // required to deal with multiple parallelizations correctly
    undo_renaming(renamedVars);
    delete renamedVars;

    protoMgr.reset();
    typeMgr.reset_types();
  }

  std::vector<SgInitializedName*>* GeneralCheckGen::build_main_for_id(SgSourceFile* file, SgSourceFile* seqProg, SgSourceFile* parProg, std::set<std::string>& non_shared_vars, int checkID)
  {
    std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*> startEndSeq = get_start_end_pragmas(seqProg, checkID);
    std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*> startEndPar = get_start_end_pragmas(parProg, checkID);
    return build_main(file, seqProg, parProg, non_shared_vars, startEndSeq, startEndPar);
    
  }

  std::vector<SgInitializedName*>* GeneralCheckGen::build_main(SgSourceFile* file, SgSourceFile* seqProg, SgSourceFile* parProg, std::set<std::string>& non_shared_vars, std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*>& startEndSeq, std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*>& startEndPar)
  {
    
    SageBuilder::pushScopeStack(file->get_globalScope());

    SgFunctionDeclaration* mainFun = SageBuilder::buildDefiningFunctionDeclaration (SgName("main"), SageBuilder::buildIntType(),  SageBuilder::buildFunctionParameterList());
    SageInterface::updateDefiningNondefiningLinks (mainFun,file->get_globalScope());
    SageInterface::appendStatement(mainFun);

    SgBasicBlock* mainBody = mainFun->get_definition()->get_body();
    // NOTE: Do not manually pass scope parameters (does not work), but always use SageBuilder::pushScopeStack(SgScopeStatement*); and afterwards call popScopeStack();
    SageBuilder::pushScopeStack(mainBody);

    std::vector<std::vector<SgFunctionCallExp*>> nondetCallsSeq, nondetCallsPar;
    for(int i=0;i<NondetTypeSize;i++)
    {
      nondetCallsSeq.push_back(std::vector<SgFunctionCallExp*>());
      nondetCallsPar.push_back(std::vector<SgFunctionCallExp*>());
    }

    // add sequential code
    std::map<SgInitializedName*, VariableClassification>* varsSeq = add_code_between_pragmas(startEndSeq, nondetCallsSeq, true);
    // SgStatement* afterSeqCode = SageInterface::getLastStatement(mainBody);
    // TODO add comment to checker program at the beginning of parallel segment
    // add parallel code
    std::map<SgInitializedName*, VariableClassification>* varsPar = add_code_between_pragmas(startEndPar, nondetCallsPar, false);
    SgStatement* afterInitStmt = SageInterface::getFirstStatement (mainBody);
      
    handle_nondet_calls(nondetCallsSeq, nondetCallsPar, file->get_globalScope());

    std::vector<std::tuple<std::string, std::string, SgType*>> reqInit;
    std::vector<std::tuple<std::string, std::string, SgType*>> reqEqCheck;
    std::map<std::string, SgInitializedName*> varInArrayDeclOrConstInit;

    std::map<std::string, std::string> varNameToSizeVar; 
    std::vector<SgVariableDeclaration*> sizeVarDeclForTypeUpdates;
    std::vector<SgInitializedName*>* rename_vars = match_vars(varsSeq, varsPar, SageInterface::getEnclosingScope(startEndSeq.second),  SageInterface::getEnclosingScope(startEndPar.second), reqInit, reqEqCheck, varInArrayDeclOrConstInit, varNameToSizeVar, non_shared_vars); 

    // renaming of sequential code
    rename_variables(rename_vars); 

    deallocate_block = SageBuilder::buildBasicBlock_nfi ();
    // assume variables are renamed already
    add_declarations(varsSeq, varsPar, varInArrayDeclOrConstInit, varNameToSizeVar, sizeVarDeclForTypeUpdates);
    // prepend variables required for arrays for other declarations
    add_declarations_for_and_init(varInArrayDeclOrConstInit, protoMgr, SageInterface::getFirstStatement (mainBody), varNameToSizeVar, sizeVarDeclForTypeUpdates, seqProg->getFileName());

    // initialize size variables for changed array types at the beginning of the main method
    for(std::vector<SgVariableDeclaration*>::iterator varDecl = sizeVarDeclForTypeUpdates.begin(); varDecl!=sizeVarDeclForTypeUpdates.end(); varDecl++)
    {
        SageInterface::prependStatement(*varDecl);
    }
    
    gen_and_add_var_init(reqInit, *afterInitStmt, protoMgr, varInArrayDeclOrConstInit, varNameToSizeVar);
    inspect_types_for_vars_declared_in_segment(varsSeq, varsPar);

    // no statement must be prepended before afterInitStmt, otherwise comment is moved in front of such a statement
    SageInterface::addMessageStatement (afterInitStmt, "// (First) sequential code segment");

    gen_and_add_eq_check(reqEqCheck, varNameToSizeVar);

    // deallocation
    if(deallocate_block->get_statements().size()>0)
    {
      SageInterface::appendStatement(deallocate_block);
    }
    else
    {
      SageInterface::deepDelete(deallocate_block);
    }
    SgReturnStmt* returnSt = SageBuilder::buildReturnStmt_nfi(SageBuilder::buildIntVal(0));
    SageInterface::appendStatement(returnSt);

    SageBuilder::popScopeStack ();

    protoMgr.insert_function_declarations();
    typeMgr.insert_types(file);

    SageBuilder::popScopeStack ();

    delete varsSeq;
    delete varsPar;

    return rename_vars;
  }

  SgFunctionCallExp* GeneralCheckGen::gen_nondet_replace_call(SgFunctionDeclaration* nondetFun, bool isSeq)
  {
    std::vector<SgExpression*> params;
    params.push_back(SageBuilder::buildIntVal(isSeq? turnSeq: turnPar));
    return SageBuilder::buildFunctionCallExp (SageBuilder::buildFunctionRefExp(nondetFun), SageBuilder::buildExprListExp (params));
  }

  SgStatement* GeneralCheckGen::build_nondet_assign(SgExpression* lhs, SgFunctionDeclaration* nondetFun)
  {
    return SageBuilder::buildAssignStatement(lhs, SageBuilder::buildFunctionCallExp (SageBuilder::buildFunctionRefExp(nondetFun), SageBuilder::buildExprListExp_nfi()));
  }

  SgExpression* GeneralCheckGen::build_array_subscript(std::string& arrName, SgExpression* subscript)
  {
    return SageBuilder::buildPntrArrRefExp(SageBuilder::buildVarRefExp(SgName(arrName)), subscript); 
  }

  void GeneralCheckGen::build_nondet_replace_body(SgBasicBlock* body, SgFunctionDeclaration* nondetFun)
  {
    SgType* return_type = nondetFun->get_type_syntax()->get_return_type();
    SageBuilder::pushScopeStack (body);
 
    std::string resName = std::string("res");
    std::string indexS = std::string("indexS");
    std::string indexP = std::string("indexP");
    std::string arrName = std::string("arr");
    SgVariableDeclaration* seqCount;
    SgVariableDeclaration* parCount;
    SgVariableDeclaration* res;
    SgVariableDeclaration* array;

    seqCount = SageBuilder::buildVariableDeclaration (indexS, SageBuilder::buildUnsignedIntType(), SageBuilder::buildAssignInitializer (SageBuilder::buildUnsignedIntVal(0), SageBuilder::buildUnsignedIntType()));
    SageInterface::setStatic(seqCount);
    SageInterface::appendStatement(seqCount);

    parCount = SageBuilder::buildVariableDeclaration (indexP, SageBuilder::buildUnsignedIntType(), SageBuilder::buildAssignInitializer (SageBuilder::buildUnsignedIntVal(0), SageBuilder::buildUnsignedIntType()));
    SageInterface::setStatic(parCount);
    SageInterface::appendStatement(parCount);

    SgArrayType* arrType = SageBuilder::buildArrayType(return_type, SageBuilder::buildUnsignedIntVal(UINT_MAX)); // max unsigned int val
    array = SageBuilder::buildVariableDeclaration (arrName, arrType);
    SageInterface::setStatic(array);
    SageInterface::appendStatement(array);

    res = SageBuilder::buildVariableDeclaration (resName, return_type);
    SageInterface::appendStatement(res);

    // sequential code execution
    SgBasicBlock* seqBlock = SageBuilder::buildBasicBlock();
    SageBuilder::pushScopeStack (seqBlock);
    SageInterface::appendStatement(build_nondet_assign(SageBuilder::buildVarRefExp(SgName(resName)), nondetFun));
    SgBasicBlock* seqBlockIf = SageBuilder::buildBasicBlock();
    SageBuilder::pushScopeStack (seqBlockIf);
    SageInterface::appendStatement(SageBuilder::buildAssignStatement(build_array_subscript(arrName, SageBuilder::buildVarRefExp(SgName(indexS))), SageBuilder::buildVarRefExp(SgName(resName))));
    SageInterface::appendStatement(SageBuilder::buildExprStatement(SageBuilder::buildPlusPlusOp(SageBuilder::buildVarRefExp(SgName(indexS)),SgUnaryOp::postfix)));
    SageBuilder::popScopeStack ();
    SageInterface::appendStatement(SageBuilder::buildIfStmt (SageBuilder::buildExprStatement(SageBuilder::buildNotEqualOp (SageBuilder::buildVarRefExp(SgName(indexS)), SageBuilder::buildUnsignedIntVal(UINT_MAX))) , seqBlockIf, NULL));
    SageBuilder::popScopeStack ();

    // parallelized code execution
    SgBasicBlock* parBlock = SageBuilder::buildBasicBlock();
   SageBuilder::pushScopeStack (parBlock);
    SgBasicBlock* parBlockIf = SageBuilder::buildBasicBlock();
    SageBuilder::pushScopeStack (parBlockIf);
    SageInterface::appendStatement(SageBuilder::buildAssignStatement(SageBuilder::buildVarRefExp(SgName(resName)), build_array_subscript(arrName, SageBuilder::buildVarRefExp(SgName(indexP)))));
    SageInterface::appendStatement(SageBuilder::buildExprStatement(SageBuilder::buildPlusPlusOp(SageBuilder::buildVarRefExp(SgName(indexP)), SgUnaryOp::postfix))); 
    SageBuilder::popScopeStack ();
    SgBasicBlock* parBlockElse = SageBuilder::buildBasicBlock();
    SageBuilder::pushScopeStack (parBlockElse);
    SageInterface::appendStatement(build_nondet_assign(SageBuilder::buildVarRefExp(SgName(resName)), nondetFun));
    SageBuilder::popScopeStack ();
    SageInterface::appendStatement(SageBuilder::buildIfStmt (SageBuilder::buildExprStatement(SageBuilder::buildLessThanOp (SageBuilder::buildVarRefExp(SgName(indexP)), SageBuilder::buildVarRefExp(SgName(indexS)))) , parBlockIf, parBlockElse));
    SageBuilder::popScopeStack ();

    // select between sequential and parallelized code execution
    SageInterface::appendStatement(SageBuilder::buildIfStmt (SageBuilder::buildExprStatement(SageBuilder::buildEqualityOp (SageBuilder::buildVarRefExp(SgName(turnVar)), SageBuilder::buildIntVal(turnSeq))) , seqBlock, parBlock));

    SageInterface::appendStatement(SageBuilder::buildReturnStmt(SageBuilder::buildVarRefExp(SgName(resName))));
    
    SageBuilder::popScopeStack ();
  }

  void GeneralCheckGen::handle_nondet_calls(std::vector<std::vector<SgFunctionCallExp*>>& nondetCallsSeq, std::vector<std::vector<SgFunctionCallExp*>>& nondetCallsPar, SgGlobal* globalScope)
  {
    assert(nondetCallsSeq.size() == nondetCallsPar.size() && nondetCallsSeq.size() == NondetTypeSize);
    SgFunctionDeclaration* nondetFun;
    SgScopeStatement* top =  SageBuilder::topScopeStack ();

    for(unsigned int i=0; i<nondetCallsSeq.size(); i++)
    {
      if(nondetCallsSeq[i].size()>0 && nondetCallsPar[i].size()>0) 
      {
        const std::string funcName = std::string(vnondetPrefix)+ std::string(helper.get_nondet_fun_suffix((NondetType) i)) + std::string(renameFunSuffix);
        SageBuilder::popScopeStack ();
        SgInitializedName* paramDecl = SageBuilder::buildInitializedName(SgName(turnVar), SageBuilder::buildIntType()); 
        nondetFun = SageBuilder::buildDefiningFunctionDeclaration (SgName(funcName), helper.get_type((NondetType) i),SageBuilder::buildFunctionParameterList(paramDecl));  
        SageInterface::updateDefiningNondefiningLinks (nondetFun, globalScope);      
        SageInterface::appendStatement(nondetFun);
        build_nondet_replace_body(nondetFun->get_definition()->get_body(), SageInterface::getFunctionDeclaration(nondetCallsSeq[i][0]));
        SageBuilder::pushScopeStack (top);

        protoMgr.add_prototype(SageBuilder::buildNondefiningFunctionDeclaration (nondetFun)); 
        protoMgr.add_verifier_nondet((NondetType) i);

        for(std::vector<SgFunctionCallExp*>::iterator callIt = nondetCallsSeq[i].begin(); callIt != nondetCallsSeq[i].end(); callIt++)
        {
          SageInterface::replaceExpression(*callIt, gen_nondet_replace_call(nondetFun, true), false);
        }
        for(std::vector<SgFunctionCallExp*>::iterator callIt = nondetCallsPar[i].begin(); callIt != nondetCallsPar[i].end(); callIt++)
        {
          SageInterface::replaceExpression(*callIt, gen_nondet_replace_call(nondetFun, false), false); 
        }
      }
    }
  }


  bool GeneralCheckGen::analyze_function_calls_in(SgStatement* stmt, std::set<SgFunctionDeclaration*>& analyzedFun, std::vector<std::vector<SgFunctionCallExp*>>& nondetCalls, std::map<SgInitializedName*, VariableClassification>& vars, SgSourceFile* file, bool isInSeq, bool nonRecursiveCall, std::set<SgFunctionCallExp*>& callReplacements)
  {
    SgFunctionCallExp* call;
    SgFunctionDeclaration* funDecl;
    SgFunctionDeclaration* funDeclNew;
    std::string funName;

    bool callsExternFunModGlobals = false;
    
    Rose_STL_Container<SgNode*> funCalls = NodeQuery::querySubTree (stmt, V_SgFunctionCallExp );
    for (Rose_STL_Container<SgNode*>::iterator i = funCalls.begin(); i != funCalls.end(); i++)
    {
      call = isSgFunctionCallExp(*i); 
      if(!call) continue; // work around
      funDecl = SageInterface::getFunctionDeclaration(call); // typically a prototype

      if(nonRecursiveCall)
      {
        std::vector<SgExpression*> params = call->get_args()->get_expressions();
        std::vector<SgType*> paramTypes = funDecl->get_type()->get_arguments();
        SgInitializedName* decl;
        for(unsigned int i=0; i< paramTypes.size(); i++)
        {
          if(params[i] && !SageInterface::isScalarType(paramTypes[i])) // TODO should handle non scalar types more precisely
          {
            std::vector<SgVarRefExp*> varRefs;
            SageInterface::collectVarRefs (params[i], varRefs);
            for(std::vector<SgVarRefExp*>::iterator paramIt = varRefs.begin(); paramIt != varRefs.end(); paramIt++)
            {
              decl = (*paramIt)->get_symbol()->get_declaration();
              if(!isSgClassDefinition((*paramIt)->get_symbol()->get_scope()))
              {
                if(vars.find(decl) == vars.end())
                {
                  vars[decl] =  VariableClassification();
                }
                vars[decl].set_modified();
                vars[decl].set_used_before();
              }
            }
          }
        }
                
      }

      if(funDecl) // funDecl == NULL should be a function pointer
      {
        if(SageInterface::isExtern(funDecl) || !typeMgr.getFunDeclaration(funDecl->get_name().getString(), isInSeq)) // in the latter implementation in a different C file, possibly library
        {
          // external
          NondetType asNondet = helper.get_nondet_type(funDecl->get_name());
          if(asNondet != UNKNOWN && funDecl->get_type_syntax()->get_argument_list()->get_arguments().size()==0 && is_same_type(funDecl->get_type_syntax()->get_return_type(), helper.get_type(asNondet)))
          {
            nondetCalls[asNondet].push_back(call);
          }
          else 
          {
            if(!is_known_pure_external(funDecl->get_name()))
            {
 
              callsExternFunModGlobals = true;
            }

            if(funDecl->get_file_info()->get_filenameString().compare(file->get_file_info()->get_filenameString())==0)
            {
              funDeclNew = SageBuilder::buildNondefiningFunctionDeclaration(funDecl->get_name(), funDecl->get_type()->get_return_type(), funDecl->get_parameterList());
              SageInterface::setExtern(funDeclNew);
              protoMgr.add_prototype(funDeclNew);
            }
          }

        }
        else
        {
          if(analyzedFun.insert(funDecl).second)
          { 
            funName = funDecl->get_name().getString();
            
            if(callReplacements.find(call) == callReplacements.end())
            {
              funName = isInSeq? funDecl->get_name().getString()+renameFunSuffix : funName;;
              if(!isInSeq || !typeMgr.getFunDeclaration(funName, isInSeq)) 
              {
                copy_function(funDecl, vars, isInSeq);
              }
            }
                                    
            callsExternFunModGlobals = analyze_function_calls_in(typeMgr.getFunDeclaration(funName, isInSeq)->get_definition()->get_body(), analyzedFun, nondetCalls, vars, file, isInSeq, false, callReplacements) || callsExternFunModGlobals;
          }

          if(isInSeq)
          {
           replace_function_call_for_seq(call, callReplacements, vars);
          }
        }
      } 
      else
      {
        assert(0); // currently do not handle function pointers
      }
    }

    return callsExternFunModGlobals;
  }

  void GeneralCheckGen::copy_function(SgFunctionDeclaration* funDecl, std::map<SgInitializedName*, VariableClassification>& vars, bool isInSeq)
  { 
    SgScopeStatement* top =  SageBuilder::topScopeStack ();
    SgFunctionDeclaration* funDeclCopy;

    // assume that function calls always refer to prototype 
    assert(funDecl->get_definition() == NULL);
    SageBuilder::popScopeStack ();

    funDecl = typeMgr.getFunDeclaration(funDecl->get_name().getString(), isInSeq);
    assert(funDecl);

    
    //funDeclCopy = SageBuilder::buildDefiningFunctionDeclaration (isInSeq? SgName(funDecl->get_name()+renameFunSuffix) : funDecl->get_name(), funDecl->get_type()->get_return_type(), SageInterface::deepCopy< SgFunctionParameterList>(funDecl->get_parameterList())); 
    SgShallowCopy copyHelper;
    funDeclCopy = SageBuilder::buildDefiningFunctionDeclaration (isInSeq? SgName(funDecl->get_name()+renameFunSuffix) : funDecl->get_name(), funDecl->get_type()->get_return_type(), isSgFunctionParameterList(funDecl->get_parameterList()->copy(copyHelper))); // shallow copy of parameters to account for renaming in recursive calls
    SageInterface::appendStatement(funDeclCopy);
    protoMgr.add_prototype(SageBuilder::buildNondefiningFunctionDeclaration (funDeclCopy)); 
    SageInterface::updateDefiningNondefiningLinks (funDeclCopy, SageInterface::getGlobalScope(funDeclCopy));

    // copy function body
    SageBuilder::pushScopeStack (funDeclCopy->get_definition()->get_body());
    SageInterface::appendStatement(SageInterface::deepCopy<SgStatement>(funDecl->get_definition()->get_body()));
    SageBuilder::popScopeStack ();

    SageBuilder::pushScopeStack (top);
    typeMgr.addFunDeclaration(funDeclCopy->get_name().getString(), funDeclCopy, isInSeq);
    
    // check modified vars
    std::vector<SgVarRefExp*> varRefs;
    SgInitializedName* decl;
    SageInterface::collectVarRefs (funDeclCopy->get_definition()->get_body(), varRefs);
    for(std::vector<SgVarRefExp*>::iterator bodyIt = varRefs.begin(); bodyIt != varRefs.end(); bodyIt++)
    {
      decl = (*bodyIt)->get_symbol()->get_declaration();
      if(helper.is_global(decl->get_scope())) // TODO should handle more precisely
      {
        if(vars.find(decl) == vars.end())
        {
          vars[decl] =  VariableClassification();
        }
        vars[decl].set_modified();
        vars[decl].set_used_before();
      }
    }
  }

  bool GeneralCheckGen::is_known_pure_external(std::string funName)
  {
    if(inclMgr.contains_stdlib() && inStdLib.find(funName) != inStdLib.end())
    {
        return true;
    }
    if(inclMgr.contains_math() && inMath.find(funName) != inMath.end())
    {
        return true;
    }
    if(inclMgr.contains_complex() && inComplex.find(funName) != inComplex.end())
    {
        return true;
    }
    return false;
  }

  void GeneralCheckGen::replace_function_call_for_seq(SgFunctionCallExp* call, std::set<SgFunctionCallExp*>& callReplacements, std::map<SgInitializedName*, VariableClassification>& vars)
  { 
    if(callReplacements.find(call) != callReplacements.end())
    {
      return;
    }
  
    SgFunctionCallExp* innerCall;
    std::vector<SgExpression*> params;
    for(std::vector<SgExpression*>::iterator  argIt = call->get_args()->get_expressions().begin(); argIt !=  call->get_args()->get_expressions().end(); argIt++)
    { 
      params.push_back(SageInterface::deepCopy<SgExpression>(*argIt));     
    }
    
    if(!typeMgr.getFunDeclaration(call->get_function()->unparseToString()+renameFunSuffix, true))
    {
      copy_function(SageInterface::getFunctionDeclaration(call), vars, true);
    }

    SgFunctionCallExp* newCall = SageBuilder::buildFunctionCallExp(SageBuilder::buildFunctionRefExp(typeMgr.getFunDeclaration(call->get_function()->unparseToString()+renameFunSuffix, true)), SageBuilder::buildExprListExp (params));
    callReplacements.insert(newCall);
    SageInterface::replaceExpression(call, newCall, false);      

    for(std::vector<SgExpression*>::iterator itExp = params.begin(); itExp!= params.end(); itExp++)
    {
      Rose_STL_Container<SgNode*> funCalls = NodeQuery::querySubTree (*itExp, V_SgFunctionCallExp);
      for (Rose_STL_Container<SgNode*>::iterator i = funCalls.begin(); i != funCalls.end(); i++)
      {
        innerCall = isSgFunctionCallExp(*i);
        if(!innerCall || callReplacements.find(innerCall) != callReplacements.end()) continue; // work around
        assert(SageInterface::getFunctionDeclaration(innerCall)); // currently no support of function pointer
        if(SageInterface::isExtern(SageInterface::getFunctionDeclaration(innerCall))) continue;
        replace_function_call_for_seq(innerCall, callReplacements, vars); // TODO does not work for unseen calls 
      } 
    }
  }

  void GeneralCheckGen::insert_all_global_vars(SgGlobal* global_scope, std::map<SgInitializedName*, VariableClassification>& vars)
  {
    SgVariableDeclaration* varDecl;
    assert(global_scope);

    for(std::vector<SgDeclarationStatement*>::iterator globalIt = global_scope->get_declarations().begin(); globalIt != global_scope->get_declarations().end(); globalIt++)
    { 
      varDecl = isSgVariableDeclaration(*globalIt);
      if(!varDecl)
      {
        continue;
      }
      for(std::vector<SgInitializedName*>::iterator varIt = varDecl->get_variables().begin(); varIt != varDecl->get_variables().end(); varIt++) 
      { 
        if(!helper.shouldIgnoreGlobal((*varIt)->get_name().getString())) // filter global system variables, e.g., introduced by stdio.h
        { 
          /* conservative assumptions
           * (a) global variables might be modified by any function (internal or external)
           * (b) global variables are live, i.e., might be used by another function afterwards
           * (c) global variables are used before, i.e., do not check that code segments defines global variable before any function is called
           */
          insert_modified_var(*varIt, vars);
          vars[*varIt].set_live_after();
          vars[*varIt].set_used_before();
        }
      }
    }
  }

  void GeneralCheckGen::insert_modified_var(SgInitializedName* decl, std::map<SgInitializedName*, VariableClassification>& vars)
  {
    if(isSgClassDefinition(decl->get_scope()))
    {
      return;
    }
    if(vars.find(decl) == vars.end())
    {
      vars[decl] =  VariableClassification();
    }
    vars[decl].set_modified();
  }
  
  void GeneralCheckGen::insert_vars_modified_internally_before_function_calls_in(std::map<SgInitializedName*, VariableClassification>& vars, std::set<std::pair<SgInitializedName*, SgNode*>>& defsBefore, DefUseAnalysis* defuse, std::vector<SgNode*>& stmtsBetweenPragmas)
  {
    std::vector<std::pair<SgInitializedName*, SgNode*>> defs;
    for(std::vector<SgNode*>::iterator stmtIt = stmtsBetweenPragmas.begin(); stmtIt != stmtsBetweenPragmas.end(); stmtIt++)
    {
      Rose_STL_Container<SgNode*> fun_calls = NodeQuery::querySubTree (*stmtIt, V_SgFunctionCallExp);
      for (Rose_STL_Container<SgNode*>::iterator itFun = fun_calls.begin(); itFun != fun_calls.end(); itFun++)
      {
        defs = defuse->getDefMultiMapFor(*itFun);
        insert_vars_modified_internally(defsBefore, defs, vars, stmtsBetweenPragmas); 
      }
    }
  }

  void GeneralCheckGen::insert_vars_modified_internally(std::set<std::pair<SgInitializedName*, SgNode*>>& defsBefore, std::vector<std::pair<SgInitializedName*, SgNode*>>& defsAfter, std::map<SgInitializedName*, VariableClassification>& vars, std::vector<SgNode*>& stmtsBetweenPragmas) 
  {// only variable modification directly performed in code segment, not in some external function being called
   // does not always work properly, there seems to be problem with reaching definition analysis  
    for(std::vector<std::pair<SgInitializedName*, SgNode*>>::iterator it = defsAfter.begin(); it!=defsAfter.end(); it++) 
    { 
      if(defsBefore.find(*it) == defsBefore.end())
      {
        insert_modified_var(it->first, vars);
      }
      else if(vars.find(it->first)==vars.end() || !(vars[it->first].is_written()))
      {
        for(std::vector<SgNode*>::iterator stmtIt = stmtsBetweenPragmas.begin(); stmtIt != stmtsBetweenPragmas.end(); stmtIt++)
        {
          // account for loops
          if(*stmtIt == it->second || SageInterface::isAncestor(*stmtIt, it->second))
          { 
            insert_modified_var(it->first, vars);
            break;
          }
          else 
          {
            if(isSgArrayType(it->first->get_type()) || SageInterface::isPointerType(it->first->get_type()))
            {
               Rose_STL_Container<SgNode*> stmts = NodeQuery::querySubTree (*stmtIt, V_SgStatement);
               bool notFound = true;
               for (Rose_STL_Container<SgNode*>::iterator nodeIt = stmts.begin(); notFound && nodeIt != stmts.end(); nodeIt++)
               {
                   SgExpression* lhs = NULL;
                   if(!(SageInterface::isAssignmentStatement(*nodeIt, &lhs, NULL, NULL)))
                   {
                     if(isSgExprStatement(*nodeIt))
                     {
                       SgExpression* expr = isSgExprStatement(*nodeIt)->get_expression();
                       if(isSgPlusPlusOp(expr))
                       {
                           lhs = isSgPlusPlusOp(expr)->get_operand();
                       } else if(isSgMinusMinusOp(expr))
                       {
                           lhs = isSgMinusMinusOp(expr)->get_operand();
                       }
                     }
                   } 
                   
                   if(isSgPntrArrRefExp(lhs) != NULL) 
                   {
                       do
                       {
                           lhs = isSgPntrArrRefExp(lhs)->get_lhs_operand();
                       } while(isSgPntrArrRefExp(lhs) != NULL);
                       if(isSgVarRefExp(lhs) && isSgVarRefExp(lhs)->get_symbol()->get_declaration() == it->first)
                       {
                           notFound = false;
                           insert_modified_var(it->first, vars);
                       }       
                   }
                   else if (isSgPointerDerefExp(lhs) != NULL)
                   {
                       lhs = isSgPointerDerefExp(lhs)->get_operand();
                       if(isSgVarRefExp(lhs) && isSgVarRefExp(lhs)->get_symbol()->get_declaration() == it->first)
                       {
                           notFound = false;
                           insert_modified_var(it->first, vars);
                       }  
                   }
               } 
               if(!notFound)
               {
                   break;
               }       
            }
            SgType* typeStripped = it->first->get_type()->stripTypedefsAndModifiers(); 
            if(SageInterface::isStructType(typeStripped))
            {
              Rose_STL_Container<SgNode*> stmts = NodeQuery::querySubTree (*stmtIt, V_SgStatement);
               for (Rose_STL_Container<SgNode*>::iterator nodeIt = stmts.begin(); nodeIt != stmts.end(); nodeIt++)
               {
                   SgExpression* lhs = NULL;
                   if(SageInterface::isAssignmentStatement(*nodeIt, &lhs, NULL, NULL))
                   {
                       while(isSgDotExp(lhs)) {
                          lhs = isSgDotExp(lhs)->get_lhs_operand();
                       }
                       if(isSgVarRefExp(lhs) && isSgVarRefExp(lhs)->get_symbol()->get_declaration() == it->first)
                       {
                           insert_modified_var(it->first, vars);
                           break;
                       } 
                   }
               }
            }
          }
        }
      }
    }
    for(std::vector<SgNode*>::iterator stmtIt = stmtsBetweenPragmas.begin(); stmtIt != stmtsBetweenPragmas.end(); stmtIt++)
    {
        Rose_STL_Container<SgNode*> stmts = NodeQuery::querySubTree (*stmtIt, V_SgStatement);
        bool notFound = true;
        for (Rose_STL_Container<SgNode*>::iterator nodeIt = stmts.begin(); notFound && nodeIt != stmts.end(); nodeIt++)
        {
            SgExpression* lhs = NULL;
            if(!SageInterface::isAssignmentStatement(*nodeIt, &lhs, NULL, NULL))
            {
                if(isSgExprStatement(*nodeIt))
                {
                   SgExpression* expr = isSgExprStatement(*nodeIt)->get_expression();
                   if(isSgPlusPlusOp(expr))
                   {
                       lhs = isSgPlusPlusOp(expr)->get_operand();
                   } else if(isSgMinusMinusOp(expr))
                   {
                       lhs = isSgMinusMinusOp(expr)->get_operand();
                   }
                }
            }
            if(isSgVarRefExp(lhs)) 
            {
                insert_modified_var(isSgVarRefExp(lhs)->get_symbol()->get_declaration(), vars);
            }
        }
    }
  }

  std::map<SgInitializedName*, VariableClassification>* GeneralCheckGen::add_code_between_pragmas(std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*>& startEnd, std::vector<std::vector<SgFunctionCallExp*>>& nondetCalls, bool isInSeq)
  {
    std::set<SgVariableDeclaration*> varDecls;
    std::set<SgStatement*> seenStatements;
    std::map<SgInitializedName*, VariableClassification>* vars = new std::map<SgInitializedName*, VariableClassification>;
    std::map<std::string, SgInitializedName*> nameToDecl;

    std::set<SgFunctionDeclaration*> analyzedFun;
    std::set<SgFunctionCallExp*> seen;

    DefUseAnalysis* defuse = new DefUseAnalysis(SageInterface::getProject(startEnd.first));
    DefUseAnalysisPF* defusePF = new DefUseAnalysisPF(false, defuse);
    bool abort = false;
    SgFunctionDefinition* funDef = SageInterface::getEnclosingFunctionDefinition(startEnd.first, true);	
    defusePF->run(funDef, abort);
    assert(!abort);

    LivenessAnalysis* liveA = new LivenessAnalysis(false, defuse);
    abort = false;
    liveA->run(funDef, abort);
    assert(!abort);
  
    bool startEndSameScope = (startEnd.first->get_scope() == startEnd.second->get_scope());
    bool funsMayModGlobals = false;

    SgStatement* nodeCopy;
    SgStatement* node = SageInterface::getNextStatement(startEnd.first); 
    SgInitializedName* varDecl;
    std::vector<SgNode*> usesNodes, defsNodes, stmtsBetweenPragmas;
    bool onlyUseAndDefStmts;

    std::vector<std::pair<SgInitializedName*, SgNode*>> defs = defuse->getDefMultiMapFor(startEnd.first);
    std::set<std::pair<SgInitializedName*, SgNode*>> defsBefore;
    defsBefore.insert(defs.begin(), defs.end());
    defs = defuse->getDefMultiMapFor(startEnd.second);

    SgFunctionDefinition* encloseFun = SageInterface::getEnclosingFunctionDefinition(startEnd.first);
    std::set<SgInitializedName*> pointerParam;
    if(encloseFun)
    {
      for(std::vector<SgInitializedName*>::iterator paramIt = encloseFun->get_declaration()->get_args().begin(); paramIt != encloseFun->get_declaration()->get_args().end(); paramIt++)
      {
        if(SageInterface::isPointerType((*paramIt)->get_type()) || isSgArrayType((*paramIt)->get_type()))
        {
          pointerParam.insert(*paramIt);
        }
      }
    }

    while(node != (startEnd.second))
    {
      assert(node);      
      stmtsBetweenPragmas.push_back(node);
      nodeCopy = SageInterface::deepCopy<SgStatement>(node);
      SageInterface::appendStatement(nodeCopy);

      Rose_STL_Container<SgNode*> decls = NodeQuery::querySubTree (node, V_SgVariableDeclaration);
      for (Rose_STL_Container<SgNode*>::iterator i = decls.begin(); i != decls.end(); i++)
      {
        varDecls.insert(isSgVariableDeclaration(*i));
      }
   
      // check if variable used before definition in code fragement
      // assumes that SgInitializedName stays the same between original and copied code
      std::vector<SgVarRefExp*> varRefs;
      // only references, not declarations
      SageInterface::collectVarRefs (node, varRefs); 

      for(std::vector<SgVarRefExp*>::iterator refIt = varRefs.begin(); refIt != varRefs.end(); refIt++)
      {
        if(isSgClassDefinition((*refIt)->get_symbol()->get_scope()))
        {
          continue; // TODO check if correct
        }
        varDecl = (*refIt)->get_symbol()->get_declaration();
        if(vars->find(varDecl) == vars->end())
        {
          (*vars)[varDecl] =  VariableClassification(); 
        }
        if(!((*vars)[varDecl].is_used_before()))
        {
          usesNodes = defuse->getUseFor(*refIt, varDecl);
          defsNodes = defuse->getDefFor(*refIt, varDecl);
          for(std::vector<SgNode*>::iterator useIt = usesNodes.begin(); useIt != usesNodes.end(); useIt++)
          {
            if((*useIt) == (*refIt)) { // *refIt is use node (see LiveVariableAnalysis implementation)
              if(defsNodes.size() == 0 || helper.is_global(varDecl->get_scope()))
              {
                (*vars)[varDecl].set_used_before();
              }
              onlyUseAndDefStmts = true;
              for(std::vector<SgNode*>::iterator defIt = defsNodes.begin(); defIt != defsNodes.end(); defIt++)
              {
                if(defsBefore.find(std::pair<SgInitializedName*, SgNode*>(varDecl, *defIt)) != defsBefore.end())
                {
                  (*vars)[varDecl].set_used_before();
                  onlyUseAndDefStmts = false;
                  break;
                }
                else if(SageInterface::getEnclosingStatement(*refIt) != SageInterface::getEnclosingStatement(*defIt)) // imprecise
                {
                  onlyUseAndDefStmts = false;
                }
              }
              if(onlyUseAndDefStmts)
              {
                (*vars)[varDecl].set_used_before();
              }
              break;
            }
          }
          
          // check whether variable occuring in segment has path without use/definition, then variable must be initialized
          if(!((*vars)[varDecl].is_used_before()))
          {
            for(std::vector<std::pair<SgInitializedName*, SgNode*>>::iterator finalDefIt = defs.begin(); finalDefIt != defs.end(); finalDefIt++)
            {
              if(varDecl == finalDefIt->first && defsBefore.find(*finalDefIt) != defsBefore.end())
              {
                  (*vars)[varDecl].set_used_before();
              }  
            }
          }
        }
      }

      decls = NodeQuery::querySubTree (nodeCopy, V_SgVariableDeclaration);
      for (Rose_STL_Container<SgNode*>::iterator i = decls.begin(); i != decls.end(); i++)
      {
        std::vector<SgInitializedName*> newDecls = isSgVariableDeclaration(*i)->get_variables();
        for(std::vector<SgInitializedName*>::iterator ndeclIt = newDecls.begin(); ndeclIt != newDecls.end(); ndeclIt++)
        {   
          if(!startEndSameScope || isSgVariableDeclaration(*i)->get_scope() == SageBuilder::topScopeStack ())
          { 
            assert(nameToDecl.find((*ndeclIt)->get_name()) == nameToDecl.end());
            nameToDecl[(*ndeclIt)->get_name()] = *ndeclIt;
          }
        }
      }

      funsMayModGlobals = analyze_function_calls_in(nodeCopy, analyzedFun, nondetCalls, *vars, SageInterface::getEnclosingSourceFile(node), isInSeq, true, seen) || funsMayModGlobals;
      insert_vars_modified_internally_before_function_calls_in(*vars, defsBefore, defuse, stmtsBetweenPragmas);

      node = SageInterface::getNextStatement(node);
    }
    
    insert_vars_modified_internally(defsBefore, defs, *vars, stmtsBetweenPragmas);

    if(funsMayModGlobals)
    {
      insert_all_global_vars(SageInterface::getGlobalScope(startEnd.first), *vars); // TODO become more precise here
    }


    std::vector<SgInitializedName*> to_delete;
    
    for(std::map<SgInitializedName*, VariableClassification>::iterator it = vars->begin(); it != vars->end(); it++)
    {
      // conservatively, make all global variables live 
      if(helper.is_global(it->first->get_scope()))
      {
        it->second.set_live_after();
      }
      // conservatively, make parameters that are pointers live 
      if(pointerParam.find(it->first)!=pointerParam.end())
      { 
        it->second.set_live_after();
      }
      // conservatively treat all types that we know of are not properly handed by live variable analysis
      if(missed_by_LV_analysis(it->first->get_type()))
      {
        it->second.set_live_after();
      }
      // remove local variables, adapt declared ones
      if(varDecls.find(isSgVariableDeclaration((*it).first->get_declaration())) != varDecls.end())
      {
        if(startEndSameScope && isSgVariableDeclaration((*it).first->get_declaration())->get_scope() != startEnd.first->get_scope())
        {
          to_delete.push_back(it->first);
        } 
        else
        {
          it->second.unset_declaration();
          it->second.set_modified();
          if(nameToDecl.find(it->first->get_name()) != nameToDecl.end())
          {
            it->second.set_decl(nameToDecl[it->first->get_name()]);
          }
        }
      }
    }

    for(std::vector<SgInitializedName*>::iterator remIt = to_delete.begin(); remIt != to_delete.end(); remIt++)
    {
      vars->erase(*remIt); 
    }

    // set live after (only local information, global variables and parameters passed might be live and are considered elsewhere)
    std::vector<SgInitializedName*> liveAfter = liveA->getOut(startEnd.second); 
    for(std::vector<SgInitializedName*>::iterator liveIt = liveAfter.begin(); liveIt != liveAfter.end(); liveIt++) // live variable analysis does not work properly sometimes
    { 
      if(vars->find(*liveIt) != vars->end())
      {
        (*vars)[*liveIt].set_live_after();
      } 
    }

    delete defusePF;
    delete defuse;
    delete liveA;

    return vars;
  }

  void GeneralCheckGen::rename_variables(std::vector<SgInitializedName*>* vars_to_rename)
  {
    std::string name;
    for(std::vector<SgInitializedName*>::iterator it = vars_to_rename->begin(); it != vars_to_rename->end(); it++)
    {
      name = (*it)->get_name().getString();
      SageInterface::set_name(*it, SgName(helper.add_rename_suffix(name))); 
      if(name.compare((*it)->get_name().getString()) == 0)
      {
        rename_variable(*it, helper.add_rename_suffix(name));
        assert(name.compare((*it)->get_name().getString()) != 0);
      }
    }
  }

  void GeneralCheckGen::rename_variable(SgInitializedName* decl, std::string new_name)
  {
    SgSymbol* symbol = decl->search_for_symbol_from_symbol_table();
    if(symbol)
    {
      assert(decl->get_scope()->symbol_exists(symbol));
      decl->get_scope()->remove_symbol(symbol);

      SgName newName = SgName(new_name);
      decl->get_scope()->insert_symbol(newName, symbol);

      // copied from SageInterface::set_name
      decl->set_name(newName);

      SgStatement* enclosingStatement = SageInterface::getEnclosingStatement(decl);
      ROSE_ASSERT(enclosingStatement != NULL);
      enclosingStatement->set_isModified(true);
      enclosingStatement->setTransformation();
    }
  }

  void GeneralCheckGen::undo_renaming(std::vector<SgInitializedName*>* vars_to_rename)
  {
    std::string name;
    for(std::vector<SgInitializedName*>::iterator it = vars_to_rename->begin(); it != vars_to_rename->end(); it++)
    {
      name = (*it)->get_name().getString();
      SageInterface::set_name(*it, SgName(helper.remove_rename_suffix((*it)->get_name().getString()))); 
      if(name.compare((*it)->get_name().getString()) == 0)
      {
        rename_variable(*it, helper.remove_rename_suffix((*it)->get_name().getString()));
        assert(name.compare((*it)->get_name().getString()) != 0);
      }	
    }
  }

  void GeneralCheckGen::inspect_types_for_vars_declared_in_segment(std::map<SgInitializedName*, VariableClassification>* varsSeq,  std::map<SgInitializedName*, VariableClassification>* varsPar)
  {
    for(std::map<SgInitializedName*, VariableClassification>::iterator it = varsSeq->begin(); it!=varsSeq->end();it++)
    {
      if(!it->second.must_declare())
      {
        typeMgr.inspect_type_for_definition(it->first->get_type(), true);
      }
    }
    
    
    for(std::map<SgInitializedName*, VariableClassification>::iterator it = varsPar->begin(); it!=varsPar->end();it++)
    {
      if(!it->second.must_declare())
      {
        typeMgr.inspect_type_for_definition(it->first->get_type(), false);
      }
    }
  }

  void GeneralCheckGen::add_declarations(std::map<SgInitializedName*, VariableClassification>* varsSeq,  std::map<SgInitializedName*, VariableClassification>* varsPar, std::map<std::string, SgInitializedName*>& reqForArrayDeclOrConstInit, std::map<std::string, std::string>& pVarNameToSizeVar, std::vector<SgVariableDeclaration*>& sizeVarDeclForTypeUpdates)
  {
    add_declarations_for(varsSeq, reqForArrayDeclOrConstInit, pVarNameToSizeVar, sizeVarDeclForTypeUpdates, true);
    add_declarations_for(varsPar, reqForArrayDeclOrConstInit, pVarNameToSizeVar, sizeVarDeclForTypeUpdates, false);
  }

  void GeneralCheckGen::add_declarations_for(std::map<SgInitializedName*, VariableClassification>* vars, std::map<std::string, SgInitializedName*>& reqForArrayDeclOrConstInit, std::map<std::string, std::string>& pVarNameToSizeVar, std::vector<SgVariableDeclaration*>& sizeVarDeclForTypeUpdates, bool isSeq)
  {
    std::string name;
    for(std::map<SgInitializedName*, VariableClassification>::iterator it = vars->begin(); it!=vars->end();it++)
    {
      if(it->second.must_declare() && reqForArrayDeclOrConstInit.find(it->first->get_name()) == reqForArrayDeclOrConstInit.end())
      {
        add_declaration((*it).first->get_name().getString(), (*it).first->get_type(), it->first->get_initializer(), (*it).first->get_scope(), pVarNameToSizeVar, sizeVarDeclForTypeUpdates, isSeq); 
      }
    }
  }

  void GeneralCheckGen::add_declarations_for_and_init(std::map<std::string, SgInitializedName*>& reqForArrayDeclOrConstInit, PrototypeManager& protoMgr, SgStatement* beforeStmt, std::map<std::string, std::string>& pVarNameToSizeVar, std::vector<SgVariableDeclaration*>& sizeVarDeclForTypeUpdates, std::string seqFileName)
  { 
    InitializationGen initGen(&protoMgr, &inclMgr, &typeMgr);
    
    std::vector<std::pair<std::string, SgInitializedName*>> sortedVars;
    for(std::map<std::string, SgInitializedName*>::iterator it = reqForArrayDeclOrConstInit.begin(); it!=reqForArrayDeclOrConstInit.end();it++)
    {
      sortedVars.push_back(std::pair<std::string, SgInitializedName*>(it->first, it->second));
    }
    
    std::pair<std::string, SgInitializedName*> tmp;
    Sg_File_Info* infoLeft;
    Sg_File_Info* infoRight;
    
    for(unsigned int i=0; i<sortedVars.size(); i++)
    {
      for(unsigned int j=1; j<sortedVars.size()-i; j++)
      {
        infoLeft = sortedVars[j-1].second->get_startOfConstruct();
        infoRight = sortedVars[j].second->get_startOfConstruct();
        if(infoLeft->get_line() < infoRight->get_line() || (infoLeft->get_line() == infoRight->get_line() && infoLeft->get_col()<infoRight->get_col()))
        {
          tmp = sortedVars[j];
          sortedVars[j] = sortedVars[j-1];
          sortedVars[j-1] = tmp;
        }
      }
    }

    for(std::vector<std::pair<std::string, SgInitializedName*>>::iterator it = sortedVars.begin(); it!=sortedVars.end();it++)
    {
       if(!helper.is_const_type(it->second->get_type()))
       {
           initGen.add_init_stmt(it->first, it->second->get_type(), beforeStmt, pVarNameToSizeVar); 
       }
       if(helper.is_global(it->second->get_scope()))
       { 
         SgScopeStatement* top =  SageBuilder::topScopeStack ();
         if(helper.is_const_type(it->second->get_type()) || it->second->get_initializer()) 
         { 
           assert(it->second->get_initializer());
           SageBuilder::popScopeStack ();
           SageInterface::prependStatement(SageBuilder::buildVariableDeclaration (it->second->get_name().getString(), it->second->get_type(), SageInterface::deepCopy<SgInitializer>(it->second->get_initializer()))); 
           SageBuilder::pushScopeStack (top);

           if(!helper.is_const_type(it->second->get_type()))
           {
               allocate_memory_and_save_sizes(it->second->get_name().getString(), it->second->get_type(), pVarNameToSizeVar, sizeVarDeclForTypeUpdates, it->second->getFilenameString().compare(seqFileName)==0); 
           }
         }
         else
         {
           add_declaration(it->second->get_name().getString(), it->second->get_type(), it->second->get_initializer(), it->second->get_scope(), pVarNameToSizeVar, sizeVarDeclForTypeUpdates,  it->second->getFilenameString().compare(seqFileName)==0); 
         }

       }
       else
       {
         add_declaration(it->second->get_name().getString(), it->second->get_type(), it->second->get_initializer(), it->second->get_scope(), pVarNameToSizeVar, sizeVarDeclForTypeUpdates, it->second->getFilenameString().compare(seqFileName)==0);
       }
    }
  }

  void GeneralCheckGen::allocate_memory_and_save_sizes(std::string& name, SgType* type, std::map<std::string, std::string>& pVarNameToSizeVar, std::vector<SgVariableDeclaration*>& sizeVarDeclForTypeUpdates, bool inSeq)
  { 
    deallocate_memory(name, type, pVarNameToSizeVar, inSeq);
    // TODO support nested data structures
    SgScopeStatement* top =  SageBuilder::topScopeStack ();
    if(isSgArrayType(type))
    {
      SgArrayType arrType = isSgArrayType(type);
      std::vector<SgExpression*> dim = SageInterface::get_C_array_dimensions(arrType);
      if(pVarNameToSizeVar.find(name) == pVarNameToSizeVar.end()) 
      {
        std::string nameR = helper.remove_rename_suffix(name);
        assert(name.compare(nameR+std::string(seqVarSuffix)) == 0);
        assert(pVarNameToSizeVar.find(nameR) != pVarNameToSizeVar.end());
      }
      else
      {
        std::string prefix = pVarNameToSizeVar[name]; 
        for(unsigned int i=1; i<dim.size();i++) // TODO check that starting with one is okay
        {
          SageInterface::const_int_expr_t  arr_size = SageInterface::evaluateConstIntegerExpression(dim[i]);
          if(isSgNullExpression(dim[i]))
          {
            delete dim[i];
            // variable declaration added in function for generating main
           sizeVarDeclForTypeUpdates.push_back(SageBuilder::buildVariableDeclaration (prefix+"_"+std::to_string(i), SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer (helper.generate_nondet_call(NondetType::INT, SageBuilder::buildIntType()))))           ;    
            protoMgr.add_verifier_nondet(NondetType::INT);              
          }
          else if(arr_size.hasValue_)
          {
            delete dim[i];
          }
          else
          {
            assert(dim[i]);
            SageBuilder::popScopeStack ();
            SageInterface::prependStatement(SageBuilder::buildVariableDeclaration(prefix+"_"+std::to_string(i), SageBuilder::buildIntType(), NULL)); 
            SageBuilder::pushScopeStack (top);
            SageInterface::prependStatement(SageBuilder::buildAssignStatement (SageBuilder::buildVarRefExp(SgName(prefix+"_"+std::to_string(i))), dim[i]));
          }
        }
      }
    }
    else if(SageInterface::isPointerType(type))
    {
      // pointer type
      std::string sizeVar;
      std::string nameR, nameLocal = std::string(name);
      bool onlyAllocate;
      SgStatement* afterStmt;
      if(pVarNameToSizeVar.find(nameLocal) == pVarNameToSizeVar.end())
      {
        nameR = helper.remove_rename_suffix(nameLocal);
        assert(nameLocal.compare(nameR+std::string(seqVarSuffix)) == 0);
        assert(pVarNameToSizeVar.find(nameR) != pVarNameToSizeVar.end());
        sizeVar = pVarNameToSizeVar[nameR];
        afterStmt = helper.malloc_assign(nameLocal, sizeVar, type);
        SageInterface::prependStatement(afterStmt);  
        onlyAllocate = true;  
      }
      else
      {
        sizeVar = pVarNameToSizeVar[nameLocal];
        afterStmt = helper.malloc_assign(nameLocal, sizeVar, type); 
        SageInterface::prependStatement(afterStmt); 
        SageBuilder::popScopeStack ();
        SageInterface::prependStatement(SageBuilder::buildVariableDeclaration (sizeVar, SageBuilder::buildIntType(), NULL)); 
        SageBuilder::pushScopeStack (top);
        SageInterface::prependStatement(helper.assign_nondet_val(SageBuilder::buildVarRefExp(SgName(sizeVar)), NondetType::INT, SageBuilder::buildIntType()));
        protoMgr.add_verifier_nondet(NondetType::INT);
        onlyAllocate = false;
      }

       int count = 0;
       SgBasicBlock* body;
       type = SageInterface::getElementType(type);
       std::vector<SgScopeStatement*> stack;
       std::string counter;
       SgName counterN;

       while(SageInterface::isPointerType(type))
       {
         counter = prefixForCounter + std::to_string(count++);
         nameLocal=nameLocal+"["+ counter +"]";
         if(onlyAllocate)
         {
           nameR=nameR+"["+counter +"]";
           sizeVar = pVarNameToSizeVar[nameR];
         }
         else
         {
           sizeVar = pVarNameToSizeVar[nameLocal]; 
           for(unsigned int i=0;i<=stack.size();i++)
           {
             SageBuilder::popScopeStack ();           
           }

           SageInterface::prependStatement(SageBuilder::buildVariableDeclaration (sizeVar, SageBuilder::buildIntType(), NULL)); 

           SageBuilder::pushScopeStack(top);
           SageInterface::prependStatement(helper.assign_nondet_val(SageBuilder::buildVarRefExp(SgName(sizeVar)), NondetType::INT, SageBuilder::buildIntType()));
           protoMgr.add_verifier_nondet(NondetType::INT);

           for(unsigned int i=0;i<stack.size();i++)
           {
             SageBuilder::pushScopeStack (stack[i]);         
           }
          }

         counterN = SgName(counter);
         body = SageBuilder::buildBasicBlock_nfi();
         SageInterface::insertStatement(afterStmt, helper.constructForLoop(counterN, SageBuilder::buildVarRefExp(SgName(sizeVar)), body, SageBuilder::buildIntType()), false);

         stack.push_back(body->get_scope());
         SageBuilder::pushScopeStack (stack.back());

         afterStmt = helper.malloc_assign(nameLocal, sizeVar, type);
         SageInterface::prependStatement(afterStmt); 
 
         type = SageInterface::getElementType(type);
       }

      for(unsigned int i=0;i<stack.size();i++)
      {
        SageBuilder::popScopeStack ();           
      }

      inclMgr.include_stdlib();
      inclMgr.include_stdio();
   
    }
    else if(isSgClassType(type))
    {   
        SgClassDeclaration* decl = isSgClassDeclaration(isSgClassType(type)->get_declaration());
        if(decl->get_class_type() == SgClassDeclaration::class_types::e_struct || decl->get_class_type() == SgClassDeclaration::class_types::e_union)
        {
            if(!decl->get_definition())
            {
                decl = typeMgr.getClassDeclaration(isSgClassType(type)->get_name().getString(), inSeq);
                assert(decl && decl->get_definition());
            }

            std::vector<SgDeclarationStatement*>& mems = decl->get_definition()->get_members();
            for(std::vector<SgDeclarationStatement*>::iterator memIt = mems.begin(); memIt != mems.end(); memIt++)
            {

                if(isSgVariableDeclaration(*memIt))
                {           
                    std::string newName;                         
                    std::vector<SgInitializedName*>& vars = isSgVariableDeclaration(*memIt)->get_variables();
                    for(std::vector<SgInitializedName*>::iterator varIt = vars.begin(); varIt != vars.end(); varIt++)
                    {
                        if(!SageInterface::isScalarType((*varIt)->get_type()))
                        {
                          newName = name + std::string(".") + (*varIt)->get_name().getString();
                          allocate_memory_and_save_sizes(newName, (*varIt)->get_type(), pVarNameToSizeVar, sizeVarDeclForTypeUpdates, inSeq);
                        }
                    }
                }
                else{
                  assert(0); // member, but no variable declaration
                }
            }

        }
    }
    else if(isSgTypedefType(type))
    {
      allocate_memory_and_save_sizes(name, isSgTypedefType(type)->get_base_type(), pVarNameToSizeVar, sizeVarDeclForTypeUpdates, inSeq);
    }
    else
    {
      assert(SageInterface::isScalarType(type));
    }
  }

  void GeneralCheckGen::deallocate_memory(std::string& name, SgType* type, std::map<std::string, std::string>& pVarNameToSizeVar, bool inSeq)
  {
    if(SageInterface::isPointerType(type))
    {
      SageBuilder::pushScopeStack(deallocate_block);
      // pointer type
      std::string sizeVar;
      std::string nameR, nameLocal = std::string(name);
      SgStatement* beforeStmt;
      bool renamed;

      beforeStmt = helper.free_stmt(name, type);
      SageInterface::appendStatement(beforeStmt);  

       int count = 0;
       SgBasicBlock* body;
       type = SageInterface::getElementType(type);
       std::vector<SgScopeStatement*> stack;
       std::string counter;
       SgName counterN;

       if(pVarNameToSizeVar.find(nameLocal) == pVarNameToSizeVar.end())
       {
         nameR = helper.remove_rename_suffix(nameLocal);
         assert(nameLocal.compare(nameR+std::string(seqVarSuffix)) == 0);
         assert(pVarNameToSizeVar.find(nameR) != pVarNameToSizeVar.end());
         sizeVar = pVarNameToSizeVar[nameR];
         renamed = true;
       }
       else
       {
         sizeVar = pVarNameToSizeVar[nameLocal];
         renamed = false;
       }

       while(SageInterface::isPointerType(type))
       {
         counter = prefixForCounter + std::to_string(count++);
         counterN = SgName(counter);
         body = SageBuilder::buildBasicBlock_nfi();
         SageInterface::insertStatement(beforeStmt, helper.constructForLoop(counterN, SageBuilder::buildVarRefExp(SgName(sizeVar)), body, SageBuilder::buildIntType()), true);

         stack.push_back(body->get_scope());
         SageBuilder::pushScopeStack (stack.back());

         nameLocal=nameLocal+"["+ counter +"]";
         beforeStmt = helper.free_stmt(nameLocal, type);
         SageInterface::appendStatement(beforeStmt); 

         if(renamed)
         {
           nameR=nameR+"["+counter +"]";
           sizeVar = pVarNameToSizeVar[nameR];
         }
         else
         {
           sizeVar = pVarNameToSizeVar[nameLocal]; 
         }
         
         type = SageInterface::getElementType(type);
       }

      for(unsigned int i=0;i<stack.size();i++)
      {
        SageBuilder::popScopeStack ();           
      }
      SageBuilder::popScopeStack();
    }
    else
    {
      assert(SageInterface::isScalarType(type) || isSgArrayType(type) || is_pointer_free(type, inSeq));
    }
  }
  
  bool GeneralCheckGen::is_pointer_free(SgType* type, bool inSeq)
  {
    if(SageInterface::isScalarType(type)) 
    {
        return true;
    }
    
    if(SageInterface::isPointerType(type))
    {
        return false;
    }
    
    if(isSgTypedefType(type))
    {
      return is_pointer_free(isSgTypedefType(type)->get_base_type(), inSeq);
    }
    
    else if(isSgClassType(type))
    {   
        SgClassDeclaration* decl = isSgClassDeclaration(isSgClassType(type)->get_declaration());
        if(decl->get_class_type() == SgClassDeclaration::class_types::e_struct || decl->get_class_type() == SgClassDeclaration::class_types::e_union)
        {
            if(!decl->get_definition())
            {
                decl = typeMgr.getClassDeclaration(isSgClassType(type)->get_name().getString(), inSeq);
                assert(decl && decl->get_definition());
            }

            std::vector<SgDeclarationStatement*>& mems = decl->get_definition()->get_members();
            for(std::vector<SgDeclarationStatement*>::iterator memIt = mems.begin(); memIt != mems.end(); memIt++)
            {

                if(isSgVariableDeclaration(*memIt))
                {                                    
                    std::vector<SgInitializedName*>& vars = isSgVariableDeclaration(*memIt)->get_variables();
                    for(std::vector<SgInitializedName*>::iterator varIt = vars.begin(); varIt != vars.end(); varIt++)
                    {
                        if(!is_pointer_free((*varIt)->get_type(), inSeq))
                        {
                            return false;
                        }  
                    }
                }
                else{
                  assert(0); // member, but no variable declaration
                }
            }
            return true;
        }
    }
    /*else
    {
      if(isSgPointerType(pType))
      {
        inspect_type_for_definition(isSgPointerType(pType)->get_base_type(), inSeq);
      }
      else if(isSgArrayType(pType))
      {
        inspect_type_for_definition(isSgArrayType(pType)->get_base_type(), inSeq);
      }
      else if(isSgModifierType(pType)) 
      {
        inspect_type_for_definition(isSgModifierType(pType)->get_base_type(), inSeq);
      }
      else if(isSgTypeComplex(pType))
      {
        inspect_type_for_definition(isSgTypeComplex(pType)->get_base_type(), inSeq);
      } 
    */
    return false;
  }
  
  SgArrayType* GeneralCheckGen::update_array_type(SgArrayType* origType, std::string& sizeVar, int id)
  {
    SgType* baseType = origType->get_base_type();
    if(isSgArrayType(baseType))
    {
      baseType = update_array_type(isSgArrayType(baseType), sizeVar, ++id);
    }
    if(isSgNullExpression(origType->get_index()))
    {       
      return SageBuilder::buildArrayType(baseType, SageBuilder::buildVarRefExp(SgName(sizeVar+"_"+std::to_string(id))));
    }
    
    if(baseType != origType->get_base_type())
    {
      return SageBuilder::buildArrayType(baseType, origType->get_index());
    }
    
    return origType;
  }

  void GeneralCheckGen::add_declaration(std::string& name, SgType* type, SgInitializer* initStmt, SgScopeStatement* scope, std::map<std::string, std::string>& pVarNameToSizeVar, std::vector<SgVariableDeclaration*>& sizeVarDeclForTypeUpdates, bool isSeq)
  {
    // TODO handle typedef, etc.?
    typeMgr.inspect_type_for_definition(type, isSeq);

    SgScopeStatement* top =  SageBuilder::topScopeStack ();

    allocate_memory_and_save_sizes(name, type, pVarNameToSizeVar, sizeVarDeclForTypeUpdates, isSeq);
    if(isSgArrayType(type))
    {
      SgArrayType* arrType = isSgArrayType(type);
      std::string sizeVar;
      if(pVarNameToSizeVar.find(name) == pVarNameToSizeVar.end()) 
      {
        std::string nameR = helper.remove_rename_suffix(name);
        assert(name.compare(nameR+std::string(seqVarSuffix)) == 0);
        assert(pVarNameToSizeVar.find(nameR) != pVarNameToSizeVar.end());
        sizeVar = pVarNameToSizeVar[nameR];
      }
      else
      {
        sizeVar = pVarNameToSizeVar[name]; 
      }
      type = update_array_type(arrType, sizeVar, 1);
    }
    
    SgInitializer* initVal = NULL;
    if(helper.is_const_type(type))
    {
        assert(initStmt);
        initVal = SageInterface::deepCopy<SgInitializer>(initStmt);
    }
    
    if(helper.is_global(scope))
    {
      SageBuilder::popScopeStack ();
      SageInterface::prependStatement(SageBuilder::buildVariableDeclaration (name, type, initVal));
      SageBuilder::pushScopeStack (top);
    }
    else
    { 
      //do not declare variable if is struct with constant member variables, will be declared during initialization
      SgType* stripType = type->stripTypedefsAndModifiers();
      if(initVal == NULL && isSgClassType(stripType) && isSgClassDeclaration(isSgClassType(stripType)->get_declaration())->get_class_type() == SgClassDeclaration::class_types::e_struct)
      {
          SgClassDeclaration* decl =  isSgClassDeclaration(isSgClassType(stripType)->get_declaration());
          if(!decl->get_definition())
          {
            decl = typeMgr.getClassDeclaration(decl->get_name().getString(), true);
          }
          assert(decl->get_definition());
          std::vector<SgDeclarationStatement*>& classMembers = decl->get_definition()->get_members();
      
          for(std::vector<SgDeclarationStatement*>::iterator memIt = classMembers.begin(); memIt != classMembers.end(); memIt++)
          {
            std::vector<SgInitializedName*>& vars = isSgVariableDeclaration(*memIt)->get_variables();
            for(std::vector<SgInitializedName*>::iterator varIt = vars.begin(); varIt != vars.end(); varIt++)
            {  
              if(helper.is_const_type((*varIt)->get_type()))
              {
                  return;
              }
            }
          }
      }
      SageInterface::prependStatement(SageBuilder::buildVariableDeclaration (name, type, initVal));
    }
  }
  
  void GeneralCheckGen::gen_and_add_var_init(std::vector<std::tuple<std::string, std::string, SgType*>>& varPairs, SgStatement& addBeforeStmt, PrototypeManager& protoMgr, std::map<std::string, SgInitializedName*>& reqForArrayDeclOrConstInit, std::map<std::string, std::string>& pVarNameToSizeVar)
  {
    InitializationGen initGen(&protoMgr, &inclMgr, &typeMgr);

    initGen.add_init_of(varPairs, addBeforeStmt, reqForArrayDeclOrConstInit, pVarNameToSizeVar); 

  }

  bool GeneralCheckGen::use_same_dimensions(SgArrayType* type1, SgArrayType* type2)
  {
        bool identical = true;
        std::vector<SgExpression*> dimLen1 = SageInterface::get_C_array_dimensions(type1);
        std::vector<SgExpression*> dimLen2 = SageInterface::get_C_array_dimensions(type2);
        if(dimLen1.size() == dimLen2.size())
        {
          for(unsigned int i=0; identical && i<dimLen1.size();i++)
          {
            if(dimLen1[i]->unparseToString().compare(dimLen2[i]->unparseToString())!=0)// TODO use better comparison
            {
              identical = false;
              break;
            }
          }
        }
        else
        {
          identical = false;
        }
        for(std::vector<SgExpression*>::iterator dimIt = dimLen1.begin(); dimIt != dimLen1.end(); dimIt++)
        {
          delete *dimIt;
        }
        for(std::vector<SgExpression*>::iterator dimIt = dimLen2.begin(); dimIt != dimLen2.end(); dimIt++)
        {
          delete *dimIt;
        }

        return identical;
  }

  bool GeneralCheckGen::is_same_type(SgType* type1, SgType* type2)
  { // TODO caching?
    bool identical = false;
    if(SageInterface::isEquivalentType(type1, type2))
    {
      identical = true;
      if(isSgArrayType(type1))
      {
        identical = use_same_dimensions(isSgArrayType(type1), isSgArrayType(type2));
      }
    }
    else
    {
      if(isSgTypedefType(type1) && isSgTypedefType(type2))
      {
        identical = is_same_type(isSgTypedefDeclaration(isSgTypedefType(type1)->get_declaration())->get_base_type(), isSgTypedefDeclaration(isSgTypedefType(type2)->get_declaration())->get_base_type());
      }
      else if(isSgClassType(type1) && isSgClassType(type2))
      {
        
        SgClassDeclaration* decl1 =  isSgClassDeclaration(isSgClassType(type1)->get_declaration());
        SgClassDeclaration* decl2 =  isSgClassDeclaration(isSgClassType(type2)->get_declaration());
        if(decl1 && decl2 && decl1->get_class_type() == decl2->get_class_type() && (decl1->get_name() == decl2->get_name() || (decl1->get_isUnNamed() && decl2->get_isUnNamed()))) 
        {

          if(!decl1->get_definition())
          {
            decl1 = typeMgr.getClassDeclaration(decl1->get_name().getString(), true);
            assert(decl1 && decl1->get_definition());
          }
          if(!decl2->get_definition())
          {
            decl2 = typeMgr.getClassDeclaration(decl2->get_name().getString(), false);
            assert(decl2 && decl2->get_definition());
          }

          std::vector<SgDeclarationStatement*>& mems1 = decl1->get_definition()->get_members();
          std::vector<SgDeclarationStatement*>& mems2 = decl2->get_definition()->get_members();
          if(mems1.size() == mems2.size() && (decl1->get_class_type() == SgClassDeclaration::class_types::e_struct || decl1->get_class_type() == SgClassDeclaration::class_types::e_union))
          {
            identical = true;
            for(unsigned int i = 0; identical && i<mems1.size(); i++)
            {
              if(isSgVariableDeclaration(mems1[i]) && isSgVariableDeclaration(mems2[i]))
              {
                std::vector<SgInitializedName*>& vars1 = isSgVariableDeclaration(mems1[i])->get_variables();
                std::vector<SgInitializedName*>& vars2 = isSgVariableDeclaration(mems2[i])->get_variables();
                identical = vars1.size() == vars2.size();
                for(unsigned int j=0; identical && j<vars1.size();j++)
                {
                  identical = vars1[j]->get_name() == vars2[j]->get_name() && is_same_type(vars1[j]->get_type(), vars2[j]->get_type());  
                }
              }
              else{
                assert(0);
              }
            }
          }
        }

      }

      if(isSgTypeComplex(type1) && isSgTypeComplex(type2))
      {
        identical = is_same_type(isSgTypeComplex(type1)->get_base_type(), isSgTypeComplex(type2)->get_base_type());
      }

      if(isSgPointerType(type1) && isSgPointerType(type2))
      {
        identical = is_same_type(isSgPointerType(type1)->get_base_type(), isSgPointerType(type2)->get_base_type());
      }

      if(isSgArrayType(type1) && isSgArrayType(type2) && use_same_dimensions(isSgArrayType(type1), isSgArrayType(type2)))
      {
        identical = is_same_type(isSgArrayType(type1)->get_base_type(), isSgArrayType(type2)->get_base_type());
      }

      if(isSgModifierType(type1) && isSgModifierType(type2) && isSgModifierType(type1)->get_typeModifier() == isSgModifierType(type2)->get_typeModifier()) 
      {
        identical = is_same_type(isSgModifierType(type1)->get_base_type(), isSgModifierType(type2)->get_base_type());
      }

      if(isSgFunctionType(type1) && isSgFunctionType(type2))
      {
        std::vector<SgType*>& args1 = isSgFunctionType(type1)->get_arguments();
        std::vector<SgType*>& args2 = isSgFunctionType(type2)->get_arguments();

        identical = args1.size() == args2.size() && is_same_type(isSgFunctionType(type1)->get_return_type(), isSgFunctionType(type2)->get_return_type());
        for(unsigned int i=0; identical && i<args1.size(); i++)
        {
          identical = is_same_type(args1[i], args2[i]);
        }
      }
    }

    return identical;
  }

  void GeneralCheckGen::add_array_init_vars(SgArrayType* arrType, std::set<SgInitializedName*>& arrInitVars)
  {
    std::vector<SgExpression*> dimLen = SageInterface::get_C_array_dimensions(arrType);
    for(std::vector<SgExpression*>::iterator dimIt = dimLen.begin(); dimIt != dimLen.end(); dimIt++)
    {
      std::vector<SgVarRefExp*> varRefs;
      SageInterface::collectVarRefs(*dimIt, varRefs);
      for(std::vector<SgVarRefExp*>::iterator varsIt = varRefs.begin(); varsIt != varRefs.end(); varsIt++)
      {
        assert(SageInterface::isScalarType((*varsIt)->get_symbol()->get_type()));
        arrInitVars.insert((*varsIt)->get_symbol()->get_declaration());
      }
      delete *dimIt;
    }
  }
  
  void GeneralCheckGen::add_const_init_vars(SgInitializer* initializer, std::set<SgInitializedName*>& constInitVars)
  {
      assert(initializer);
      std::vector<SgVarRefExp*> varRefs;
      SageInterface::collectVarRefs(initializer, varRefs);
      SgInitializedName* var;
      for(std::vector<SgVarRefExp*>::iterator varsIt = varRefs.begin(); varsIt != varRefs.end(); varsIt++)
      {
        var = (*varsIt)->get_symbol()->get_declaration();
        assert(SageInterface::isScalarType((*varsIt)->get_symbol()->get_type()));
        if(constInitVars.insert(var).second && helper.is_const_type(var->get_type()))
        { 
            add_const_init_vars(var->get_initializer(), constInitVars);
        }
      }
  }

  void GeneralCheckGen::store_size_variable(std::string& name, SgType* type, std::map<std::string, std::string>& pVarNameToSizeVar, bool isSeq)
  {
    // TODO support nested data structures         
    std::string nameLocal = std::string(name);
    if(isSgArrayType(type) || SageInterface::isPointerType(type) || isSgPointerType(type))
    {
     pVarNameToSizeVar[nameLocal] = prefixForSizeVar + std::to_string(countIndex++);
     if(SageInterface::isPointerType(type))
     {
       int count = 0;
       type = SageInterface::getElementType(type);
       while(SageInterface::isPointerType(type))
       {
         nameLocal=nameLocal+"["+prefixForCounter + std::to_string(count++) +"]";
         pVarNameToSizeVar[nameLocal] = prefixForSizeVar + std::to_string(countIndex++);
         type = SageInterface::getElementType(type);
       }
     }
    }
    else 
    { 

      if(isSgTypedefType(type))
      {
        store_size_variable(name, isSgTypedefDeclaration(isSgTypedefType(type)->get_declaration())->get_base_type(), pVarNameToSizeVar, isSeq); 
      }
      else if(isSgClassType(type))
      {
        std::string memName;
        SgClassDeclaration* decl =  isSgClassDeclaration(isSgClassType(type)->get_declaration());
        if(decl) 
        {
          if(!decl->get_definition())
          {
            decl = typeMgr.getClassDeclaration(decl->get_name().getString(), isSeq);
            assert(decl && decl->get_definition());
          }

          std::vector<SgDeclarationStatement*>& mems = decl->get_definition()->get_members();
          for(unsigned int i = 0; i<mems.size(); i++)
          {
            if(isSgVariableDeclaration(mems[i]))
            {
                std::vector<SgInitializedName*>& vars = isSgVariableDeclaration(mems[i])->get_variables();
                for(unsigned int j=0;  j<vars.size();j++)
                {
                  memName = name + "." + vars[j]->get_name().getString();
                  store_size_variable(memName, vars[j]->get_type(), pVarNameToSizeVar, isSeq);  
                }
            }
            else{
              assert(0);
            }
          }
        }
        else
        {
          assert(0);
        }
      }

      else if(isSgModifierType(type)) 
      {
        store_size_variable(name, isSgModifierType(type)->get_base_type(), pVarNameToSizeVar, isSeq);
      }
      else 
      { 
        assert(SageInterface::isScalarType(type) || (isSgTypeComplex(type) && SageInterface::isScalarType(isSgTypeComplex(type)->get_base_type())));
      }
    }
  }
  
  SgInitializedName* GeneralCheckGen::findVar(SgScopeStatement* funScope, std::string pName) 
  {    
    SgName varName = SgName(pName);
    SgVariableSymbol* varSymb = NULL;
    SgScopeStatement* currentScope = funScope;
    do 
    {
      varSymb = currentScope->lookup_variable_symbol(varName);

      if(helper.is_global(currentScope))
      {
        currentScope = NULL;
      } 
      else
      {
        currentScope = SageInterface::getEnclosingScope(currentScope, false);
      }
    } while(varSymb == NULL && currentScope != NULL); 
    
    if(varSymb != NULL)
    {
      return varSymb->get_declaration();
    }
    return NULL;
  }
  
  std::vector<SgInitializedName*>* GeneralCheckGen::match_vars(std::map<SgInitializedName*, VariableClassification>* varsSeq, std::map<SgInitializedName*, VariableClassification>* varsPar,  SgScopeStatement* seqEndScope, SgScopeStatement* parEndScope, std::vector<std::tuple<std::string, std::string, SgType*>>& reqInit, std::vector<std::tuple<std::string, std::string, SgType*>>& reqEqCheck, std::map<std::string, SgInitializedName*>& reqForArrayDeclOrConstInit, std::map<std::string, std::string>& pVarNameToSizeVar, std::set<std::string>& non_shared_vars)
  {
    std::vector<SgInitializedName*>* vars_to_rename = new std::vector<SgInitializedName*>;
    std::set<SgInitializedName*> matchedWithSeqVar;
    std::set<SgInitializedName*> arrInitVars;
    std::set<SgInitializedName*> constInitVars;
    std::string emptyStr = "";
    std::string varName, varNameR;
    SgType* type;

    std::map<std::string, SgInitializedName*> varNamesPar, varNamesSeq;
    std::map<std::string, SgInitializedName*>::iterator found;
    for(std::map<SgInitializedName*, VariableClassification>::iterator itPar = varsPar->begin(); itPar != varsPar->end(); itPar++)
    {
      assert(varNamesPar.insert(std::pair<std::string, SgInitializedName*>(itPar->first->get_name().getString(), itPar->first)).second);
    }

    for(std::map<SgInitializedName*, VariableClassification>::iterator itSeq = varsSeq->begin(); itSeq != varsSeq->end(); itSeq++)
    {
      assert(varNamesSeq.insert(std::pair<std::string, SgInitializedName*>(itSeq->first->get_name().getString(), itSeq->first)).second);
      varName = itSeq->first->get_name().getString();
      varNameR = helper.add_rename_suffix(varName);
      type = itSeq->first->get_type();
      found = varNamesPar.find(varName);

      store_size_variable(varName, itSeq->first->get_type(),  pVarNameToSizeVar, true);

      if(isSgArrayType(type))
      {
        add_array_init_vars(isSgArrayType(type), arrInitVars);
      }
      
      if(helper.is_const_type(type) && !isSgClassDefinition(itSeq->first->get_scope()))
      { 
        add_const_init_vars(itSeq->first->get_initializer(), constInitVars);
      }

      if(found != varNamesPar.end())
      {
        assert(is_same_type(type, found->second->get_type()) || (!(itSeq->second.is_used_before()) && !(itSeq->second.must_compare()) && !((*varsPar)[found->second].is_used_before()) && !((*varsPar)[found->second].must_compare()))); 
        assert(matchedWithSeqVar.insert(found->second).second);

        if(itSeq->second.is_written() || (*varsPar)[found->second].is_written()) // renaming
        { 
          itSeq->second.set_rename();
          
          if(itSeq->second.get_decl() != NULL)
          {
            itSeq->second.get_decl()->set_name(varNameR);
          } 
          vars_to_rename->push_back(itSeq->first);

          if(itSeq->second.must_compare() || (*varsPar)[found->second].must_compare()) // equivalence check
          {
            reqEqCheck.push_back(std::tuple<std::string, std::string, SgType*>(varNameR, varName, type));
          }
        }
        else // read only, require only one declaration
        {
          if(itSeq->second.must_declare() && (*varsPar)[found->second].must_declare())
          {
            itSeq->second.unset_declaration();
          }
        }

        if(itSeq->second.must_initialize()) // initialization
        {
          if(!(itSeq->second.must_rename()))
          {
            reqInit.push_back(std::tuple<std::string, std::string, SgType*>(varName, emptyStr, type));  
          }
          else
          {
            if((*varsPar)[found->second].must_initialize())
            {
              reqInit.push_back(std::tuple<std::string, std::string, SgType*>(varNameR, varName, type));
            }
            else
            {
              reqInit.push_back(std::tuple<std::string, std::string, SgType*>(varNameR, emptyStr, type));
            }
          }
        }  
        else if((*varsPar)[found->second].must_initialize())
        {
          reqInit.push_back(std::tuple<std::string, std::string, SgType*>(varName, emptyStr, type));           
        } 
      
      }
      else
      {
        if(itSeq->second.must_compare() && non_shared_vars.find(itSeq->first->get_name().getString()) == non_shared_vars.end())
        {
          SgInitializedName* parVar = findVar(parEndScope, itSeq->first->get_name().getString());
          assert(parVar != NULL);
          VariableClassification parVarClass;
          parVarClass.set_used_before();
          (*varsPar)[parVar] = parVarClass;
          assert(varNamesPar.insert(std::pair<std::string, SgInitializedName*>(itSeq->first->get_name().getString(), parVar)).second);        
                  
          assert(is_same_type(type, parVar->get_type())); 
          assert(matchedWithSeqVar.insert(parVar).second);
          
          itSeq->second.set_used_before();
          itSeq->second.set_rename();
          if(itSeq->second.get_decl() != NULL)
          {
            itSeq->second.get_decl()->set_name(varNameR);
          } 
          vars_to_rename->push_back(itSeq->first);
          
          reqEqCheck.push_back(std::tuple<std::string, std::string, SgType*>(varNameR, varName, type));
          if(itSeq->second.must_initialize())
          {
            reqInit.push_back(std::tuple<std::string, std::string, SgType*>(varNameR, varName, type)); 
          }
          else
          {
            reqInit.push_back(std::tuple<std::string, std::string, SgType*>(varName, emptyStr, type));
          }
          
        } 
        else if(itSeq->second.must_initialize()) 
        {
          reqInit.push_back(std::tuple<std::string, std::string, SgType*>(varName, emptyStr, type));
        }
      }
    }

    for(std::map<SgInitializedName*, VariableClassification>::iterator itPar = varsPar->begin(); itPar != varsPar->end(); itPar++)
    { 
      varName = itPar->first->get_name().getString();
      type = itPar->first->get_type();
      if(helper.is_const_type(type) && !isSgClassDefinition(itPar->first->get_scope()))
      {
        add_const_init_vars(itPar->first->get_initializer(), constInitVars);
      }
        
      if(matchedWithSeqVar.find(itPar->first) == matchedWithSeqVar.end())
      {
        store_size_variable(varName, type,  pVarNameToSizeVar, false);

        if(isSgArrayType(type))
        {
          add_array_init_vars(isSgArrayType(type), arrInitVars);
        }
        
        if(itPar->second.must_compare() && non_shared_vars.find(itPar->first->get_name().getString()) == non_shared_vars.end())
        {    
          varNameR = helper.add_rename_suffix(varName);                
          SgInitializedName* seqVar = findVar(seqEndScope, itPar->first->get_name().getString());
          assert(seqVar != NULL);
          VariableClassification seqVarClass;
          seqVarClass.set_used_before();
          (*varsSeq)[seqVar] = seqVarClass;
          assert(varNamesSeq.insert(std::pair<std::string, SgInitializedName*>(itPar->first->get_name().getString(), seqVar)).second);    
                  
          assert(is_same_type(type, seqVar->get_type())); 
          assert(matchedWithSeqVar.insert(itPar->first).second);
          
          seqVarClass.set_rename();
          if(seqVarClass.get_decl() != NULL)
          {
            seqVarClass.get_decl()->set_name(varNameR);
          } 
          vars_to_rename->push_back(seqVar);
          itPar->second.set_used_before();
          
          reqEqCheck.push_back(std::tuple<std::string, std::string, SgType*>(varNameR, varName, type));
          if(itPar->second.must_initialize())
          {
            reqInit.push_back(std::tuple<std::string, std::string, SgType*>(varNameR, varName, type)); 
          }
          else
          {
            reqInit.push_back(std::tuple<std::string, std::string, SgType*>(varNameR, emptyStr, type));
          }
        
        }
        else if(itPar->second.must_initialize()) 
        {
          assert(itPar->second.must_declare());
          reqInit.push_back(std::tuple<std::string, std::string, SgType*>(varName, emptyStr, type));
        }
      }
    }

    std::set<SgInitializedName*> renameVarSet (vars_to_rename->begin(), vars_to_rename->end());
    for(std::set<SgInitializedName*>::iterator it = constInitVars.begin(); it != constInitVars.end(); it++)
    {
      if(renameVarSet.find(*it) != renameVarSet.end())
      {
        reqForArrayDeclOrConstInit.insert(std::pair<std::string, SgInitializedName*>(helper.add_rename_suffix((*it)->get_name()), (*it)));
      }
      else
      { 
        if(varNamesSeq.find((*it)->get_name()) == varNamesSeq.end() || varNamesSeq[(*it)->get_name()] == *it || constInitVars.find(varNamesSeq[(*it)->get_name()])==constInitVars.end() || (varNamesSeq[(*it)->get_name()] != *it && renameVarSet.find(varNamesSeq[(*it)->get_name()]) != renameVarSet.end()))
        { 
          reqForArrayDeclOrConstInit.insert(std::pair<std::string, SgInitializedName*>((*it)->get_name(), *it));
        }
      }
    }

    for(std::set<SgInitializedName*>::iterator it = arrInitVars.begin(); it != arrInitVars.end(); it++)
    {
      if(varNamesPar.find((*it)->get_name()) != varNamesPar.end())
      { 
        assert(is_same_type(varNamesPar[(*it)->get_name()]->get_type(), (*it)->get_type()));
        reqForArrayDeclOrConstInit.insert(std::pair<std::string, SgInitializedName*>((*it)->get_name(), varNamesPar[(*it)->get_name()]));
        continue;
      }
      else
      {
        reqForArrayDeclOrConstInit.insert(std::pair<std::string, SgInitializedName*>((*it)->get_name(), (*it))); 
      }
 
    }

    return vars_to_rename;
  }

  void GeneralCheckGen::gen_and_add_eq_check(std::vector<std::tuple<std::string, std::string, SgType*>>& varPairsEq, std::map<std::string, std::string>& pVarNameToSizeVar)
  {
    assert(!require_output_vars() || varPairsEq.size()>0);
    if(varPairsEq.size()>0)
    {
        EqualityCheckGen eqGen;
    
        SgStatement* varDecl = SageBuilder::buildVariableDeclaration (equalVar, SageBuilder::buildIntType());
        SageInterface::addMessageStatement (varDecl, "// Start equality check");
        SageInterface::appendStatement(varDecl);
        eqGen.add_eq_checks(varPairsEq, true, pVarNameToSizeVar, &typeMgr);
    }
    else
    {
        std::cout<<"No relevant output variables found resulting in verification tasks without equivalence assertion.\n";
    }
  }


  void GeneralCheckGen::write_checker_file(SgSourceFile* file, std::string fileName, SgProject* seqProject)
  {
    file->set_unparse_output_filename (fileName);
    assert(file->get_project() && seqProject);
    // check generated program(s)
    AstTests::runAllTests(file->get_project());   
    // Generate source code from AST, ensure that modified sequential project okay -> renaming okay 
    AstTests::runAllTests(seqProject);
    file->unparse();
  }
