//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "rose.h"
#include "GeneralCheckGen.h"
#include "WholeCheckGen.h"
#include "DoAllChecker.h"
#include "ReductionChecker.h"
#include "PECConstants.h"
#include <iostream>
#include <string>
#include <assert.h>

int main( int argc, char * argv[] ){
    assert(argc>=3);

   int progArgsStart = 1;
   CheckType checkingProc = GENERAL;

   if(strncmp(argv[1], "-type=", 6) == 0)
   {
     progArgsStart++;
     if(strcmp(argv[1], "-type=DOALL") == 0)
     {
       checkingProc = DOALL;
     }
     else if(strcmp(argv[1], "-type=REDUCTION") == 0)
     {
       checkingProc = REDUCTIONPAT;
     } 
     else if(strcmp(argv[1], "-type=WHOLE") == 0)
     {
       checkingProc = WHOLE;
     }
     else
     {
       assert(strcmp(argv[1], "-type=GENERAL") == 0);
     }
   }
   
   // TODO allow configuration of entry function
   std::string entryFun = std::string("main");
   
   bool requireOutputVars = true;
   if(strcmp(argv[progArgsStart], "-allowNoOut") == 0)
   {
     progArgsStart++;
     requireOutputVars = false;
   }

    // Initialize and check compatibility. See Rose::initialize
    ROSE_INITIALIZE;

    // Build the AST used by ROSE
    // Separately for sequential and parallelized implementation to avoid that they share global definitions
    std::vector<std::string> args1, args2;
    int argc1 = argc-progArgsStart;
    char* argv1[argc1];
    argv1[0] = argv[0];
    argv1[1] =  argv[progArgsStart];

    int argc2 = argc1;
    char* argv2[argc2];
    argv2[0] = argv[0];
    argv2[1] = argv[progArgsStart+1];

    for(int j=2,i=progArgsStart+2, k=0; i<argc; j++,i++, k++)
    {
      argv1[j] = argv[i];
      argv2[j] = argv[i];
    }
    SgProject* project = frontend(argc1,argv1);
    ROSE_ASSERT (project != NULL);

    SgProject* project2 = frontend(argc2,argv2);
    ROSE_ASSERT (project2 != NULL);
    // Run internal consistency tests on AST
    AstTests::runAllTests(project);
    AstTests::runAllTests(project2);

    // Insert your own manipulation of the AST here...

    switch(checkingProc)
    {
      case GENERAL:
      {
        GeneralCheckGen generator = GeneralCheckGen(requireOutputVars);
        generator.generate_checker_programs(isSgSourceFile(project->operator[](0)), isSgSourceFile(project2->operator[](0)), "checker");
        break;
      }
      case DOALL:
      {
        DoAllChecker checker = DoAllChecker(requireOutputVars);
        checker.perform_pattern_checks(isSgSourceFile(project->operator[](0)), isSgSourceFile(project2->operator[](0)));
        break;
      }
      case REDUCTIONPAT:
      {
        ReductionChecker checker = ReductionChecker(requireOutputVars);
        checker.perform_pattern_checks(isSgSourceFile(project->operator[](0)), isSgSourceFile(project2->operator[](0)));
        break;
      }
      case WHOLE:
      {
        // TODO
        WholeCheckGen generator = WholeCheckGen(requireOutputVars);
        generator.generate_checker_program(isSgSourceFile(project->operator[](0)), isSgSourceFile(project2->operator[](0)), "checker", entryFun);
        assert(0);
        break;
      }
      default: 
        assert(0);
    }


    delete project;
    delete project2;

    return 0; 
}


