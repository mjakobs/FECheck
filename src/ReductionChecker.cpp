//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "ReductionChecker.h"

  bool ReductionChecker::check_parallelized_loop(SgForStatement* parFor, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar, std::set<std::string>& seqVarsForOutput)
  {
    if(DoAllChecker::check_parallelized_loop(parFor, varsPar, seqVarsForOutput)) // check input, output correctly classified (data sharing attributes) and independence of loop iterations
    {
      // check correct usage of reduction variables
      bool contains_global = false;
      std::set<SgInitializedName*> reductionVars;
      for(std::map<SgInitializedName*, VariableAttributeClassification>::iterator it = varsPar.begin(); it != varsPar.end(); it++)
      {
        if(it->second.get_sharing_type() == REDUCTION)
        {
          if(helper.is_global(it->first->get_scope()))
          {
            contains_global = true;
          }
          reductionVars.insert(it->first);
          // currently only scalar reduction types are supported
          assert(SageInterface::isScalarType(it->first->get_type()));  
          // reduction operators min and max are not supported 
          assert(it->second.get_reduction_operation() != MINIMUM && it->second.get_reduction_operation() != MAXIMUM); 
        }
      }

      // inspect statements
      SgStatement* stmt;
      SgExpression* expr;
      SgInitializedName* var;
      std::set<SgStatement*> analyzed_stmts;
      std::vector<SgVarRefExp*> varRefs;
      SageInterface::collectVarRefs (parFor, varRefs); 
      for(std::vector<SgVarRefExp*>::iterator refIt = varRefs.begin(); refIt != varRefs.end(); refIt++)
      {
        var = (*refIt)->get_symbol()->get_declaration();
        if(reductionVars.find(var) == reductionVars.end())
        {
          continue;
        }

        stmt = SageInterface::getEnclosingStatement(*refIt);
        if(isSgLabelStatement(stmt))
        {
          stmt = isSgLabelStatement(stmt)->get_statement();
        }
        assert(stmt);

        if(analyzed_stmts.find(stmt) == analyzed_stmts.end())
        {
          analyzed_stmts.insert(stmt);
          // currently only support reductions statements of the form var+=expr, var=var+expr, var=expr+var, var++, ++var, where + is an example for one reduction operator
          if(isSgExprStatement(stmt))
          {
            expr = isSgExprStatement(stmt)->get_expression();
            if((isSgMinusMinusOp(expr) && varsPar[var].get_reduction_operation() == MINUS)|| (isSgPlusPlusOp(expr) && varsPar[var].get_reduction_operation() == PLUS))
            {
              continue;
            }
            if(isSgAssignOp(expr))
            {
              if(!check_assignment(var, varsPar[var].get_reduction_operation(), isSgAssignOp(expr)))
              {
                std::cout<<"Reduction variable " + var->unparseToString() << " wrongly used in assignment statement "<<expr->unparseToString()<<std::endl;
                return false;
              }
            }
            else if(isSgCompoundAssignOp(expr))
            {
              if(!check_compound_assignment(var, varsPar[var].get_reduction_operation(), isSgCompoundAssignOp(expr)))
              {
                std::cout<<"Reduction variable " + var->unparseToString() << " wrongly used in assignment statement "<<expr->unparseToString()<<std::endl;
                return false;
              }
            }
            else
            { 
              std::cout<<"Reduction variable " + var->unparseToString() << " used outside of assignment."<<std::endl;
              return false; 
            }
          }
          else
          {
            std::cout<<"Reduction variable " + var->unparseToString() << " used outside of assignment."<<std::endl;
            return false; 
          }
        }
      }

      // inspect function calls, conservative, do not support global reduction variables if function is called in parallel reduction
      // TODO it would be more precise to check if the called functions use the reduction variable or even better only use them in reduction statements
      // usages in function arguments should have been forbidden by the previous inspection
      Rose_STL_Container<SgNode*> funCalls = NodeQuery::querySubTree (parFor->get_loop_body(), V_SgFunctionCallExp );
      if(contains_global && funCalls.size()>0)
      {
        std::cout<<"Global reduction variable might inadequately accessed in function call."<<std::endl;
        return false; 
      }

      return true;
    }
    return false;
  }

  bool ReductionChecker::check_assignment(SgInitializedName* var, ReductionOp opType, SgAssignOp* assign)
  {
    if(!is_reduction_variable(var, assign->get_lhs_operand()))
    {
      return false;
    }

    if(isSgBinaryOp(assign->get_rhs_operand()))
    {
      SgBinaryOp* binOp = isSgBinaryOp(assign->get_rhs_operand());

      if((is_reduction_variable(var, binOp->get_lhs_operand()) && does_not_contain_reduction_variable(var, binOp->get_rhs_operand())) || (is_reduction_variable(var, binOp->get_rhs_operand()) && does_not_contain_reduction_variable(var, binOp->get_lhs_operand())))
      {
        return (opType == AND && isSgAndOp(binOp)) || (opType == B_AND && isSgBitAndOp(binOp)) || (opType == B_OR && isSgBitOrOp(binOp)) || (opType == MINUS && isSgSubtractOp(binOp)) || (opType == MUL && isSgMultiplyOp(binOp)) || (opType == OR && isSgOrOp(binOp)) || (opType == PLUS && isSgAddOp(binOp)) || (opType == XOR && isSgBitXorOp(binOp));
      }
    }

    return false;    
  }

  bool ReductionChecker::check_compound_assignment(SgInitializedName* var, ReductionOp opType, SgCompoundAssignOp* assign)
  {
    if(!is_reduction_variable(var, assign->get_lhs_operand()))
    {
      return false;
    }

    Rose_STL_Container<SgNode*> vars = NodeQuery::querySubTree(assign->get_rhs_operand(), V_SgInitializedName); 
    for (Rose_STL_Container<SgNode*>::iterator varIt = vars.begin(); varIt != vars.end(); varIt++)
    {
      if(*varIt == var)
      {
        return false;
      }
    }
    
    return does_not_contain_reduction_variable(var, assign->get_rhs_operand()) && ((opType == B_AND && isSgAndAssignOp(assign)) || (opType == B_OR && isSgIorAssignOp(assign)) || (opType == MINUS && isSgMinusAssignOp(assign)) || (opType == MUL && isSgMultAssignOp(assign))  || (opType == PLUS && isSgPlusAssignOp(assign)) || (opType == XOR && isSgXorAssignOp(assign)));

  }

  bool ReductionChecker::is_reduction_variable(SgInitializedName* reductionVar, SgExpression* expr)
  {
    return isSgVarRefExp(expr) && isSgVarRefExp(expr)->get_symbol()->get_declaration() == reductionVar;
  }

  bool ReductionChecker::does_not_contain_reduction_variable(SgInitializedName* reductionVar, SgExpression* expr)
  {
    Rose_STL_Container<SgNode*> vars = NodeQuery::querySubTree(expr, V_SgInitializedName); 
    for (Rose_STL_Container<SgNode*>::iterator varIt = vars.begin(); varIt != vars.end(); varIt++)
    {
      if(*varIt == reductionVar)
      {
        return false;
      }
    }

    return true;
  }

  bool ReductionChecker::allows_reduction()
  {
    return true;
  }

  std::string ReductionChecker::get_pattern_name()
  {
    return "Reduction";
  }
