//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <assert.h>
#include "InitializationGen.h"
#include "PECConstants.h"

  InitializationGen::InitializationGen(PrototypeManager* pProtoManager, IncludeManager* pIncludeMgr, TypeManager* pTypeMgr)
  {
    protoMgr = pProtoManager;
    inclMgr = pIncludeMgr;
    typeMgr = pTypeMgr;
  }

  void InitializationGen::add_init_of(std::vector<std::tuple<std::string, std::string, SgType*>>& varPairs, SgStatement& addBeforeStmt, std::map<std::string, SgInitializedName*>& reqForArrayDecl, std::map<std::string, std::string>& pVarNameToSizeVar)
  {
    beforeStmt = &addBeforeStmt;
    varNameToSizeVar = &pVarNameToSizeVar;

    for(std::vector<std::tuple<std::string, std::string, SgType*>>::iterator it = varPairs.begin(); it != varPairs.end(); it++)
    {
      count = 0;
      SgType* type = std::get<2>(*it);
      std::string nameS = std::get<0>(*it), nameP = std::get<1>(*it);
      
      if(helper.is_const_type(type))
      {
          assert(nameS.empty() || nameP.empty());
          continue;
      }
      

      if(nameS.empty())
      {
        if(reqForArrayDecl.find(nameP) == reqForArrayDecl.end())
        {
          add_init_stmt(SageBuilder::buildVarRefExp(nameP), type, false);
        }
      }
      else if (nameP.empty())
      {
        if(reqForArrayDecl.find(nameS) == reqForArrayDecl.end())
        {
          add_init_stmt(SageBuilder::buildVarRefExp(nameS), type, false);
        }
      }
      else
      {
        // assume that variables used in array declaration are not modified, thus, will not be duplicated, TODO assumption not always true
        assert(reqForArrayDecl.find(nameS) == reqForArrayDecl.end() && reqForArrayDecl.find(nameP) == reqForArrayDecl.end());
        if(reqForArrayDecl.find(nameS) != reqForArrayDecl.end())
        {
          equal_stmts(SageBuilder::buildVarRefExp(nameS), SageBuilder::buildVarRefExp(nameP), type, false);
        }
        else if(reqForArrayDecl.find(nameP) != reqForArrayDecl.end())
        {
          equal_stmts(SageBuilder::buildVarRefExp(nameP), SageBuilder::buildVarRefExp(nameS), type, false);
        }
        else
        {
          add_init_and_equal_stmts(SageBuilder::buildVarRefExp(nameS), SageBuilder::buildVarRefExp(nameP), type, false); 
        }
      }
    }
  }
  
  bool InitializationGen::contains_const_elems(SgClassType* type)
  {
      SgClassDeclaration* decl =  isSgClassDeclaration(type->get_declaration());
      if(!decl->get_definition())
      {
        decl = typeMgr->getClassDeclaration(decl->get_name().getString(), true);
      }
      assert(decl->get_definition());
      std::vector<SgDeclarationStatement*>& classMembers = decl->get_definition()->get_members();

      if(decl->get_class_type() == SgClassDeclaration::class_types::e_struct)
      {
        for(std::vector<SgDeclarationStatement*>::iterator memIt = classMembers.begin(); memIt != classMembers.end(); memIt++)
        {
          std::vector<SgInitializedName*>& vars = isSgVariableDeclaration(*memIt)->get_variables();
          for(std::vector<SgInitializedName*>::iterator varIt = vars.begin(); varIt != vars.end(); varIt++)
          {  
            if(helper.is_const_type((*varIt)->get_type()))
            {
                return true;
            }
          }
        }
      }
      else
      {
        assert(0);
      }
      return false; // might be incorrect because helper.is_const_type is incomplete
  }

  void InitializationGen::add_init_stmt(std::string baseName, SgType* type, SgStatement* pBeforeStmt, std::map<std::string, std::string>& pVarNameToSizeVar)
  {
    count = 0;
    SgStatement* tmpStmt = beforeStmt;
    beforeStmt = pBeforeStmt;
    std::map<std::string, std::string>* tempVarNameToSizeVar = varNameToSizeVar;
    varNameToSizeVar = &pVarNameToSizeVar;

    add_init_stmt(SageBuilder::buildVarRefExp(baseName), type, false);
    
    beforeStmt = tmpStmt;
    varNameToSizeVar = tempVarNameToSizeVar;
  }

  void InitializationGen::add_init_stmt(SgExpression* var, SgType* type, bool shouldAppend)
  {
    if(SageInterface::isScalarType(type))
    {
      add_init_stmt_scalar(var, type, shouldAppend);
    }
    else if(isSgArrayType(type))
    {
      add_init_arrays(var, NULL, isSgArrayType(type), shouldAppend, false);
    }
    else if(SageInterface::isPointerType(type))
    {
      add_init_ptr(var, NULL, type, shouldAppend, false);
    }
    else if(isSgTypedefType(type))
    {
      add_init_stmt(var, isSgTypedefDeclaration(isSgTypedefType(type)->get_declaration())->get_base_type(), shouldAppend);
    }
    else if(isSgModifierType(type))
    {
      add_init_stmt(var, isSgModifierType(type)->get_base_type(), shouldAppend);
    }
    else if(isSgClassType(type))
    {
      SgClassDeclaration* decl =  isSgClassDeclaration(isSgClassType(type)->get_declaration());
      if(!decl->get_definition())
      {
        decl = typeMgr->getClassDeclaration(decl->get_name().getString(), true);
      }
      assert(decl->get_definition());
      std::vector<SgDeclarationStatement*>& classMembers = decl->get_definition()->get_members();

      if(decl->get_class_type() == SgClassDeclaration::class_types::e_struct)
      {
        if(contains_const_elems(isSgClassType(type)))
        {
            assert(isSgVarRefExp(var));
            add_init_struct_initList(isSgVarRefExp(var)->get_symbol()->get_name(), classMembers, type, shouldAppend);
        }
        else
        {
          for(std::vector<SgDeclarationStatement*>::iterator memIt = classMembers.begin(); memIt != classMembers.end(); memIt++)
          {
            std::vector<SgInitializedName*>& vars = isSgVariableDeclaration(*memIt)->get_variables();
            for(std::vector<SgInitializedName*>::iterator varIt = vars.begin(); varIt != vars.end(); varIt++)
            {  
              add_init_stmt(SageBuilder::buildDotExp(var, SageBuilder::buildVarRefExp((*varIt)->get_name())), (*varIt)->get_type(), shouldAppend);
            }
          }
        }
      }
      else
      {
        assert(0);
      }
    }
    else
    {
      // TODO  support more variable types
      assert(0);
    }
  }

  void InitializationGen::equal_stmts(SgExpression* varS, SgExpression* varP, SgType* type, bool shouldAppend)
  {
    if(SageInterface::isScalarType(type))
    {
      equal_stmts_scalar(varS, varP, shouldAppend);
    }
    else if(isSgArrayType(type))
    {
      add_init_arrays(varS, varP, isSgArrayType(type), shouldAppend, true);
    }
    else if(SageInterface::isPointerType(type))
    {
      add_init_ptr(varS, varP, type, shouldAppend, true);
    }
    else if(isSgTypedefType(type))
    {
      equal_stmts(varS, varP, isSgTypedefDeclaration(isSgTypedefType(type)->get_declaration())->get_base_type(), shouldAppend);
    }
    else if(isSgModifierType(type))
    {
      equal_stmts(varS, varP, isSgModifierType(type)->get_base_type(), shouldAppend);
    }
    else if(isSgClassType(type))
    {
      SgClassDeclaration* decl =  isSgClassDeclaration(isSgClassType(type)->get_declaration());
      if(!decl->get_definition())
      {
        decl = typeMgr->getClassDeclaration(decl->get_name().getString(), true);
      }
      assert(decl->get_definition());
      std::vector<SgDeclarationStatement*>& classMembers = decl->get_definition()->get_members();

      if(decl->get_class_type() == SgClassDeclaration::class_types::e_struct)
      {
        if(contains_const_elems(isSgClassType(type)))
        {
          assert(isSgVarRefExp(varP));
          equal_struct_initList(varS, isSgVarRefExp(varP)->get_symbol()->get_name(), classMembers, type, shouldAppend);
        }
        else
        {
          for(std::vector<SgDeclarationStatement*>::iterator memIt = classMembers.begin(); memIt != classMembers.end(); memIt++)
          {
            std::vector<SgInitializedName*>& vars = isSgVariableDeclaration(*memIt)->get_variables();
            for(std::vector<SgInitializedName*>::iterator varIt = vars.begin(); varIt != vars.end(); varIt++)
            {  
              equal_stmts(SageBuilder::buildDotExp(varS, SageBuilder::buildVarRefExp((*varIt)->get_name())), SageBuilder::buildDotExp(varP, SageBuilder::buildVarRefExp((*varIt)->get_name())), (*varIt)->get_type(), shouldAppend);
            }
          }
        }
      }
      else
      {
        assert(0);
      }
    }
    else
    {
    // TODO  support more variable types
      assert(0);
    }
  }

  void InitializationGen::add_init_and_equal_stmts(SgExpression* varS, SgExpression* varP, SgType* type, bool shouldAppend)
  {
    if(SageInterface::isScalarType(type))
    {
      add_init_and_equal_stmts_scalar(varS, varP, type, shouldAppend);
    }
    else if(isSgArrayType(type))
    {
      add_init_arrays(varS, varP, isSgArrayType(type), shouldAppend, false);
    }
    else if(SageInterface::isPointerType(type))
    {
      add_init_ptr(varS, varP, type, shouldAppend, false);
    }
    else if(isSgTypedefType(type))
    {
      add_init_and_equal_stmts(varS, varP, isSgTypedefDeclaration(isSgTypedefType(type)->get_declaration())->get_base_type(), shouldAppend);
    }
    else if(isSgModifierType(type))
    {
      add_init_and_equal_stmts(varS, varP, isSgModifierType(type)->get_base_type(), shouldAppend);
    }
    else if(isSgClassType(type))
    {
      SgClassDeclaration* decl =  isSgClassDeclaration(isSgClassType(type)->get_declaration());
      if(!decl->get_definition())
      {
        decl = typeMgr->getClassDeclaration(decl->get_name().getString(), true);
      }
      assert(decl->get_definition());
      std::vector<SgDeclarationStatement*>& classMembers = decl->get_definition()->get_members();

      if(decl->get_class_type() == SgClassDeclaration::class_types::e_struct)
      {
        if(contains_const_elems(isSgClassType(type)))
        {
          assert(isSgVarRefExp(varS) && isSgVarRefExp(varP));
          add_init_struct_initList(isSgVarRefExp(varS)->get_symbol()->get_name(), classMembers, type, shouldAppend);
          equal_struct_initList(varS, isSgVarRefExp(varP)->get_symbol()->get_name(), classMembers, type, shouldAppend);
        }
        else
        {
          for(std::vector<SgDeclarationStatement*>::iterator memIt = classMembers.begin(); memIt != classMembers.end(); memIt++)
          {
            std::vector<SgInitializedName*>& vars = isSgVariableDeclaration(*memIt)->get_variables();
            for(std::vector<SgInitializedName*>::iterator varIt = vars.begin(); varIt != vars.end(); varIt++)
            {  
              add_init_and_equal_stmts(SageBuilder::buildDotExp(varS, SageBuilder::buildVarRefExp((*varIt)->get_name())), SageBuilder::buildDotExp(varP, SageBuilder::buildVarRefExp((*varIt)->get_name())), (*varIt)->get_type(), shouldAppend);
            }
          }
        }
      }
      else
      {
        assert(0);
      }
    }
    else
    {
    // TODO support more variable types
      assert(0);
    }
  }

  void InitializationGen::add_init_ptr(SgExpression* varS, SgExpression* varP, SgType* type, bool shouldAppend, bool onlyEqualize)
  {  
    // only initialization, space allocation need to be done with declaration
    if(SageInterface::isPointerType(type))
    {
      SgType* elementType = SageInterface::getElementType(type);
      SgName size;
      SgExpression* ptrS;
      SgExpression* ptrP;
      SgName counter = SgName(prefixForCounter + std::to_string(count++));

      if(varP == NULL)
      {
        assert(varNameToSizeVar->find(varS->unparseToString()) != varNameToSizeVar->end());
        size = SgName((*varNameToSizeVar)[varS->unparseToString()]);
        ptrP = NULL;
      }
      else
      {
        assert(varNameToSizeVar->find(varP->unparseToString()) != varNameToSizeVar->end());
        size = SgName((*varNameToSizeVar)[varP->unparseToString()]);
        ptrP = SageBuilder::buildPntrArrRefExp(varP, SageBuilder::buildVarRefExp(counter));

      }
      ptrS = SageBuilder::buildPntrArrRefExp(varS, SageBuilder::buildVarRefExp(counter));

      SgBasicBlock* body = SageBuilder::buildBasicBlock_nfi();

      
      if(shouldAppend)
      {
        SageInterface::appendStatement(helper.constructForLoop(counter, SageBuilder::buildVarRefExp(size), body, SageBuilder::buildIntType()));
      }
      else
      {
        SageInterface::insertStatement(beforeStmt, helper.constructForLoop(counter, SageBuilder::buildVarRefExp(size), body, SageBuilder::buildIntType()));
      }

      SageBuilder::pushScopeStack(body);
      if(ptrP == NULL)
      {
        add_init_stmt(ptrS, elementType, true);
      }
      else
      {
        if(onlyEqualize)
        {
          equal_stmts(ptrS, ptrP, elementType, true);
        }
        else
        {
          add_init_and_equal_stmts(ptrS, ptrP, elementType, true);
        }
      }
      SageBuilder::popScopeStack();
    }
    else
    {
      // TODO extend to support e.g. typedefs
      assert(0); 
    }
  }

  void InitializationGen::add_init_arrays(SgExpression* varS, SgExpression* varP, SgArrayType* type, bool shouldAppend, bool onlyEqualize)
  { 
    std::vector<SgExpression*> dimLen = SageInterface::get_C_array_dimensions(type);
    assert((int)dimLen.size()==SageInterface::getDimensionCount(type)+1);	
    SgName counter;
    SgBasicBlock* body;
    SgExpression* arrVS = varS;
    SgExpression* arrVP = varP; 
    SgType* baseType = type;

    std::string* upperBoundVarName = NULL;

    if(varP == NULL)
    {
      if(varNameToSizeVar->find(varS->unparseToString())!= varNameToSizeVar->end())
      {
        upperBoundVarName = &((*varNameToSizeVar)[varS->unparseToString()]);
      }
      else
      {
        upperBoundVarName = NULL;
      }
    }
    else
    {
      if(varNameToSizeVar->find(varP->unparseToString())!= varNameToSizeVar->end())
      {
        upperBoundVarName = &((*varNameToSizeVar)[varP->unparseToString()]);
      }
      else
      {
        upperBoundVarName = NULL;
      }
    }

    for(unsigned int i=1; i<dimLen.size(); i++)
    {
      body = SageBuilder::buildBasicBlock_nfi();
      counter = SgName(prefixForCounter + std::to_string(count++));
      arrVS = SageBuilder::buildPntrArrRefExp(arrVS, SageBuilder::buildVarRefExp(counter));
      if(arrVP != NULL)
      {
        arrVP = SageBuilder::buildPntrArrRefExp(arrVP, SageBuilder::buildVarRefExp(counter));
      }

      SageInterface::const_int_expr_t  arr_size = SageInterface::evaluateConstIntegerExpression(dimLen[i]);
      if(!isSgNullExpression(dimLen[i]) && arr_size.hasValue_)
      {
        assert(arr_size.value_>=0); 
        SgType* counterType;
        SgExpression* upperBound;
        
        if(arr_size.value_ <= INT_MAX)
        {
          counterType = SageBuilder::buildIntType();
          upperBound = SageBuilder::buildIntVal(arr_size.value_);
        }
        else if (arr_size.value_ <= UINT_MAX)
        {
          counterType = SageBuilder::buildUnsignedIntType();
          upperBound = SageBuilder::buildUnsignedIntVal(arr_size.value_);
        }
        else if (arr_size.value_ <= LONG_MAX)
        {
          counterType = SageBuilder::buildLongType();
          upperBound = SageBuilder::buildLongIntVal(arr_size.value_);
        }
        else if (arr_size.value_ <= ULONG_MAX)
        {
          counterType = SageBuilder::buildUnsignedLongType();
          upperBound = SageBuilder::buildUnsignedLongVal(arr_size.value_);
        }
        else if (arr_size.value_ <= LLONG_MAX)
        {
          counterType = SageBuilder::buildLongLongType();
          upperBound = SageBuilder::buildLongLongIntVal(arr_size.value_);
        }
        else if (arr_size.value_ <= ULLONG_MAX)
        {
          counterType = SageBuilder::buildUnsignedLongLongType();
          upperBound = SageBuilder::buildUnsignedLongLongIntVal(arr_size.value_);
        }
        else
        {
          assert(0);
        }

        if(shouldAppend) 
        {
          SageInterface::appendStatement(helper.constructForLoop(counter, upperBound, body, counterType));
        }
        else
        {
          SageInterface::insertStatement(beforeStmt, helper.constructForLoop(counter, upperBound, body, counterType));
          shouldAppend = true;
        }
      }
      else 
      {
        // assume that we can reuse initializer expression (TODO check what happens if none exists)
        assert(upperBoundVarName);
        if(shouldAppend) 
        {
          SageInterface::appendStatement(helper.constructForLoop(counter, SageBuilder::buildVarRefExp(*upperBoundVarName+"_"+std::to_string(i)), body, SageBuilder::buildIntType()));
        }
        else
        {
          SageInterface::insertStatement(beforeStmt, helper.constructForLoop(counter, SageBuilder::buildVarRefExp(*upperBoundVarName+"_"+std::to_string(i)), body, SageBuilder::buildIntType()));
          shouldAppend = true;
        }
      }

      baseType = isSgArrayType(baseType)->get_base_type();
      SageBuilder::pushScopeStack(body);
    }

    if(varP == NULL)
    {
      add_init_stmt(arrVS, baseType, true);
    }
    else
    {
      if(onlyEqualize)
      {
        equal_stmts(arrVS, arrVP, baseType, true);
      }
      else
      {
        add_init_and_equal_stmts(arrVS, arrVP, baseType, true);
      }
    }

    for(unsigned int i=1; i<dimLen.size(); i++)
    {
      delete dimLen[i];
      SageBuilder::popScopeStack();
    }
  }
  
  SgAggregateInitializer* InitializationGen::get_struct_init_initializer(std::vector<SgDeclarationStatement*>& classMembers, SgType* type)
  {
    std::vector<SgExpression*> exprs;
    for(std::vector<SgDeclarationStatement*>::iterator memIt = classMembers.begin(); memIt != classMembers.end(); memIt++)
    {
      std::vector<SgInitializedName*>& vars = isSgVariableDeclaration(*memIt)->get_variables();
      for(std::vector<SgInitializedName*>::iterator varIt = vars.begin(); varIt != vars.end(); varIt++)
      { 
        SgType* varMemType = (*varIt)->get_type();
        while(isSgTypedefType(varMemType))
        {
          varMemType = isSgTypedefDeclaration(isSgTypedefType(varMemType)->get_declaration())->get_base_type();
        }
        
        if(SageInterface::isScalarType(varMemType))
        {
          exprs.push_back(get_scalar_init_expr(varMemType));
        }
        else
        {
          if(isSgClassType(varMemType))
          {
            SgClassDeclaration* decl =  isSgClassDeclaration(isSgClassType(varMemType)->get_declaration());
            assert(decl->get_class_type() == SgClassDeclaration::class_types::e_struct);

            if(!decl->get_definition())
            {
              decl = typeMgr->getClassDeclaration(decl->get_name().getString(), true);
            }
            assert(decl->get_definition());
            
            exprs.push_back(get_struct_init_initializer(decl->get_definition()->get_members(), (*varIt)->get_type()));
          } 
          else
          {
            assert(0);
          }
        }
      }
    }
    
    return SageBuilder::buildAggregateInitializer(SageBuilder::buildExprListExp(exprs), type);   
  }
      
  void InitializationGen::add_init_struct_initList(SgName var, std::vector<SgDeclarationStatement*>& classMembers, SgType* type, bool shouldAppend)
  {
    SgStatement* assign = SageBuilder::buildVariableDeclaration (var, type, get_struct_init_initializer(classMembers, type));//SageBuilder::buildAssignStatement (var, get_struct_init_initializer(classMembers, type)); 
    if(shouldAppend)
    {
      SageInterface::appendStatement(assign);
    }
    else
    {
      SageInterface::insertStatement(beforeStmt, assign);
    }
  }
  
  SgExpression* InitializationGen::get_scalar_init_expr(SgType* type)
  {
    NondetType nonType;
    
    while(isSgTypedefType(type))
    {
      type = isSgTypedefDeclaration(isSgTypedefType(type)->get_declaration())->get_base_type();
    }
    
    if(isSgTypeComplex(type))
    {
      SgType* baseType = isSgTypeComplex(type)->get_base_type();
      assert(SageInterface::isScalarType(baseType) && !isSgTypeComplex(baseType));
      nonType = helper.get_nondet_type(baseType);
      assert(nonType != UNKNOWN);
      protoMgr->add_verifier_nondet(nonType);
      inclMgr->include_complex();
      
      return SageBuilder::buildAddOp(SageBuilder::buildFunctionCallExp (SgName(helper.get_nondet_fun_name(nonType)), baseType,  SageBuilder::buildExprListExp()), SageBuilder::buildMultiplyOp(SageBuilder::buildFunctionCallExp (SgName(helper.get_nondet_fun_name(nonType)), baseType,  SageBuilder::buildExprListExp()), SageBuilder::buildVarRefExp(SgName("I"))));
    }
    else
    { 
      nonType = helper.get_nondet_type(type);
      assert(nonType != UNKNOWN);
      protoMgr->add_verifier_nondet(nonType);
      
      return helper.generate_nondet_call(nonType, type);
    }
  }

  void InitializationGen::add_init_stmt_scalar(SgExpression* var, SgType* type, bool shouldAppend)
  { 
    SgExprStatement* assign = SageBuilder::buildAssignStatement(var, get_scalar_init_expr(type));

    if(shouldAppend)
    {
      SageInterface::appendStatement(assign);
    }
    else
    {
      SageInterface::insertStatement(beforeStmt, assign);
    }
  }

  void InitializationGen::equal_stmts_scalar(SgExpression* varS, SgExpression* varP, bool shouldAppend)
  {
    SgExprStatement* assign = SageBuilder::buildAssignStatement (varP, varS);
    if(shouldAppend)
    {
      SageInterface::appendStatement(assign);
    }
    else
    {
      SageInterface::insertStatement(beforeStmt, assign);
    }
  }

  void InitializationGen::add_init_and_equal_stmts_scalar(SgExpression* varS, SgExpression* varP, SgType* type, bool shouldAppend)
  {
    add_init_stmt_scalar(varS, type, shouldAppend);
    equal_stmts_scalar(varS, varP, shouldAppend);
  }
  
  SgAggregateInitializer* InitializationGen::get_struct_equal_initializer(SgExpression* varS, std::vector<SgDeclarationStatement*>& classMembers, SgType* type)
  {
    std::vector<SgExpression*> exprs;
    for(std::vector<SgDeclarationStatement*>::iterator memIt = classMembers.begin(); memIt != classMembers.end(); memIt++)
    {
      std::vector<SgInitializedName*>& vars = isSgVariableDeclaration(*memIt)->get_variables();
      for(std::vector<SgInitializedName*>::iterator varIt = vars.begin(); varIt != vars.end(); varIt++)
      { 
        SgType* varMemType = (*varIt)->get_type();
        while(isSgTypedefType(varMemType))
        {
          varMemType = isSgTypedefDeclaration(isSgTypedefType(varMemType)->get_declaration())->get_base_type();
        }
          
        if(SageInterface::isScalarType(varMemType))
        {
          exprs.push_back(SageBuilder::buildDotExp(varS, SageBuilder::buildVarRefExp((*varIt)->get_name())));
        }
        else
        {
          if(isSgClassType(varMemType))
          {
            SgClassDeclaration* decl =  isSgClassDeclaration(isSgClassType(varMemType)->get_declaration());
            assert(decl->get_class_type() == SgClassDeclaration::class_types::e_struct);

            if(!decl->get_definition())
            {
              decl = typeMgr->getClassDeclaration(decl->get_name().getString(), true);
            }
            assert(decl->get_definition());
            
            exprs.push_back(get_struct_equal_initializer(SageBuilder::buildDotExp(varS, SageBuilder::buildVarRefExp((*varIt)->get_name())), decl->get_definition()->get_members(),(*varIt)->get_type()));
          } 
          else
          {
            assert(0);
          }
        }
      }
    }
    
    return SageBuilder::buildAggregateInitializer(SageBuilder::buildExprListExp(exprs), type); 
  }
  
  void InitializationGen::equal_struct_initList(SgExpression* varS, SgName varP, std::vector<SgDeclarationStatement*>& classMembers, SgType* type, bool shouldAppend)
  {       
    SgStatement* assign = SageBuilder::buildVariableDeclaration (varP, type, get_struct_equal_initializer(varS, classMembers, type)); //SageBuilder::buildAssignStatement (varP, get_struct_equal_initializer(varS, classMembers, type));
    if(shouldAppend)
    {
      SageInterface::appendStatement(assign);
    }
    else
    {
      SageInterface::insertStatement(beforeStmt, assign);
    }
  }
