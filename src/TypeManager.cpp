//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "TypeManager.h"
#include <assert.h>

  void TypeManager::init_type_Mgr(SgSourceFile* seqProg, SgSourceFile* parProg)
  {
    SgGlobal* seqGlobal = seqProg->get_globalScope();
    SgGlobal* parGlobal = parProg->get_globalScope();

    seqClassNameToDecl.clear();
    parClassNameToDecl.clear();
    add_defining_classes(seqGlobal, seqClassNameToDecl);
    add_defining_classes(parGlobal, parClassNameToDecl);
    
    seqFunNameToDeclOrig.clear();
    seqFunNameToDeclCopy.clear();
    parFunNameToDecl.clear();
    add_defining_fun(seqGlobal, seqFunNameToDeclOrig);
    add_defining_fun(parGlobal, parFunNameToDecl);

    typesToDefine.clear();
    seenTypes.clear();
  }

  SgClassDeclaration* TypeManager::getClassDeclaration(std::string className, bool inSeq)
  {
    if(inSeq)
    {
      if(seqClassNameToDecl.find(className) != seqClassNameToDecl.end())
      {
        return seqClassNameToDecl[className];
      }
    }
    else
    {
      if(parClassNameToDecl.find(className) != parClassNameToDecl.end())
      {
        return parClassNameToDecl[className];
      }
    }
    return NULL;
  }

  SgFunctionDeclaration* TypeManager::getFunDeclaration(std::string funName, bool inSeq)
  {
    if(inSeq)
    {
      if(seqFunNameToDeclOrig.find(funName) != seqFunNameToDeclOrig.end())
      {
        return seqFunNameToDeclOrig[funName];
      }
      if(seqFunNameToDeclCopy.find(funName) != seqFunNameToDeclCopy.end())
      {
        return seqFunNameToDeclCopy[funName];
      }
    }
    else
    {
      if(parFunNameToDecl.find(funName) != parFunNameToDecl.end())
      {
        return parFunNameToDecl[funName];
      }
    }
    return NULL;
  }

  void TypeManager::add_defining_classes(SgGlobal* globalScope, std::map<std::string, SgClassDeclaration*>& classNameToDecl)
  {
    SgClassDeclaration* decl;
    Rose_STL_Container<SgNode*> decls = NodeQuery::querySubTree (globalScope, V_SgClassDeclaration); 
    for (Rose_STL_Container<SgNode*>::iterator i = decls.begin(); i != decls.end(); i++)
    {
      decl = isSgClassDeclaration(*i);
      if(decl->get_definition() != NULL )
      {
        assert(classNameToDecl.find(decl->get_name().getString()) == classNameToDecl.end());
        classNameToDecl[decl->get_name().getString()] = decl;
      }
    }
  }

  void TypeManager::add_defining_fun(SgGlobal* globalScope, std::map<std::string, SgFunctionDeclaration*>& funNameToDecl)
  {
    // note that in C function names are sufficient to identify function, typically no overloading
    SgFunctionDeclaration* funDecl;
    Rose_STL_Container<SgNode*> decls = NodeQuery::querySubTree (globalScope, V_SgFunctionDeclaration); 
    for (Rose_STL_Container<SgNode*>::iterator i = decls.begin(); i != decls.end(); i++)
    {
      funDecl = isSgFunctionDeclaration(*i);
      if(funDecl->get_definition() != NULL )
      {
        assert(funNameToDecl.find(funDecl->get_name().getString()) == funNameToDecl.end());
        funNameToDecl[funDecl->get_name().getString()] = funDecl;
      }
    }
  }

  void TypeManager::addFunDeclaration(std::string funName, SgFunctionDeclaration* funDecl, bool inSeq)
  {
    assert(funDecl);
    if(inSeq)
    {
      seqFunNameToDeclCopy[funName] = funDecl;
    }
    else
    {
      parFunNameToDecl[funName] = funDecl;
    }
    
  }

  void TypeManager::inspect_type_for_definition(SgType* pType, bool inSeq)
  {
    if(isSgNamedType(pType))
    {
      if(seenTypes.find(isSgNamedType(pType)->get_name().getString()) == seenTypes.end()) 
      {
        seenTypes.insert(isSgNamedType(pType)->get_name().getString());
        
        assert(helper.is_global(isSgNamedType(pType)->get_declaration()->get_declarationScope())); 
        if(!helper.in_header(isSgNamedType(pType)->get_declaration())) // only if not from include file
        {
          if(isSgTypedefType(pType))
          {
            typesToDefine.insert(isSgTypedefType(pType)->get_declaration());
            inspect_type_for_definition(isSgTypedefDeclaration(isSgTypedefType(pType)->get_declaration())->get_base_type(), inSeq);
          }
          else if(isSgClassType(pType))
          {
            SgClassDeclaration* decl = isSgClassDeclaration(isSgClassType(pType)->get_declaration());
            if(decl->get_class_type() == SgClassDeclaration::class_types::e_struct || decl->get_class_type() == SgClassDeclaration::class_types::e_union)
            {
              if(!decl->get_definition())
              {
                if(!decl->get_isUnNamed() && SageInterface::getEnclosingNode<SgDeclarationStatement>(decl) == NULL) 
                {
                  typesToDefine.insert(decl);	
                }
                decl = getClassDeclaration(isSgClassType(pType)->get_name().getString(), inSeq);
                assert(decl && decl->get_definition());
              }

              SgDeclarationStatement* enclDecl = SageInterface::getEnclosingNode<SgDeclarationStatement>(decl);
              if(enclDecl == NULL)
              { 
                if(!decl->get_isUnNamed())
                {
                  typesToDefine.insert(decl); 
                }
              }
              else
              {
                if(isSgTypedefDeclaration(enclDecl))
                {
                  if(seenTypes.find(isSgTypedefDeclaration(enclDecl)->get_type()->get_name().getString()) == seenTypes.end())
                  {
                    inspect_type_for_definition(isSgTypedefDeclaration(enclDecl)->get_type(), inSeq);
                    return;
                  }
                }
                else if(isSgClassDeclaration(enclDecl)) 
                {
                  if(seenTypes.find(isSgClassDeclaration(enclDecl)->get_type()->get_name().getString()) == seenTypes.end())
                  {
                    inspect_type_for_definition(isSgClassDeclaration(enclDecl)->get_type(), inSeq);
                    return;
                  }
                }
                else if(!isSgVariableDeclaration(enclDecl))
                {
                  assert(0);  
                }
              }

              std::vector<SgDeclarationStatement*>& mems = decl->get_definition()->get_members();
              for(std::vector<SgDeclarationStatement*>::iterator memIt = mems.begin(); memIt != mems.end(); memIt++)
              {
                if(isSgVariableDeclaration(*memIt))
                {
                  std::vector<SgInitializedName*>& vars = isSgVariableDeclaration(*memIt)->get_variables();
                  for(std::vector<SgInitializedName*>::iterator varIt = vars.begin(); varIt != vars.end(); varIt++)
                  {
                    inspect_type_for_definition((*varIt)->get_type(), inSeq);  
                  }
                }
                else{
                  assert(0); // member, but no variable declaration
                }
              }
            }
            else
            {
              assert(0); // only support structs and union classes
            }
          }
          else
          {
            assert(0); // only support classes and typedefs as named types
          }
        }
      }
    }
    else
    {
      if(isSgPointerType(pType))
      {
        inspect_type_for_definition(isSgPointerType(pType)->get_base_type(), inSeq);
      }
      else if(isSgArrayType(pType))
      {
        inspect_type_for_definition(isSgArrayType(pType)->get_base_type(), inSeq);
      }
      else if(isSgModifierType(pType)) 
      {
        inspect_type_for_definition(isSgModifierType(pType)->get_base_type(), inSeq);
      }
      else if(isSgTypeComplex(pType))
      {
        inspect_type_for_definition(isSgTypeComplex(pType)->get_base_type(), inSeq);
      } 
    }
  }

  void TypeManager::insert_types(SgSourceFile* file)
  {
    // sort typesToDefine based on source code line number
    std::vector<SgDeclarationStatement*> outTypes = std::vector<SgDeclarationStatement*>(typesToDefine.size());
    std::copy(typesToDefine.begin(), typesToDefine.end(), outTypes.begin()); 

    SgDeclarationStatement* tmp;
    Sg_File_Info* infoLeft;
    Sg_File_Info* infoRight;

    // bubble sort, increasing order
    for(unsigned int i=0; i<outTypes.size(); i++)
    {
      for(unsigned int j=1; j<outTypes.size()-i; j++)
      {
        infoLeft = outTypes[j-1]->get_startOfConstruct();
        infoRight = outTypes[j]->get_startOfConstruct();
        if(infoLeft->get_line() < infoRight->get_line() || (infoLeft->get_line() == infoRight->get_line() && infoLeft->get_col()<infoRight->get_col()))
        {
          tmp = outTypes[j];
          outTypes[j] = outTypes[j-1];
          outTypes[j-1] = tmp;
        }
      }
    }

    for(std::vector<SgDeclarationStatement*>::iterator typeIt = outTypes.begin(); typeIt != outTypes.end(); typeIt++)
    { 
      SageInterface::prependStatement(generate_type_decl(*typeIt)); 
    }
  }

  SgDeclarationStatement* TypeManager::generate_type_decl(SgDeclarationStatement* typeDecl)
  {
    SgDeclarationStatement* copyDecl = SageInterface::deepCopy<SgDeclarationStatement>(typeDecl);

    Rose_STL_Container<SgNode*> decls = NodeQuery::querySubTree (copyDecl,  V_SgClassDeclaration);
    SgClassDeclaration* classDecl;
    SgClassDeclaration* newClassDecl;
    for (Rose_STL_Container<SgNode*>::iterator i = decls.begin(); i != decls.end(); i++)
    {
      classDecl = isSgClassDeclaration(*i);
      if(classDecl != copyDecl && !classDecl->get_definition())
      {
        newClassDecl = getClassDeclaration(classDecl->get_type()->get_name().getString(), true);
        if(!newClassDecl)
        {
          newClassDecl = getClassDeclaration(classDecl->get_type()->get_name().getString(), false);
        }
        assert(newClassDecl);
        SageInterface::replaceStatement(classDecl, generate_type_decl(newClassDecl));
      }
    }

    return copyDecl;
  }
  
  void TypeManager::reset_types()
  {
    typesToDefine.clear();
    seenTypes.clear();  
    seqFunNameToDeclCopy.clear();
  }
