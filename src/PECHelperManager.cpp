//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "PECHelperManager.h"
#include "rose.h"
#include <assert.h>

  PECHelperManager::PECHelperManager()
  {
    std::string insertNext = std::string(ignored_globals_list[0]);
    int i = 0;
    while(!insertNext.empty())
    {
      ignoredGlobals.insert(insertNext);
      i++;
      insertNext = std::string(ignored_globals_list[i]);
    }
  }

  SgStatement* PECHelperManager::constructForLoop(SgName& loopCounter, SgExpression* upperBound, SgBasicBlock* body, SgType* counterType)
  {
    assert(counterType->isIntegerType());
    return SageBuilder::buildForStatement_nfi(
             SageBuilder::buildVariableDeclaration (loopCounter, counterType, SageBuilder::buildAssignInitializer(SageBuilder::buildIntVal(0), SageBuilder::buildIntType())),  
             SageBuilder::buildExprStatement(SageBuilder::buildLessThanOp(SageBuilder::buildVarRefExp(loopCounter), upperBound)), 
             SageBuilder::buildPlusPlusOp(SageBuilder::buildVarRefExp(loopCounter), SgUnaryOp::postfix), 
             body);
  }

  SgStatement* PECHelperManager::malloc_assign(std::string& varName, std::string& varSize, SgType* type)
  {
    if(SageInterface::isPointerType(type))
    { 
      return SageBuilder::buildAssignStatement(SageBuilder::buildVarRefExp(SgName(varName)),
          SageBuilder::buildCastExp(SageBuilder::buildFunctionCallExp (SgName("malloc"), SageBuilder::buildPointerType(SageBuilder::buildVoidType()), 
          SageBuilder::buildExprListExp(SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(SgName(varSize)), SageBuilder::buildSizeOfOp(SageInterface::getElementType(type))))), type));
    }
    else
    {
      assert(0);
    }
  }

  SgStatement* PECHelperManager::free_stmt(std::string& varName, SgType* type)
  {
    if(SageInterface::isPointerType(type))
    { 
       return SageBuilder::buildFunctionCallStmt(SgName("free"), SageBuilder::buildVoidType(), 
          SageBuilder::buildExprListExp(SageBuilder::buildVarRefExp(SgName(varName))));
    }
    else
    {
      assert(0);
    }
  }

  std::string PECHelperManager::add_rename_suffix(std::string name)
  {
    return name+seqVarSuffix;
  }

  bool PECHelperManager::in_header(SgNode* node)
  {
    // only heuristic
    const std::string& fileName = node->get_file_info()->get_filenameString();
    return fileName.size() >= 2 && fileName.find(".h", fileName.size()-2) != std::string::npos;
  }

  bool PECHelperManager::is_global(SgScopeStatement* scope)
  {
    return SageInterface::getGlobalScope(scope) == scope;
  }
  
  bool PECHelperManager::is_const_type(SgType* type)
  {
    if(SageInterface::isConstType(type))
    {
      return true;
    }
    
    while(isSgArrayType(type))
    {
      type = isSgArrayType(type)->get_base_type();
    }
    
    SgModifierType* modType = isSgModifierType(type);
    while(modType)
    { 
      if(modType->get_typeModifier().get_constVolatileModifier().isConst())
      {
        return true;
      }
      
      modType = isSgModifierType(modType->get_base_type());
    }
    // TODO handle typedef, etc.?
    
    return false;
  }

  NondetType PECHelperManager::get_nondet_type(SgType* type)
  {
    // utilizes that types are shared, at least if build with SageBuilder
    if(isSgModifierType(type))
    {
      type = isSgModifierType(type)->get_base_type(); 
    }

    if(type == SageBuilder::buildIntType() || type == SageBuilder::buildSignedIntType ())
    {
      return INT;
    }
    else if(type == SageBuilder::buildDoubleType ())
    {
      return DOUBLE;
    }
    else if(type == SageBuilder::buildFloatType ())
    {
      return FLOAT;
    }
    else if(type == SageBuilder::buildUnsignedIntType ())
    {
      return UINT;
    }
    else if(type == SageBuilder::buildCharType () || type == SageBuilder::buildSignedCharType ())
    {
      return CHAR;
    }
    else if(type == SageBuilder::buildShortType () || type == SageBuilder::buildSignedShortType ())
    {
      return SHORT;
    }
    else if(type == SageBuilder::buildLongType () || type == SageBuilder::buildSignedLongType ())
    {
      return LONG;
    }
    else if(type == SageBuilder::buildUnsignedShortType ())
    {
      return USHORT;
    }
    else if(type == SageBuilder::buildUnsignedCharType ())
    {
      return UCHAR;
    }
    else if(type == SageBuilder::buildUnsignedLongType())
    {
      return ULONG;
    }
    else if(type == SageBuilder::buildBoolType())
    {
      return BOOLEAN;
    }
    return UNKNOWN;
  }

  NondetType PECHelperManager::get_nondet_type(std::string funName)
  {
    if(funName.rfind(nondetPrefix,0) == 0)
    {
      std::string suffix = funName.substr(vNondetPrefixLen, funName.length()-vNondetPrefixLen);
      for(unsigned int i=0; i<NondetTypeSize; i++)
      {
        if(suffix.compare(nondetFunSuffix[i]) == 0)
        {
          return (NondetType) i;
        }
      }
    }
    return UNKNOWN;
  }

  const char* PECHelperManager::get_nondet_fun_suffix(NondetType nondet)
  {
    return nondetFunSuffix[nondet];
  }

 SgType* PECHelperManager::get_type(NondetType nondet)
  {
    switch(nondet)
    {
      case INT:
        return SageBuilder::buildIntType();
      case DOUBLE:
        return SageBuilder::buildDoubleType ();
      case FLOAT:
        return SageBuilder::buildFloatType ();
      case CHAR:
        return SageBuilder::buildCharType ();
      case UINT:
        return SageBuilder::buildUnsignedIntType ();
      case SHORT:
        return SageBuilder::buildShortType ();
      case LONG:
        return SageBuilder::buildLongType ();
      case UCHAR:
        return SageBuilder::buildUnsignedCharType ();
      case USHORT:
        return SageBuilder::buildUnsignedShortType ();
      case ULONG:
        return SageBuilder::buildUnsignedLongType();
      case BOOLEAN:
        return SageBuilder::buildBoolType();
      default:
        assert(0);
    }
  }

  std::string PECHelperManager::remove_rename_suffix(std::string varName)
  {
    if(varName.length()<suffixLen)
    {
      return varName;
    }
    return varName.substr(0,varName.length()-suffixLen);
  }

  bool PECHelperManager::shouldIgnoreGlobal(std::string globalVarName)
  {

    return ignoredGlobals.find(globalVarName) != ignoredGlobals.end();
  }

  std::string PECHelperManager::get_nondet_fun_name(NondetType nondet)
  {
    return std::string(vnondetPrefix)+ std::string(get_nondet_fun_suffix(nondet));
  }

  SgExprStatement* PECHelperManager::assign_nondet_val(SgExpression* lhs, NondetType nondet, SgType* type)
  {
    return SageBuilder::buildAssignStatement (lhs, generate_nondet_call(nondet, type));
  }
  
  SgFunctionCallExp* PECHelperManager::generate_nondet_call(NondetType nondet, SgType* type)
  {
    return SageBuilder::buildFunctionCallExp (SgName(get_nondet_fun_name(nondet)), type,  SageBuilder::buildExprListExp());
  }


