#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
double (*new)[10];
int _size_st1;
double (*old)[10];
int _size_st0;
double (*new_s)[10];

int main()
{
  int col;
  int row;
  double error;
  _size_st1 = __VERIFIER_nondet_int();
  new = ((double (*)[10])(malloc(_size_st1 * sizeof(double [10]))));
  _size_st0 = __VERIFIER_nondet_int();
  old = ((double (*)[10])(malloc(_size_st0 * sizeof(double [10]))));
  int col_s;
  int row_s;
  double error_s;
  new_s = ((double (*)[10])(malloc(_size_st1 * sizeof(double [10]))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      old[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      new_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      new[_f_ct0][_f_ct1] = new_s[_f_ct0][_f_ct1];
    }
  }
  error_s = __VERIFIER_nondet_double();
  error = error_s;
  row_s = __VERIFIER_nondet_int();
  row = row_s;
  col_s = __VERIFIER_nondet_int();
  col = col_s;
// (First) sequential code segment
  for (row_s = 1; row_s < 10 - 1; row_s++) {
    for (col_s = 1; col_s < 10 - 1; col_s++) {
      new_s[row_s][col_s] = (old[row_s - 1][col_s] + old[row_s + 1][col_s] + old[row_s][col_s - 1] + old[row_s][col_s + 1]) * 0.25;
      error_s += (old[row_s][col_s] - new_s[row_s][col_s]) * (old[row_s][col_s] - new_s[row_s][col_s]);
    }
  }
  
#pragma omp parallel for collapse(2) reduction(+: error)
  for (row = 1; row < 10 - 1; row++) {
    for (col = 1; col < 10 - 1; col++) {
      new[row][col] = (old[row - 1][col] + old[row + 1][col] + old[row][col - 1] + old[row][col + 1]) * 0.25;
      error += (old[row][col] - new[row][col]) * (old[row][col] - new[row][col]);
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      equal = equal && new_s[_f_ct0][_f_ct1] == new[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  equal = equal && error_s == error;
  assert(equal);
{
    free(new_s);
    free(old);
    free(new);
  }
  return 0;
}
