#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
double (*new)[10];
int _size_st3;
double (*old)[10];
int _size_st2;
double (*new_s)[10];
double (*old_s)[10];

int main()
{
  int col;
  int row;
  _size_st3 = __VERIFIER_nondet_int();
  new = ((double (*)[10])(malloc(_size_st3 * sizeof(double [10]))));
  _size_st2 = __VERIFIER_nondet_int();
  old = ((double (*)[10])(malloc(_size_st2 * sizeof(double [10]))));
  int col_s;
  int row_s;
  new_s = ((double (*)[10])(malloc(_size_st3 * sizeof(double [10]))));
  old_s = ((double (*)[10])(malloc(_size_st2 * sizeof(double [10]))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      old_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      old[_f_ct0][_f_ct1] = old_s[_f_ct0][_f_ct1];
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st3; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      new_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      new[_f_ct0][_f_ct1] = new_s[_f_ct0][_f_ct1];
    }
  }
  col_s = __VERIFIER_nondet_int();
  col = col_s;
// (First) sequential code segment
  for (row_s = 1; row_s < 10 - 1; row_s++) 
    for (col_s = 1; col_s < 10 - 1; col_s++) 
      new_s[row_s][col_s] = old_s[row_s][col_s] = ((double )0);
  
#pragma omp parallel for
  for (row = 1; row < 10 - 1; row++) {
    
#pragma omp parallel for
    for (col = 1; col < 10 - 1; col++) 
      new[row][col] = old[row][col] = ((double )0);
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      equal = equal && old_s[_f_ct0][_f_ct1] == old[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st3; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      equal = equal && new_s[_f_ct0][_f_ct1] == new[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
{
    free(old_s);
    free(new_s);
    free(old);
    free(new);
  }
  return 0;
}
