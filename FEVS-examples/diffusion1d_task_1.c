#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
double *u;
int _size_st0;
int nx;
double *u_s;

int main()
{
  _size_st0 = __VERIFIER_nondet_int();
  u = ((double *)(malloc(_size_st0 * sizeof(double ))));
  u_s = ((double *)(malloc(_size_st0 * sizeof(double ))));
  nx = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    u_s[_f_ct0] = __VERIFIER_nondet_double();
    u[_f_ct0] = u_s[_f_ct0];
  }
// (First) sequential code segment
  for (int i = 1; i < nx - 1; i++) 
    u_s[i] = 100.0;
  
#pragma omp parallel for
  for (int i = 1; i < nx - 1; i++) 
    u[i] = 100.0;
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    equal = equal && u_s[_f_ct0] == u[_f_ct0];
  }
  assert(equal);
{
    free(u_s);
    free(u);
  }
  return 0;
}
