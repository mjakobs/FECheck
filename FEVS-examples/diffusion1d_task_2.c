#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
double *u_new;
int _size_st2;
double *u;
int _size_st1;
double k;
int nx;
double *u_new_s;

int main()
{
  _size_st2 = __VERIFIER_nondet_int();
  u_new = ((double *)(malloc(_size_st2 * sizeof(double ))));
  _size_st1 = __VERIFIER_nondet_int();
  u = ((double *)(malloc(_size_st1 * sizeof(double ))));
  u_new_s = ((double *)(malloc(_size_st2 * sizeof(double ))));
  nx = __VERIFIER_nondet_int();
  k = __VERIFIER_nondet_double();
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u[_f_ct0] = __VERIFIER_nondet_double();
  }
// (First) sequential code segment
  for (int i = 1; i < nx - 1; i++) 
    u_new_s[i] = u[i] + k * (u[i + 1] + u[i - 1] - ((double )2) * u[i]);
  
#pragma omp parallel for
  for (int i = 1; i < nx - 1; i++) 
    u_new[i] = u[i] + k * (u[i + 1] + u[i - 1] - ((double )2) * u[i]);
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    equal = equal && u_new_s[_f_ct0] == u_new[_f_ct0];
  }
  assert(equal);
{
    free(u_new_s);
    free(u);
    free(u_new);
  }
  return 0;
}
