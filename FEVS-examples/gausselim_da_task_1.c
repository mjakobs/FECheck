#include <assert.h> 
#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int _size_st0;

int main()
{
  double tmp;
  int j;
  int pivotRow;
  int top;
  int numCols;
  double *matrix;
  _size_st0 = __VERIFIER_nondet_int();
  matrix = ((double *)(malloc(_size_st0 * sizeof(double ))));
  double tmp_s;
  int j_s;
  double *matrix_s;
  matrix_s = ((double *)(malloc(_size_st0 * sizeof(double ))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    matrix_s[_f_ct0] = __VERIFIER_nondet_double();
    matrix[_f_ct0] = matrix_s[_f_ct0];
  }
  numCols = __VERIFIER_nondet_int();
  top = __VERIFIER_nondet_int();
  pivotRow = __VERIFIER_nondet_int();
  tmp_s = __VERIFIER_nondet_double();
  tmp = tmp_s;
// (First) sequential code segment
  for (j_s = 0; j_s < numCols; j_s++) {
    tmp_s = matrix_s[top * numCols + j_s];
    matrix_s[top * numCols + j_s] = matrix_s[pivotRow * numCols + j_s];
    matrix_s[pivotRow * numCols + j_s] = tmp_s;
  }
  
#pragma omp parallel for private(tmp)
  for (j = 0; j < numCols; j++) {
    tmp = matrix[top * numCols + j];
    matrix[top * numCols + j] = matrix[pivotRow * numCols + j];
    matrix[pivotRow * numCols + j] = tmp;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    equal = equal && matrix_s[_f_ct0] == matrix[_f_ct0];
  }
  assert(equal);
{
    free(matrix_s);
    free(matrix);
  }
  return 0;
}
