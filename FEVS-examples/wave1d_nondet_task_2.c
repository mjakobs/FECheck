#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <math.h> 
extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();
double __VERIFIER_nondet_double_s(int __PEC_turn);
double *u_prev;
int _size_st3;
int nx;
double *u_prev_s;

int main()
{
  int i;
  _size_st3 = __VERIFIER_nondet_int();
  u_prev = ((double *)(malloc(_size_st3 * sizeof(double ))));
  int i_s;
  u_prev_s = ((double *)(malloc(_size_st3 * sizeof(double ))));
  nx = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < _size_st3; _f_ct0++) {
    u_prev_s[_f_ct0] = __VERIFIER_nondet_double();
    u_prev[_f_ct0] = u_prev_s[_f_ct0];
  }
// (First) sequential code segment
  for (i_s = 0; i_s < nx; i_s++) 
    u_prev_s[1 + i_s] = __VERIFIER_nondet_double_s(0);
  
#pragma omp parallel for
  for (i = 0; i < nx; i++) 
    u_prev[1 + i] = __VERIFIER_nondet_double_s(1);
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st3; _f_ct0++) {
    equal = equal && u_prev_s[_f_ct0] == u_prev[_f_ct0];
  }
  assert(equal);
{
    free(u_prev_s);
    free(u_prev);
  }
  return 0;
}

double __VERIFIER_nondet_double_s(int __PEC_turn)
{
  static unsigned int indexS = 0U;
  static unsigned int indexP = 0U;
  static double arr[4294967295U];
  double res;
  if (__PEC_turn == 0) {
    res = __VERIFIER_nondet_double();
    if (indexS != 4294967295U) {
      arr[indexS] = res;
      indexS++;
    }
  }
   else {
    if (indexP < indexS) {
      res = arr[indexP];
      indexP++;
    }
     else {
      res = __VERIFIER_nondet_double();
    }
  }
  return res;
}
