#include <assert.h> 
#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int _size_st1;

int main()
{
  int j;
  double pivot;
  int col;
  int top;
  int numCols;
  double *matrix;
  _size_st1 = __VERIFIER_nondet_int();
  matrix = ((double *)(malloc(_size_st1 * sizeof(double ))));
  int j_s;
  double *matrix_s;
  matrix_s = ((double *)(malloc(_size_st1 * sizeof(double ))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    matrix_s[_f_ct0] = __VERIFIER_nondet_double();
    matrix[_f_ct0] = matrix_s[_f_ct0];
  }
  numCols = __VERIFIER_nondet_int();
  top = __VERIFIER_nondet_int();
  col = __VERIFIER_nondet_int();
  pivot = __VERIFIER_nondet_double();
  j_s = __VERIFIER_nondet_int();
  j = j_s;
// (First) sequential code segment
  for (j_s = col; j_s < numCols; j_s++) {
    matrix_s[top * numCols + j_s] /= pivot;
  }
  
#pragma omp parallel for
  for (j = col; j < numCols; j++) {
    matrix[top * numCols + j] /= pivot;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    equal = equal && matrix_s[_f_ct0] == matrix[_f_ct0];
  }
  assert(equal);
{
    free(matrix_s);
    free(matrix);
  }
  return 0;
}
