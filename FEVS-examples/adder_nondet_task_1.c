#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern int __VERIFIER_nondet_int();
int _size_st0_1;

int main()
{
  int n;
  n = __VERIFIER_nondet_int();
  int a[n];
  _size_st0_1 = n;
  int i;
  int result;
  int i_s;
  int result_s;
  result_s = __VERIFIER_nondet_int();
  result = result_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    a[_f_ct0] = __VERIFIER_nondet_int();
  }
// (First) sequential code segment
  for (i_s = 0; i_s < n; i_s++) 
    result_s += a[i_s];
  
#pragma omp parallel for reduction(+: result)
  for (i = 0; i < n; i++) 
    result += a[i];
// Start equality check
  int equal;
  equal = 1;
  equal = equal && result_s == result;
  assert(equal);
  return 0;
}
