#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <math.h> 
extern double __VERIFIER_nondet_double();
double __VERIFIER_nondet_double_s(int __PEC_turn);
extern int __VERIFIER_nondet_int();
int __VERIFIER_nondet_int_s(int __PEC_turn);
void update();
void write_frame(int time);
void initialization();
void update_s();
void write_frame_s(int time);
void initialization_s();
double k;
double *u_next;
int _size_st2;
double *u_curr;
int _size_st1;
double *u_prev;
int _size_st0;
double c;
int wstep;
int nsteps;
int nx;
int signgam;
double k_s;
double *u_next_s;
double *u_curr_s;
double *u_prev_s;
double c_s;
int wstep_s;
int nsteps_s;
int nx_s;
int signgam_s;

int main()
{
  _size_st2 = __VERIFIER_nondet_int();
  u_next = ((double *)(malloc(_size_st2 * sizeof(double ))));
  _size_st1 = __VERIFIER_nondet_int();
  u_curr = ((double *)(malloc(_size_st1 * sizeof(double ))));
  _size_st0 = __VERIFIER_nondet_int();
  u_prev = ((double *)(malloc(_size_st0 * sizeof(double ))));
  int iter;
  u_next_s = ((double *)(malloc(_size_st2 * sizeof(double ))));
  u_curr_s = ((double *)(malloc(_size_st1 * sizeof(double ))));
  u_prev_s = ((double *)(malloc(_size_st0 * sizeof(double ))));
  int iter_s;
  signgam_s = __VERIFIER_nondet_int();
  signgam = signgam_s;
  nx_s = __VERIFIER_nondet_int();
  nx = nx_s;
  nsteps_s = __VERIFIER_nondet_int();
  nsteps = nsteps_s;
  wstep_s = __VERIFIER_nondet_int();
  wstep = wstep_s;
  c_s = __VERIFIER_nondet_double();
  c = c_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    u_prev_s[_f_ct0] = __VERIFIER_nondet_double();
    u_prev[_f_ct0] = u_prev_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u_curr_s[_f_ct0] = __VERIFIER_nondet_double();
    u_curr[_f_ct0] = u_curr_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    u_next_s[_f_ct0] = __VERIFIER_nondet_double();
    u_next[_f_ct0] = u_next_s[_f_ct0];
  }
  k_s = __VERIFIER_nondet_double();
  k = k_s;
// (First) sequential code segment
  initialization_s();
  for (iter_s = 0; iter_s < nsteps_s; iter_s++) {
    if (iter_s % wstep_s == 0) 
      write_frame_s(iter_s);
    update_s();
  }
  initialization();
  for (iter = 0; iter < nsteps; iter++) {
    if (iter % wstep == 0) 
      write_frame(iter);
    update();
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && signgam_s == signgam;
  assert(equal);
  equal = 1;
  equal = equal && nx_s == nx;
  assert(equal);
  equal = 1;
  equal = equal && nsteps_s == nsteps;
  assert(equal);
  equal = 1;
  equal = equal && wstep_s == wstep;
  assert(equal);
  equal = 1;
  equal = equal && c_s == c;
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    equal = equal && u_prev_s[_f_ct0] == u_prev[_f_ct0];
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    equal = equal && u_curr_s[_f_ct0] == u_curr[_f_ct0];
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    equal = equal && u_next_s[_f_ct0] == u_next[_f_ct0];
  }
  assert(equal);
  equal = 1;
  equal = equal && k_s == k;
  assert(equal);
{
    free(u_prev_s);
    free(u_curr_s);
    free(u_next_s);
    free(u_prev);
    free(u_curr);
    free(u_next);
  }
  return 0;
}

void initialization_s()
{
{
    int i;
    nx_s = __VERIFIER_nondet_int_s(0);
    if (!(nx_s > 0)) 
      exit(0);
    c_s = __VERIFIER_nondet_double_s(0);
    if (!(c_s > 0.0)) 
      exit(0);
    nsteps_s = __VERIFIER_nondet_int_s(0);
    if (!(nsteps_s > 0)) 
      exit(0);
    wstep_s = __VERIFIER_nondet_int_s(0);
    if (!(wstep_s > 0 && wstep_s <= nsteps_s)) 
      exit(0);
    k_s = c_s * c_s;
    printf("Wave1d with nx=%d, c=%f, nsteps=%d, wstep=%d\n",nx_s,c_s,nsteps_s,wstep_s);
    u_prev_s = ((double *)(malloc(((unsigned long )(nx_s + 2)) * sizeof(double ))));
    if (!u_prev_s) 
      exit(0);
    u_curr_s = ((double *)(malloc(((unsigned long )(nx_s + 2)) * sizeof(double ))));
    if (!u_curr_s) 
      exit(0);
    u_next_s = ((double *)(malloc(((unsigned long )(nx_s + 2)) * sizeof(double ))));
    if (!u_next_s) 
      exit(0);
// sets constant boundaries
    u_prev_s[0] = ((double )0);
    u_curr_s[0] = ((double )0);
    u_next_s[0] = ((double )0);
    u_prev_s[nx_s + 1] = ((double )0);
    u_curr_s[nx_s + 1] = ((double )0);
    u_next_s[nx_s + 1] = ((double )0);
// reads initial data from input file
    for (i = 0; i < nx_s; i++) 
      u_prev_s[1 + i] = __VERIFIER_nondet_double_s(0);
    memcpy((void *)(&u_curr_s[1]),(const void *)(&u_prev_s[1]),((unsigned long )nx_s) * sizeof(double ));
  }
}

void write_frame_s(int time)
{
{
    char outfilename[50];
    FILE *outfile;
    int i;
    sprintf(outfilename,"./seqout/out_%d",time);
    outfile = fopen((const char *)outfilename,"w");
    if (!outfile) 
      exit(0);
    for (i = 0; i < nx_s; i++) 
      fprintf(outfile,"%8.2lf",u_curr_s[1 + i]);
    fprintf(outfile,"\n");
    fclose(outfile);
  }
}

void update_s()
{
{
    int i;
    double *tmp;
    for (i = 1; i < nx_s + 1; i++) {
      u_next_s[i] = 2.0 * u_curr_s[i] - u_prev_s[i] + k_s * (u_curr_s[i + 1] + u_curr_s[i - 1] - 2.0 * u_curr_s[i]);
    }
//cycle pointers:
    tmp = u_prev_s;
    u_prev_s = u_curr_s;
    u_curr_s = u_next_s;
    u_next_s = tmp;
  }
}

void initialization()
{
{
    int i;
    nx = __VERIFIER_nondet_int_s(1);
    if (!(nx > 0)) 
      exit(0);
    c = __VERIFIER_nondet_double_s(1);
    if (!(c > 0.0)) 
      exit(0);
    nsteps = __VERIFIER_nondet_int_s(1);
    if (!(nsteps > 0)) 
      exit(0);
    wstep = __VERIFIER_nondet_int_s(1);
    if (!(wstep > 0 && wstep <= nsteps)) 
      exit(0);
    k = c * c;
    printf("Wave1d with nx=%d, c=%f, nsteps=%d, wstep=%d\n",nx,c,nsteps,wstep);
    u_prev = ((double *)(malloc(((unsigned long )(nx + 2)) * sizeof(double ))));
    if (!u_prev) 
      exit(0);
    u_curr = ((double *)(malloc(((unsigned long )(nx + 2)) * sizeof(double ))));
    if (!u_curr) 
      exit(0);
    u_next = ((double *)(malloc(((unsigned long )(nx + 2)) * sizeof(double ))));
    if (!u_next) 
      exit(0);
    u_prev[0] = ((double )0);
    u_curr[0] = ((double )0);
    u_next[0] = ((double )0);
    u_prev[nx + 1] = ((double )0);
    u_curr[nx + 1] = ((double )0);
    u_next[nx + 1] = ((double )0);
    
#pragma omp parallel for
    for (i = 0; i < nx; i++) 
      u_prev[1 + i] = __VERIFIER_nondet_double_s(1);
    memcpy((void *)(&u_curr[1]),(const void *)(&u_prev[1]),((unsigned long )nx) * sizeof(double ));
  }
}

void write_frame(int time)
{
{
    char outfilename[50];
    FILE *outfile;
    int i;
    sprintf(outfilename,"./seqout/out_%d",time);
    outfile = fopen((const char *)outfilename,"w");
    if (!outfile) 
      exit(0);
    for (i = 0; i < nx; i++) 
      fprintf(outfile,"%8.2lf",u_curr[1 + i]);
    fprintf(outfile,"\n");
    fclose(outfile);
  }
}

void update()
{
{
    int i;
    double *tmp;
    
#pragma omp parallel for
    for (i = 1; i < nx + 1; i++) {
      u_next[i] = 2.0 * u_curr[i] - u_prev[i] + k * (u_curr[i + 1] + u_curr[i - 1] - 2.0 * u_curr[i]);
    }
    tmp = u_prev;
    u_prev = u_curr;
    u_curr = u_next;
    u_next = tmp;
  }
}

int __VERIFIER_nondet_int_s(int __PEC_turn)
{
  static unsigned int indexS = 0U;
  static unsigned int indexP = 0U;
  static int arr[4294967295U];
  int res;
  if (__PEC_turn == 0) {
    res = __VERIFIER_nondet_int();
    if (indexS != 4294967295U) {
      arr[indexS] = res;
      indexS++;
    }
  }
   else {
    if (indexP < indexS) {
      res = arr[indexP];
      indexP++;
    }
     else {
      res = __VERIFIER_nondet_int();
    }
  }
  return res;
}

double __VERIFIER_nondet_double_s(int __PEC_turn)
{
  static unsigned int indexS = 0U;
  static unsigned int indexP = 0U;
  static double arr[4294967295U];
  double res;
  if (__PEC_turn == 0) {
    res = __VERIFIER_nondet_double();
    if (indexS != 4294967295U) {
      arr[indexS] = res;
      indexS++;
    }
  }
   else {
    if (indexP < indexS) {
      res = arr[indexP];
      indexP++;
    }
     else {
      res = __VERIFIER_nondet_double();
    }
  }
  return res;
}
