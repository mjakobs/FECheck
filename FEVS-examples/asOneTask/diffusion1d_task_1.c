#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
void update();
int initialize();
void update_s();
int initialize_s();
int time;
double *u_new;
int _size_st1;
double *u;
int _size_st0;
int wstep;
int nsteps;
double k;
int nx;
int time_s;
double *u_new_s;
double *u_s;
int wstep_s;
int nsteps_s;
double k_s;
int nx_s;

int main()
{
  _size_st1 = __VERIFIER_nondet_int();
  u_new = ((double *)(malloc(_size_st1 * sizeof(double ))));
  _size_st0 = __VERIFIER_nondet_int();
  u = ((double *)(malloc(_size_st0 * sizeof(double ))));
  u_new_s = ((double *)(malloc(_size_st1 * sizeof(double ))));
  u_s = ((double *)(malloc(_size_st0 * sizeof(double ))));
  nx_s = __VERIFIER_nondet_int();
  nx = nx_s;
  k_s = __VERIFIER_nondet_double();
  k = k_s;
  nsteps_s = __VERIFIER_nondet_int();
  nsteps = nsteps_s;
  wstep_s = __VERIFIER_nondet_int();
  wstep = wstep_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    u_s[_f_ct0] = __VERIFIER_nondet_double();
    u[_f_ct0] = u_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u_new_s[_f_ct0] = __VERIFIER_nondet_double();
    u_new[_f_ct0] = u_new_s[_f_ct0];
  }
  time_s = __VERIFIER_nondet_int();
  time = time_s;
// (First) sequential code segment
  initialize_s();
  for (time_s = 1; time_s <= nsteps_s; time_s++) {
    update_s();
  }
  initialize();
  for (time = 1; time <= nsteps; time++) {
    update();
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && nx_s == nx;
  assert(equal);
  equal = 1;
  equal = equal && k_s == k;
  assert(equal);
  equal = 1;
  equal = equal && nsteps_s == nsteps;
  assert(equal);
  equal = 1;
  equal = equal && wstep_s == wstep;
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    equal = equal && u_s[_f_ct0] == u[_f_ct0];
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    equal = equal && u_new_s[_f_ct0] == u_new[_f_ct0];
  }
  assert(equal);
  equal = 1;
  equal = equal && time_s == time;
  assert(equal);
{
    free(u_s);
    free(u_new_s);
    free(u);
    free(u_new);
  }
  return 0;
}

int initialize_s()
{
{
    nx_s = 10;
    k_s = 0.3;
    nsteps_s = 150;
    wstep_s = 10;
    u_s = ((double *)(malloc(((unsigned long )nx_s) * sizeof(double ))));
    if (!u_s) 
      return - 1;
    u_new_s = ((double *)(malloc(((unsigned long )nx_s) * sizeof(double ))));
    if (!u_new_s) 
      return - 1;
    for (int i = 1; i < nx_s - 1; i++) 
      u_s[i] = 100.0;
    u_s[0] = u_new_s[0] = 0.0;
    u_s[nx_s - 1] = u_new_s[nx_s - 1] = 0.0;
    return 0;
  }
}

void update_s()
{
{
    for (int i = 1; i < nx_s - 1; i++) 
      u_new_s[i] = u_s[i] + k_s * (u_s[i + 1] + u_s[i - 1] - ((double )2) * u_s[i]);
    double *tmp = u_new_s;
    u_new_s = u_s;
    u_s = tmp;
  }
}

int initialize()
{
{
    nx = 10;
    k = 0.3;
    nsteps = 150;
    wstep = 10;
    u = ((double *)(malloc(((unsigned long )nx) * sizeof(double ))));
    if (!u) 
      return - 1;
    u_new = ((double *)(malloc(((unsigned long )nx) * sizeof(double ))));
    if (!u_new) 
      return - 1;
    
#pragma omp parallel for
    for (int i = 1; i < nx - 1; i++) 
      u[i] = 100.0;
    u[0] = u_new[0] = 0.0;
    u[nx - 1] = u_new[nx - 1] = 0.0;
    return 0;
  }
}

void update()
{
{
    
#pragma omp parallel for
    for (int i = 1; i < nx - 1; i++) 
      u_new[i] = u[i] + k * (u[i + 1] + u[i - 1] - ((double )2) * u[i]);
    double *tmp = u_new;
    u_new = u;
    u = tmp;
  }
}
