// OpenMP parallelization of laplace_seq.c

/* FEVS: A Functional Equivalence Verification Suite for High-Performance
 * Scientific Computing
 *
 * Copyright (C) 2010, Stephen F. Siegel, Timothy K. Zirkel,
 * University of Delaware
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include <stdlib.h>
#include <stdio.h>
#define SQUARE(x) ((x)*(x))

#define nx   10    
#define ny   10    
#define EPSILON 0.000001  

double u1[ny][nx];
double u2[ny][nx];
double (*old)[nx] = u1;
double (*new)[nx] = u2;

void initdata();
double jacobi();

int main(int argc,char *argv[]) {
#pragma scope_1
  initdata();
  double error = jacobi(EPSILON);
 #pragma epocs_1
  printf("%f", error);
  return 0;
}

double jacobi(double epsilon) {
  double error = epsilon;
  int row, col, time;
  double (*tmp)[nx];

  time = 0;
  while (error >= epsilon) {
    error=0.0;
    
    #pragma omp parallel for private(col) reduction(+: error)
    for (row = 1; row < ny-1; row++) {
      for (col = 1; col < nx-1; col++) {
	new[row][col] = (old[row-1][col]+old[row+1][col]+
			 old[row][col-1]+old[row][col+1])*0.25;
	error += SQUARE(old[row][col] - new[row][col]);
      }
    }
   
    time++;
    tmp = new; new = old; old = tmp;
  }
  return error;
}

void initdata() {
  int row, col;


  #pragma omp parallel for
  for (row = 1; row < ny-1; row++)
    #pragma omp parallel for
    for (col = 1; col < nx-1; col++)
      new[row][col] = old[row][col] = 0;

  #pragma omp parallel for
  for (col = 0; col < nx; col++) {
    new[0][col] = old[0][col] = 10;
    new[ny-1][col] = old[ny-1][col] = 10;
  }

}
