// OpenMP parallelization of adder_e_seq.c
/* FEVS: A Functional Equivalence Verification Suite for High-Performance
 * Scientific Computing
 *
 * Copyright (C) 2010, Stephen F. Siegel, Timothy K. Zirkel,
 * University of Delaware
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */
#include <stdio.h>

#define N 100000
 
int main () {
  #pragma scope_1
  int i;
  int sum = 0;
  int array[N];

  #pragma omp parallel for
  for (i = 0; i < N; i++)
    array[i] = i + 1;
  #pragma omp parallel for
  for (i = 0; i < N; i++)
    sum += array[i];
  #pragma epocs_1
  printf("%d\n", sum);
  return 0;
}
