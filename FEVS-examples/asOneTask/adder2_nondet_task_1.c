#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern int __VERIFIER_nondet_int();
int __VERIFIER_nondet_int_s(int __PEC_turn);

int main()
{
  int n;
  n = __VERIFIER_nondet_int();
// (First) sequential code segment
  int result_s = 0;
  int i_s;
  int a_s[n];
  for (i_s = 0; i_s < n; i_s++) 
    a_s[i_s] = __VERIFIER_nondet_int_s(0);
  for (i_s = 0; i_s < n; i_s++) 
    result_s += a_s[i_s];
  int result = 0;
  int i;
  int a[n];
  for (i = 0; i < n; i++) 
    a[i] = __VERIFIER_nondet_int_s(1);
  
#pragma omp parallel for reduction(+: result)
  for (int j = n - 1; j >= 0; j--) 
    result += a[j];
// Start equality check
  int equal;
  equal = 1;
  equal = equal && result_s == result;
  assert(equal);
  return 0;
}

int __VERIFIER_nondet_int_s(int __PEC_turn)
{
  static unsigned int indexS = 0U;
  static unsigned int indexP = 0U;
  static int arr[4294967295U];
  int res;
  if (__PEC_turn == 0) {
    res = __VERIFIER_nondet_int();
    if (indexS != 4294967295U) {
      arr[indexS] = res;
      indexS++;
    }
  }
   else {
    if (indexP < indexS) {
      res = arr[indexP];
      indexP++;
    }
     else {
      res = __VERIFIER_nondet_int();
    }
  }
  return res;
}
