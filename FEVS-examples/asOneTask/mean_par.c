// OpenMP parallelization of mean_seq.c

/* FEVS: A Functional Equivalence Verification Suite for High-Performance
 * Scientific Computing
 *
 * Copyright (C) 2010, Stephen F. Siegel, Timothy K. Zirkel,
 * University of Delaware
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */
#include<stdio.h>
#include<stdlib.h>

extern int __VERIFIER_nondet_int();

int main(int argc, char *argv[]) {
  int N =  atoi(argv[1]);
  #pragma scope_1
  int i;
  int sum = 0;

  int a[N];

  for(i = 0; i < N; i++)
    a[i] = __VERIFIER_nondet_int();


  #pragma omp parallel for reduction(+: sum)
  for(i = 0; i < N; i++)
    sum += a[i];
  #pragma epocs_1

  printf("%lf", (double)sum/N);
  return 0;
}
