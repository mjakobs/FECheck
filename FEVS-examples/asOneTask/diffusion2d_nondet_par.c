// OpenMP parallelization of diffusion2d_nondet_seq.c

/* FEVS: A Functional Equivalence Verification Suite for High-Performance
 * Scientific Computing
 *
 * Copyright (C) 2009-2010, Stephen F. Siegel, Timothy K. Zirkel,
 * University of Delaware
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */


/* diffusion2d_seq.c: sequential version of 2d diffusion.
 * The length of the side of the square is 1. Initially entire square
 * is 100 degrees, but edges are held at 0 degrees.
 *
 */


/* Constants: the following should be defined at compilation:
 *
 *  NSTEPS = number of time steps
 *   WSTEP = write frame every this many steps
 *      NX = number of points in x direction, including endpoints
 *       K = D*dt/(dx*dx)
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();


int nx = -1;              
int ny = -1;			 
double k = -1;           
int nsteps = -1;          
int wstep = -1;		  
double **u;        


int init() {
  int i, j;

  nx = __VERIFIER_nondet_int();
  ny = __VERIFIER_nondet_int();
  k = __VERIFIER_nondet_double();
  nsteps = __VERIFIER_nondet_int();
  wstep = __VERIFIER_nondet_int();

  if (nx<1 || ny<1 || k<=0 || k>=0.5 || nsteps<1 || wstep<1 || wstep>nsteps)
    return -1;

  u = (double **)malloc(sizeof(double*) * ny);
  for(i=0; i < ny; i++)
    u[i] = (double *)malloc(sizeof(double) * nx);

  # pragma omp parallel for
  for (i=0; i < ny; i++){
    # pragma omp parallel for
    for (j=0; j < nx; j++)
      u[i][j] = __VERIFIER_nondet_double();
  }
  return 0;
}


/* updates u for next time step. */
void update(int time) {
  int i, j;
  int sizex = nx, sizey = ny;
  double u_new[sizey][sizex];

  #pragma omp parallel for
  for (i=1; i<ny-1; i++)
    #pragma omp parallel for
    for (j=1; j<nx-1; j++)
      u_new[i][j] = u[i][j] + k * (u[i+1][j] + u[i-1][j] + u[i][j+1] + u[i][j-1]
				   - 4 * u[i][j]);
  for (i=1; i<ny-1; i++)
    memcpy(&u[i][1], &u_new[i][1], sizeof(double) * (nx-2));
}

int main(int argc,char *argv[]) {
  #pragma scope_1
  int iter;
  int notSuccess = 0;

  if(!init())
   notSuccess = -1;
  else
  {

    for (iter = 1; iter <= nsteps; iter++) {
      update(iter);
    }
  }
  #pragma epocs_1
  if(notSuccess)
    return notSuccess;
  for(int i=0; i<ny; i++)
    free(u[i]);
  free(u);
  return 0;
}

