#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();
double __VERIFIER_nondet_double_s(int __PEC_turn);
int _size_st2_2;
int _size_st2_1;
int _size_st1_2;
int _size_st1_1;
int _size_st0_2;
int _size_st0_1;

int main()
{
  int L;
  int M;
  int N;
  N = __VERIFIER_nondet_int();
  M = __VERIFIER_nondet_int();
  L = __VERIFIER_nondet_int();
  double C[L][N];
  _size_st2_2 = N;
  _size_st2_1 = L;
  double B[M][N];
  _size_st1_2 = N;
  _size_st1_1 = M;
  double A[L][M];
  _size_st0_2 = M;
  _size_st0_1 = L;
  double C_s[L][N];
  double B_s[M][N];
  double A_s[L][M];
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st0_2; _f_ct1++) {
      A_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      A[_f_ct0][_f_ct1] = A_s[_f_ct0][_f_ct1];
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st1_2; _f_ct1++) {
      B_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      B[_f_ct0][_f_ct1] = B_s[_f_ct0][_f_ct1];
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st2_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st2_2; _f_ct1++) {
      C_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      C[_f_ct0][_f_ct1] = C_s[_f_ct0][_f_ct1];
    }
  }
// (First) sequential code segment
  int i_s;
  int j_s;
  int k_s;
  for (i_s = 0; i_s < L; i_s++) 
    for (j_s = 0; j_s < M; j_s++) 
      A_s[i_s][j_s] = __VERIFIER_nondet_double_s(0);
  for (i_s = 0; i_s < M; i_s++) 
    for (j_s = 0; j_s < N; j_s++) 
      B_s[i_s][j_s] = __VERIFIER_nondet_double_s(0);
  for (i_s = 0; i_s < L; i_s++) 
    for (j_s = 0; j_s < N; j_s++) {
      C_s[i_s][j_s] = 0.0;
      for (k_s = 0; k_s < M; k_s++) 
        C_s[i_s][j_s] += A_s[i_s][k_s] * B_s[k_s][j_s];
    }
  int i;
  int j;
  int k;
  for (i = 0; i < L; i++) 
    for (j = 0; j < M; j++) 
      A[i][j] = __VERIFIER_nondet_double_s(1);
  for (i = 0; i < M; i++) 
    for (j = 0; j < N; j++) 
      B[i][j] = __VERIFIER_nondet_double_s(1);
  
#pragma omp parallel for
  for (i = 0; i < L; i++) {
    
#pragma omp parallel for
    for (j = 0; j < N; j++) {
      double x = 0.0;
      
#pragma omp parallel for reduction(+:x)
      for (k = 0; k < M; k++) 
        x += A[i][k] * B[k][j];
      C[i][j] = x;
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st0_2; _f_ct1++) {
      equal = equal && A_s[_f_ct0][_f_ct1] == A[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st1_2; _f_ct1++) {
      equal = equal && B_s[_f_ct0][_f_ct1] == B[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st2_2; _f_ct1++) {
      equal = equal && C_s[_f_ct0][_f_ct1] == C[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}

double __VERIFIER_nondet_double_s(int __PEC_turn)
{
  static unsigned int indexS = 0U;
  static unsigned int indexP = 0U;
  static double arr[4294967295U];
  double res;
  if (__PEC_turn == 0) {
    res = __VERIFIER_nondet_double();
    if (indexS != 4294967295U) {
      arr[indexS] = res;
      indexS++;
    }
  }
   else {
    if (indexP < indexS) {
      res = arr[indexP];
      indexP++;
    }
     else {
      res = __VERIFIER_nondet_double();
    }
  }
  return res;
}
