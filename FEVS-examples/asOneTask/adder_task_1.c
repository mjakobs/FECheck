#include <assert.h> 
#include <stdio.h> 

int main()
{
// (First) sequential code segment
  int i_s;
  int sum_s = 0;
  int array_s[100000];
//Initialization of the array
  for (i_s = 0; i_s < 100000; i_s++) 
    array_s[i_s] = i_s + 1;
//Accumulating
  for (i_s = 0; i_s < 100000; i_s++) 
    sum_s += array_s[i_s];
  int i;
  int sum = 0;
  int array[100000];
  
#pragma omp parallel for
  for (i = 0; i < 100000; i++) 
    array[i] = i + 1;
  
#pragma omp parallel for reduction(+: sum)
  for (i = 0; i < 100000; i++) 
    sum += array[i];
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}
