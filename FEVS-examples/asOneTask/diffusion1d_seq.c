// modification of diffusion1d_spec.c from FEVS, added pragma statement to mark relevant code segments for functional equivalence checking

/* FEVS: A Functional Equivalence Verification Suite for High-Performance
 * Scientific Computing
 *
 * Copyright (C) 2009-2010, Stephen F. Siegel, Timothy K. Zirkel,
 * University of Delaware
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */
/* diffusion1d_seq.c: sequential version of 1d diffusion. The length
 * of the rod is 1. The endpoints are frozen at the input temperature.
 */

#include <stdlib.h>


/* Global variables */
int nx;        /* number of points in x direction, including endpoints */
double k;      /* D*dt/(dx*dx) */
int nsteps;    /* number of time steps */
int wstep;     /* write frame every this many steps */
double *u;     /* temperature function, local */
double *u_new; /* second copy of temperature function, local */
int time;      /* current time step */

/* Initialize parameters with some constant values */
int initialize() {
  nx = 10;
  k = 0.3;
  nsteps = 150;
  wstep = 10;
  u = (double*)malloc((nx)*sizeof(double));
  if(!u)
    return -1;
  u_new = (double*)malloc((nx)*sizeof(double));
  if(!u_new)
    return -1;

  for (int i=1; i<nx-1; i++)
    u[i]=100.0;

  u[0] = u_new[0] = 0.0;
  u[nx-1] = u_new[nx-1] = 0.0;
  return 0;
}


/* Updates u_new using u, then swaps u and u_new.
 * Reads the ghost cells in u.  Purely local operation. */
void update() {
  for (int i = 1; i < nx-1; i++)
    u_new[i] = u[i] + k*(u[i+1] + u[i-1] - 2*u[i]);
  double * tmp = u_new; u_new=u; u=tmp;
}

/* main: executes simulation, creates one output file for each time
 * step */
int main(int argc, char *argv[]) {
  #pragma scope_1
  initialize();
  for (time = 1; time <= nsteps; time++) {
    update();
  }
  #pragma epocs_1
  free(u);
  free(u_new);
  return 0;
}
