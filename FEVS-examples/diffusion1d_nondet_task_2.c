#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int nx = - 1;
int _size_st2_1;
double *u;
int _size_st1;
double k;

int main()
{
  nx = __VERIFIER_nondet_int();
  double u_new[nx];
  _size_st2_1 = nx;
  int i;
  _size_st1 = __VERIFIER_nondet_int();
  u = ((double *)(malloc(_size_st1 * sizeof(double ))));
  double u_new_s[nx];
  int i_s;
  k = __VERIFIER_nondet_double();
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u[_f_ct0] = __VERIFIER_nondet_double();
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st2_1; _f_ct0++) {
    u_new_s[_f_ct0] = __VERIFIER_nondet_double();
    u_new[_f_ct0] = u_new_s[_f_ct0];
  }
// (First) sequential code segment
  for (i_s = 1; i_s < nx - 1; i_s++) 
    u_new_s[i_s] = u[i_s] + k * (u[i_s + 1] + u[i_s - 1] - ((double )2) * u[i_s]);
  
#pragma omp parallel for
  for (i = 1; i < nx - 1; i++) 
    u_new[i] = u[i] + k * (u[i + 1] + u[i - 1] - ((double )2) * u[i]);
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2_1; _f_ct0++) {
    equal = equal && u_new_s[_f_ct0] == u_new[_f_ct0];
  }
  assert(equal);
{
    free(u);
  }
  return 0;
}
