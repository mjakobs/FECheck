// OpenMP parallelization of wave1d_nondet_seq.c
/* FEVS: A Functional Equivalence Verification Suite for High-Performance
 * Scientific Computing
 *
 * Copyright (C) 2009-2010, Stephen F. Siegel, Timothy K. Zirkel,
 * University of Delaware
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();

#define SQR(x) ((x)*(x))


int nx, nsteps, wstep;
double c;
double *u_prev, *u_curr, *u_next;
double k;

void quit() {
  printf("Input file must have format:\n\n");
  printf("nx = <INTEGER>\n");
  printf("c = <DOUBLE>\n");
  printf("nsteps = <INTEGER>\n");
  printf("wstep = <INTEGER>\n");
  printf("<DOUBLE> <DOUBLE> ...\n\n");
  printf("where there are nx doubles at the end.\n");
  fflush(stdout);
  exit(1);
}

void update() {
  int i;
  double *tmp;
  #pragma scope_1
  #pragma omp parallel for
  for (i = 1; i < nx+1; i++){
    u_next[i] = 2.0*u_curr[i] - u_prev[i] +
      k*(u_curr[i+1] + u_curr[i-1] -2.0*u_curr[i]);
  }
  #pragma epocs_1

  tmp = u_prev;
  u_prev = u_curr;
  u_curr = u_next;
  u_next = tmp;
}


void initialization(char * infilename) {
  int i;
  FILE * infile = fopen(infilename, "r");

  assert(infile);
  nx = __VERIFIER_nondet_int();
  assert(nx > 0);
  c = __VERIFIER_nondet_double();
  assert(c > 0.0);
  nsteps = __VERIFIER_nondet_int();
  assert(nsteps > 0);
  wstep = __VERIFIER_nondet_int();
  assert(wstep > 0 && wstep <= nsteps);
  k = c * c;
  printf("Wave1d with nx=%d, c=%f, nsteps=%d, wstep=%d\n", 
	 nx, c, nsteps, wstep);
  u_prev = (double *)malloc((nx + 2) * sizeof(double));
  assert(u_prev);
  u_curr = (double *)malloc((nx + 2) * sizeof(double));
  assert(u_curr);
  u_next = (double *)malloc((nx + 2) * sizeof(double));
  assert(u_next);

  u_prev[0] = 0;
  u_curr[0] = 0;
  u_next[0] = 0;
  u_prev[nx + 1] = 0;
  u_curr[nx + 1] = 0;
  u_next[nx + 1] = 0;

  #pragma scope_2
  # pragma omp parallel for
  for(i = 0; i < nx; i++)
    u_prev[1+i] = __VERIFIER_nondet_double(); 
  #pragma epocs_2
  memcpy(&u_curr[1], &u_prev[1], nx * sizeof(double));
  fclose(infile);
}

void write_frame (int time) {
  char outfilename[50];
  FILE *outfile;
  int i;

  sprintf(outfilename, "./seqout/out_%d", time);
  outfile = fopen(outfilename, "w");
  assert(outfile);
  for(i = 0; i < nx; i++) fprintf(outfile, "%8.2lf", u_curr[1+i]);
  fprintf(outfile, "\n");
  fclose(outfile);
}

int main(int argc, char * argv[]) {
  int iter;

  if(argc < 2) {
    printf("Too few arguments, please specify input file:\n");
    quit();
  } else 
    initialization(argv[1]);
  for(iter = 0; iter < nsteps; iter++) {
    if(iter % wstep == 0)
      write_frame(iter);
    update();
  }
  free(u_curr);
  free(u_prev);
  free(u_next);
  return 0;
}
