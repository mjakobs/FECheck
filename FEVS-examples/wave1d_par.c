// OpenMP parallelization of wave1d_seq.c
/* FEVS: A Functional Equivalence Verification Suite for High-Performance
 * Scientific Computing
 *
 * Copyright (C) 2009-2010, Stephen F. Siegel, Timothy K. Zirkel,
 * University of Delaware
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#define SQR(x) ((x)*(x))

int nx, height_init, width_init;
int nsteps, wstep;
double c;

double *u_prev, *u_curr, *u_next;
double k;

void initData() {
  int i;
  double e = exp(1.0);

  #pragma scope_1
  # pragma omp parallel for
  for(i = 1; i < nx+1; i++) {
    if(i == 1 || i >= width_init)
      u_prev[i] = 0.0;
    else
      u_prev[i] = height_init * e *
	exp(-1.0/(1-SQR(2.0*(i-width_init/2.0)/width_init)));
  }
  #pragma epocs_1
}

void update() {
  int i;
  double *tmp;
  
  #pragma scope_2
  # pragma omp parallel for
  for (i = 1; i < nx+1; i++){
    u_next[i] = 2.0*u_curr[i] - u_prev[i] +
      k*(u_curr[i+1] + u_curr[i-1] -2.0*u_curr[i]);
  }
  #pragma epocs_2

  tmp = u_prev;
  u_prev = u_curr;
  u_curr = u_next;
  u_next = tmp;
}

void initialization() {
  int i, j;

  nx = 50;
  c = 0.3;
  height_init = 10;
  width_init = 10;
  nsteps = 500;
  wstep = 5;
  assert(nx >= 2);
  assert(c > 0);
  assert(nsteps >= 1);
  assert(wstep >= 1 && wstep <= nsteps);
  k = c * c;
  printf("Wave1d with nx=%d, c=%f, nsteps=%d, wstep=%d\n", 
	 nx, c, nsteps, wstep);
  u_prev = (double *)malloc((nx + 2) * sizeof(double));
  assert(u_prev);
  u_curr = (double *)malloc((nx + 2) * sizeof(double));
  assert(u_curr);
  u_next = (double *)malloc((nx + 2) * sizeof(double));
  assert(u_next);
  u_prev[0] = 0;
  u_curr[0] = 0;
  u_next[0] = 0;
  u_prev[nx + 1] = 0;
  u_curr[nx + 1] = 0;
  u_next[nx + 1] = 0;
}

void write_frame (int time) {
  printf("\n======= Time %d =======\n", time);
  for(int i=1; i<nx+1; i++)
    printf("%2.2lf ", u_curr[i]);
  printf("\n");
}

int main(int argc, char * argv[]) {
  int iter;

  initialization();
  initData();
  memcpy(&u_curr[1], &u_prev[1], sizeof(double)*(nx-2));
  for(iter = 0; iter < nsteps; iter++) {
    if(iter % wstep == 0)
      write_frame(iter);
    update();
  }
  free(u_curr);
  free(u_prev);
  free(u_next);
  return 0;
}
