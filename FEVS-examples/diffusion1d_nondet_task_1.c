#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();
double __VERIFIER_nondet_double_s(int __PEC_turn);
double *u;
int _size_st0;
int nx;
double *u_s;

int main()
{
  int i;
  _size_st0 = __VERIFIER_nondet_int();
  u = ((double *)(malloc(_size_st0 * sizeof(double ))));
  int i_s;
  u_s = ((double *)(malloc(_size_st0 * sizeof(double ))));
  nx = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    u_s[_f_ct0] = __VERIFIER_nondet_double();
    u[_f_ct0] = u_s[_f_ct0];
  }
// (First) sequential code segment
  for (i_s = 0; i_s < nx; i_s++) {
    u_s[i_s] = __VERIFIER_nondet_double_s(0);
  }
  
#pragma omp parallel for
  for (i = 0; i < nx; i++) {
    u[i] = __VERIFIER_nondet_double_s(1);
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    equal = equal && u_s[_f_ct0] == u[_f_ct0];
  }
  assert(equal);
{
    free(u_s);
    free(u);
  }
  return 0;
}

double __VERIFIER_nondet_double_s(int __PEC_turn)
{
  static unsigned int indexS = 0U;
  static unsigned int indexP = 0U;
  static double arr[4294967295U];
  double res;
  if (__PEC_turn == 0) {
    res = __VERIFIER_nondet_double();
    if (indexS != 4294967295U) {
      arr[indexS] = res;
      indexS++;
    }
  }
   else {
    if (indexP < indexS) {
      res = arr[indexP];
      indexP++;
    }
     else {
      res = __VERIFIER_nondet_double();
    }
  }
  return res;
}
