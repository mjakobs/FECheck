#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int nx = - 1;
int ny = - 1;
int _size_st4_2;
int _size_st4_1;
double **u;
int _size_st3;
int _size_st2;
double k;

int main()
{
  ny = __VERIFIER_nondet_int();
  nx = __VERIFIER_nondet_int();
  double u_new[ny][nx];
  _size_st4_2 = nx;
  _size_st4_1 = ny;
  int j;
  int i;
  _size_st3 = __VERIFIER_nondet_int();
  _size_st2 = __VERIFIER_nondet_int();
  u = ((double **)(malloc(_size_st2 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st3; _f_ct0++) {
    u[_f_ct0] = ((double *)(malloc(_size_st3 * sizeof(double ))));
  }
  double u_new_s[ny][nx];
  int j_s;
  int i_s;
  k = __VERIFIER_nondet_double();
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st3; _f_ct1++) {
      u[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
    }
  }
  j_s = __VERIFIER_nondet_int();
  j = j_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st4_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st4_2; _f_ct1++) {
      u_new_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      u_new[_f_ct0][_f_ct1] = u_new_s[_f_ct0][_f_ct1];
    }
  }
// (First) sequential code segment
  for (i_s = 1; i_s < ny - 1; i_s++) 
    for (j_s = 1; j_s < nx - 1; j_s++) 
      u_new_s[i_s][j_s] = u[i_s][j_s] + k * (u[i_s + 1][j_s] + u[i_s - 1][j_s] + u[i_s][j_s + 1] + u[i_s][j_s - 1] - ((double )4) * u[i_s][j_s]);
  
#pragma omp parallel for
  for (i = 1; i < ny - 1; i++) {
    
#pragma omp parallel for
    for (j = 1; j < nx - 1; j++) 
      u_new[i][j] = u[i][j] + k * (u[i + 1][j] + u[i - 1][j] + u[i][j + 1] + u[i][j - 1] - ((double )4) * u[i][j]);
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st4_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st4_2; _f_ct1++) {
      equal = equal && u_new_s[_f_ct0][_f_ct1] == u_new[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
{
    for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
      free(u[_f_ct0]);
    }
    free(u);
  }
  return 0;
}
