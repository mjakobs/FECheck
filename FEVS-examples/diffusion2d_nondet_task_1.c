#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();
double __VERIFIER_nondet_double_s(int __PEC_turn);
double **u;
int _size_st1;
int _size_st0;
int ny;
int nx;
double **u_s;

int main()
{
  int j;
  int i;
  _size_st1 = __VERIFIER_nondet_int();
  _size_st0 = __VERIFIER_nondet_int();
  u = ((double **)(malloc(_size_st0 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u[_f_ct0] = ((double *)(malloc(_size_st1 * sizeof(double ))));
  }
  int j_s;
  int i_s;
  u_s = ((double **)(malloc(_size_st0 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u_s[_f_ct0] = ((double *)(malloc(_size_st1 * sizeof(double ))));
  }
  nx = __VERIFIER_nondet_int();
  ny = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st1; _f_ct1++) {
      u_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      u[_f_ct0][_f_ct1] = u_s[_f_ct0][_f_ct1];
    }
  }
  j_s = __VERIFIER_nondet_int();
  j = j_s;
// (First) sequential code segment
  for (i_s = 0; i_s < ny; i_s++) {
    for (j_s = 0; j_s < nx; j_s++) 
      u_s[i_s][j_s] = __VERIFIER_nondet_double_s(0);
  }
  
#pragma omp parallel for
  for (i = 0; i < ny; i++) {
    
#pragma omp parallel for
    for (j = 0; j < nx; j++) 
      u[i][j] = __VERIFIER_nondet_double_s(1);
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st1; _f_ct1++) {
      equal = equal && u_s[_f_ct0][_f_ct1] == u[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
{
    for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
      free(u_s[_f_ct0]);
    }
    free(u_s);
    for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
      free(u[_f_ct0]);
    }
    free(u);
  }
  return 0;
}

double __VERIFIER_nondet_double_s(int __PEC_turn)
{
  static unsigned int indexS = 0U;
  static unsigned int indexP = 0U;
  static double arr[4294967295U];
  double res;
  if (__PEC_turn == 0) {
    res = __VERIFIER_nondet_double();
    if (indexS != 4294967295U) {
      arr[indexS] = res;
      indexS++;
    }
  }
   else {
    if (indexP < indexS) {
      res = arr[indexP];
      indexP++;
    }
     else {
      res = __VERIFIER_nondet_double();
    }
  }
  return res;
}
