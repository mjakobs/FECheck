#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int nx = - 1;
int _size_st4_1;
double *u;
int _size_st3;
double *u_s;

int main()
{
  nx = __VERIFIER_nondet_int();
  double u_new[nx];
  _size_st4_1 = nx;
  int i;
  _size_st3 = __VERIFIER_nondet_int();
  u = ((double *)(malloc(_size_st3 * sizeof(double ))));
  int i_s;
  u_s = ((double *)(malloc(_size_st3 * sizeof(double ))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st4_1; _f_ct0++) {
    u_new[_f_ct0] = __VERIFIER_nondet_double();
  }
// (First) sequential code segment
  for (i_s = 1; i_s < nx - 1; i_s++) 
    u_s[i_s] = u_new[i_s];
  
#pragma omp parallel for
  for (i = 1; i < nx - 1; i++) 
    u[i] = u_new[i];
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st3; _f_ct0++) {
    equal = equal && u_s[_f_ct0] == u[_f_ct0];
  }
  assert(equal);
{
    free(u_s);
    free(u);
  }
  return 0;
}
