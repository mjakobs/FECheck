#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int array[100000];
  int sum;
  int i;
  int sum_s;
  int i_s;
  sum_s = __VERIFIER_nondet_int();
  sum = sum_s;
  for (int _f_ct0 = 0; _f_ct0 < 100000; _f_ct0++) {
    array[_f_ct0] = __VERIFIER_nondet_int();
  }
// (First) sequential code segment
  for (i_s = 0; i_s < 100000; i_s++) 
    sum_s += array[i_s];
  
#pragma omp parallel for
  for (i = 0; i < 100000; i++) {
    
#pragma omp atomic
    sum += array[i];
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}
