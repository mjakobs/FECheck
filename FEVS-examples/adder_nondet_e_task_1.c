#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern int __VERIFIER_nondet_int();
int _size_st0_1;

int main()
{
  int n;
  n = __VERIFIER_nondet_int();
  int a[n];
  _size_st0_1 = n;
  int result;
  int i;
  int result_s;
  result_s = __VERIFIER_nondet_int();
  result = result_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    a[_f_ct0] = __VERIFIER_nondet_int();
  }
// (First) sequential code segment
  for (i = 0; i < n; i++) 
    result_s += a[i];
  
#pragma omp parallel for reduction(+: result)
  for (int j = n - 1; j > 0; j--) 
    result += a[j];
// Start equality check
  int equal;
  equal = 1;
  equal = equal && result_s == result;
  assert(equal);
  return 0;
}
