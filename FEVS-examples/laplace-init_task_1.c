#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
double (*new)[10];
int _size_st1;
double (*old)[10];
int _size_st0;
double (*new_s)[10];
double (*old_s)[10];

int main()
{
  int col;
  _size_st1 = __VERIFIER_nondet_int();
  new = ((double (*)[10])(malloc(_size_st1 * sizeof(double [10]))));
  _size_st0 = __VERIFIER_nondet_int();
  old = ((double (*)[10])(malloc(_size_st0 * sizeof(double [10]))));
  int col_s;
  new_s = ((double (*)[10])(malloc(_size_st1 * sizeof(double [10]))));
  old_s = ((double (*)[10])(malloc(_size_st0 * sizeof(double [10]))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      old_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      old[_f_ct0][_f_ct1] = old_s[_f_ct0][_f_ct1];
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      new_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      new[_f_ct0][_f_ct1] = new_s[_f_ct0][_f_ct1];
    }
  }
// (First) sequential code segment
  for (col_s = 0; col_s < 10; col_s++) {
    new_s[0][col_s] = old_s[0][col_s] = ((double )10);
    new_s[10 - 1][col_s] = old_s[10 - 1][col_s] = ((double )10);
  }
  
#pragma omp parallel for
  for (col = 0; col < 10; col++) {
    new[0][col] = old[0][col] = ((double )10);
    new[10 - 1][col] = old[10 - 1][col] = ((double )10);
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      equal = equal && old_s[_f_ct0][_f_ct1] == old[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      equal = equal && new_s[_f_ct0][_f_ct1] == new[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
{
    free(old_s);
    free(new_s);
    free(old);
    free(new);
  }
  return 0;
}
