#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern double __VERIFIER_nondet_double();
extern long __VERIFIER_nondet_long();
extern int __VERIFIER_nondet_int();
double **u_next;
int _size_st7;
int _size_st6;
double **u_curr;
int _size_st5;
int _size_st4;
double k;
long ny;
long nx;
double **u_next_s;

int main()
{
  _size_st7 = __VERIFIER_nondet_int();
  _size_st6 = __VERIFIER_nondet_int();
  u_next = ((double **)(malloc(_size_st6 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st7; _f_ct0++) {
    u_next[_f_ct0] = ((double *)(malloc(_size_st7 * sizeof(double ))));
  }
  _size_st5 = __VERIFIER_nondet_int();
  _size_st4 = __VERIFIER_nondet_int();
  u_curr = ((double **)(malloc(_size_st4 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st5; _f_ct0++) {
    u_curr[_f_ct0] = ((double *)(malloc(_size_st5 * sizeof(double ))));
  }
  u_next_s = ((double **)(malloc(_size_st6 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st7; _f_ct0++) {
    u_next_s[_f_ct0] = ((double *)(malloc(_size_st7 * sizeof(double ))));
  }
  nx = __VERIFIER_nondet_long();
  ny = __VERIFIER_nondet_long();
  k = __VERIFIER_nondet_double();
  for (int _f_ct0 = 0; _f_ct0 < _size_st4; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st5; _f_ct1++) {
      u_curr[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st6; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st7; _f_ct1++) {
      u_next_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      u_next[_f_ct0][_f_ct1] = u_next_s[_f_ct0][_f_ct1];
    }
  }
// (First) sequential code segment
  for (int i = 1; ((long )i) < ny + ((long )1); i++) 
    for (int j = 1; ((long )j) < nx + ((long )1); j++) 
      u_next_s[i][j] = u_curr[i][j] + k * (u_curr[i + 1][j] + u_curr[i - 1][j] + u_curr[i][j + 1] + u_curr[i][j - 1] - ((double )4) * u_curr[i][j]);
  
#pragma omp parallel for
  for (int i = 1; ((long )i) < ny + ((long )1); i++) {
    
#pragma omp parallel for
    for (int j = 1; ((long )j) < nx + ((long )1); j++) 
      u_next[i][j] = u_curr[i][j] + k * (u_curr[i + 1][j] + u_curr[i - 1][j] + u_curr[i][j + 1] + u_curr[i][j - 1] - ((double )4) * u_curr[i][j]);
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st6; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st7; _f_ct1++) {
      equal = equal && u_next_s[_f_ct0][_f_ct1] == u_next[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
{
    for (int _f_ct0 = 0; _f_ct0 < _size_st6; _f_ct0++) {
      free(u_next_s[_f_ct0]);
    }
    free(u_next_s);
    for (int _f_ct0 = 0; _f_ct0 < _size_st4; _f_ct0++) {
      free(u_curr[_f_ct0]);
    }
    free(u_curr);
    for (int _f_ct0 = 0; _f_ct0 < _size_st6; _f_ct0++) {
      free(u_next[_f_ct0]);
    }
    free(u_next);
  }
  return 0;
}
