#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <math.h> 
#include <float.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
double f1(double x);
double f2(double x);
double integrate(double a,double b,double fa,double fb,double area,double tolerance,int choice);
double f1_s(double x);
double f2_s(double x);
double integrate_s(double a,double b,double fa,double fb,double area,double tolerance,int choice);
int numIntervals;
double pi;
double epsilon;
int signgam;
int numIntervals_s;
double pi_s;
double epsilon_s;
int signgam_s;

int main()
{
  int i;
  double width;
  double b1;
  double a1;
  double sum;
  int choice;
  double tolerance;
  double a;
  int i_s;
  double b1_s;
  double a1_s;
  double sum_s;
  signgam_s = __VERIFIER_nondet_int();
  signgam = signgam_s;
  epsilon_s = __VERIFIER_nondet_double();
  epsilon = epsilon_s;
  pi_s = __VERIFIER_nondet_double();
  pi = pi_s;
  numIntervals_s = __VERIFIER_nondet_int();
  numIntervals = numIntervals_s;
  a = __VERIFIER_nondet_double();
  tolerance = __VERIFIER_nondet_double();
  choice = __VERIFIER_nondet_int();
  sum_s = __VERIFIER_nondet_double();
  sum = sum_s;
  a1_s = __VERIFIER_nondet_double();
  a1 = a1_s;
  b1_s = __VERIFIER_nondet_double();
  b1 = b1_s;
  width = __VERIFIER_nondet_double();
// (First) sequential code segment
  for (i_s = 0; i_s < numIntervals_s; i_s++) {
    a1_s = a + ((double )i_s) * width;
    b1_s = a + ((double )(i_s + 1)) * width;
    if (choice) {
      sum_s += integrate_s(a1_s,b1_s,(f2_s(a1_s)),(f2_s(b1_s)),(f2_s(a1_s) + f2_s(b1_s)) * (b1_s - a1_s) / ((double )2),tolerance / ((double )numIntervals_s),choice);
    }
     else {
      sum_s += integrate_s(a1_s,b1_s,(f1_s(a1_s)),(f1_s(b1_s)),(f1_s(a1_s) + f1_s(b1_s)) * (b1_s - a1_s) / ((double )2),tolerance / ((double )numIntervals_s),choice);
    }
  }
  
#pragma omp parallel for reduction(+: sum) private(a1, b1)
  for (i = 0; i < numIntervals; i++) {
    a1 = a + ((double )i) * width;
    b1 = a + ((double )(i + 1)) * width;
    if (choice) {
      sum += integrate(a1,b1,(f2(a1)),(f2(b1)),(f2(a1) + f2(b1)) * (b1 - a1) / ((double )2),tolerance / ((double )numIntervals),choice);
    }
     else {
      sum += integrate(a1,b1,(f1(a1)),(f1(b1)),(f1(a1) + f1(b1)) * (b1 - a1) / ((double )2),tolerance / ((double )numIntervals),choice);
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && signgam_s == signgam;
  assert(equal);
  equal = 1;
  equal = equal && epsilon_s == epsilon;
  assert(equal);
  equal = 1;
  equal = equal && pi_s == pi;
  assert(equal);
  equal = 1;
  equal = equal && numIntervals_s == numIntervals;
  assert(equal);
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}

double integrate_s(double a,double b,double fa,double fb,double area,double tolerance,int choice)
{
{
    double delta = b - a;
    double c = a + delta / ((double )2);
    double fc;
    if (choice) 
      fc = f2_s(c);
     else 
      fc = f1_s(c);
    double leftArea = (fa + fc) * delta / ((double )4);
    double rightArea = (fc + fb) * delta / ((double )4);
    if (tolerance < epsilon_s) {
// printf("Tolerance may not be possible to obtain.\n");
      return leftArea + rightArea;
    }
    if (fabs(leftArea + rightArea - area) <= tolerance) {
      return leftArea + rightArea;
    }
    return integrate_s(a,c,fa,fc,leftArea,tolerance / ((double )2),choice) + integrate_s(c,b,fc,fb,rightArea,tolerance / ((double )2),choice);
  }
}

double f2_s(double x)
{
{
    return cos(x);
  }
}

double f1_s(double x)
{
{
    return sin(x);
  }
}

double integrate(double a,double b,double fa,double fb,double area,double tolerance,int choice)
{
{
    double delta = b - a;
    double c = a + delta / ((double )2);
    double fc;
    if (choice) 
      fc = f2(c);
     else 
      fc = f1(c);
    double leftArea = (fa + fc) * delta / ((double )4);
    double rightArea = (fc + fb) * delta / ((double )4);
    if (tolerance < epsilon) {
// printf("Tolerance may not be possible to obtain.\n");
      return leftArea + rightArea;
    }
    if (fabs(leftArea + rightArea - area) <= tolerance) {
      return leftArea + rightArea;
    }
    return integrate(a,c,fa,fc,leftArea,tolerance / ((double )2),choice) + integrate(c,b,fc,fb,rightArea,tolerance / ((double )2),choice);
  }
}

double f2(double x)
{
{
    return cos(x);
  }
}

double f1(double x)
{
{
    return sin(x);
  }
}
