#include <assert.h> 
#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int _size_st1;

int main()
{
  double tmp;
  int j;
  int i;
  int col;
  int top;
  int numCols;
  int numRows;
  double *matrix;
  _size_st1 = __VERIFIER_nondet_int();
  matrix = ((double *)(malloc(_size_st1 * sizeof(double ))));
  double tmp_s;
  int j_s;
  int i_s;
  double *matrix_s;
  matrix_s = ((double *)(malloc(_size_st1 * sizeof(double ))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    matrix_s[_f_ct0] = __VERIFIER_nondet_double();
    matrix[_f_ct0] = matrix_s[_f_ct0];
  }
  numRows = __VERIFIER_nondet_int();
  numCols = __VERIFIER_nondet_int();
  top = __VERIFIER_nondet_int();
  col = __VERIFIER_nondet_int();
  i_s = __VERIFIER_nondet_int();
  i = i_s;
  j_s = __VERIFIER_nondet_int();
  j = j_s;
  tmp_s = __VERIFIER_nondet_double();
  tmp = tmp_s;
// (First) sequential code segment
  for (i_s = 0; i_s < numRows; i_s++) {
    if (i_s != top) {
      tmp_s = matrix_s[i_s * numCols + col];
      for (j_s = col; j_s < numCols; j_s++) {
        matrix_s[i_s * numCols + j_s] -= matrix_s[top * numCols + j_s] * tmp_s;
      }
    }
  }
  
#pragma omp parallel for
  for (i = 0; i < numRows; i++) {
    if (i != top) {
      tmp = matrix[i * numCols + col];
      
#pragma omp parallel for
      for (j = col; j < numCols; j++) {
        matrix[i * numCols + j] -= matrix[top * numCols + j] * tmp;
      }
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    equal = equal && matrix_s[_f_ct0] == matrix[_f_ct0];
  }
  assert(equal);
{
    free(matrix_s);
    free(matrix);
  }
  return 0;
}
