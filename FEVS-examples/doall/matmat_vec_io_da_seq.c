// modification of matmat_vec_io.c from FEVS, replaced file I/O input by __VERIFER_nondet_int() calls and added pragma statement to mark relevant code segments for functional equivalence checking

/* FEVS: A Functional Equivalence Verification Suite for High-Performance
 * Scientific Computing
 *
 * Copyright (C) 2010, Stephen F. Siegel, Timothy K. Zirkel,
 * University of Delaware
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include <stdlib.h>
#include <stdio.h>

extern double __VERIFIER_nondet_double();

void printMatrix(int numRows, int numCols, double *m) {
  int i, j;
  for (i = 0; i < numRows; i++) {
    for (j = 0; j < numCols; j++)
      printf("%f ", m[i*numCols + j]);
    printf("\n");
  }
  fflush(stdout);
}

int N, L, M;

void vecmat(double vector[L], double matrix[L][M], double result[M]) {
  int j, k;
  for (j = 0; j < M; j++)
  {
    double x = 0.0;
    for (k = 0; k < L; k++)
      x += vector[k]*matrix[k][j];
    result[j] = x;
  }
}

int main(int argc, char *argv[]) {
  int i, j, k;
  N = atoi(argv[1]);
  L = atoi(argv[2]);
  M = atoi(argv[3]);
  double a[N][L], b[L][M], c[N][M];


  for (i = 0; i < L; i++)
    for (j = 0; j < M; j++)
      a[i][j] = __VERIFIER_nondet_double();
  for (i = 0; i < M; i++)
    for (j = 0; j < N; j++)
      b[i][j] = __VERIFIER_nondet_double();

  #pragma scope_1
  for (i = 0; i < N; i++)
    vecmat(a[i], b, c[i]);
  #pragma epocs_1

  printMatrix(N, M, &c[0][0]);

  return 0;
}
