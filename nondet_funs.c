//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h>

//#include <stdbool.h>
/*
bool __VERIFIER_nondet_bool()
{
  bool res;
  int x;
  scanf("%d", &x);
  if(x)
    return true;
  return false;
}*/

char __VERIFIER_nondet_char() 
{
  char res;
  scanf("%hhd", &res);
  return res;
}

int __VERIFIER_nondet_int()
{
  int res;
  scanf("%d", &res);
  if(res > 5 || res < -5)
    return 0;
  return res;
}

float __VERIFIER_nondet_float()
{
  float res;
  scanf("%g", &res);
  if(res > 5.0 || res < -5.0)
    return 0.0;
  return res;
}

double __VERIFIER_nondet_double()
{
  double res;
  scanf("%lf", &res);
  if(res > 5.0 || res < -5.0)
    return 0.0;
  return res;
}

long __VERIFIER_nondet_long()
{
  long res;
  scanf("%ld", &res);
  if(res > 5 || res < -5)
    return 0;
  return res;
}

short __VERIFIER_nondet_short()
{
  short res;
  scanf("%hd", &res);
  if(res > 5 || res < -5)
    return 0;
  return res;
}

unsigned char __VERIFIER_nondet_uchar()
{
  unsigned char res;
  scanf("%hhu", &res);
  if(res > 5 || res < 0)
    return 0;
  return res;
}

unsigned int __VERIFIER_nondet_uint()
{
  unsigned int res;
  scanf("%u", &res);
  if(res > 5 || res < 0)
    return 0;
  return res;
}

unsigned long __VERIFIER_nondet_ulong()
{
  unsigned long res;
  scanf("%lu", &res);
  if(res > 5 || res < 0)
    return 0;
  return res;
}

unsigned short __VERIFIER_nondet_ushort()
{
  unsigned short res;
  scanf("%hu", &res);
  if(res > 5 || res < 0)
    return 0;
  return res;
}
