// added pragma statements to mark beginning and end of code segments that should be considered by functional equivalence check
/*Copyright (c) 2016 Karlsruhe Insitute of Technology

All rights reserved.

Contributors:
  2016 Moritz Kiefer
  2016 Mattias Ulbrich
  2016 Stephan Gocht
  2016 Daniel Lentzsch

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/

int __mark(int input)
{
  return input;
}

int f(int n) {
    #pragma scope_1
    int result = 1;

    int b = 1;
    int retval = -1;

    int mark = __mark(42);
    while (mark && !(b == 0)) {
        if (n < 10) {
            retval = result;
            b = 0;
        } else if (n < 100) {
            retval = result + 1;
            b = 0;
        } else if (n < 1000) {
            retval = result + 2;
            b = 0;
        } else if (n < 10000) {
            retval = result + 3;
            b = 0;
        } else {
            n = n / 10000;
            result = result + 4;
        }
        mark = __mark(42);
    }
    #pragma epocs_1
    return retval;
}
