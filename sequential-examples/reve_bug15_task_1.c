#include <assert.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int x;
  int y;
  int y_s;
  int x_s;
  x_s = __VERIFIER_nondet_int();
  x = x_s;
  y_s = __VERIFIER_nondet_int();
  y = y_s;
// (First) sequential code segment
  while(x_s <= 9){
    y_s = x_s + 2;
    x_s = 2 * y_s;
  }
  while(x < 10){
    y = 2 + x;
    x = y + y;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  return 0;
}
