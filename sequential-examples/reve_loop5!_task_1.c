#include <assert.h> 
extern int __VERIFIER_nondet_int();
extern int __mark(int );

int main()
{
  int j;
  int i;
  int n;
  int j_s;
  int i_s;
  n = __VERIFIER_nondet_int();
// (First) sequential code segment
  i_s = 0;
  j_s = 0;
  while(__mark(42) & i_s < n + n){
    j_s++;
    i_s++;
  }
  i = n + 1;
  j = 0;
  while(__mark(42) & i > 0){
    j = j + 2;
    i = i - 1;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && j_s == j;
  assert(equal);
  return 0;
}
