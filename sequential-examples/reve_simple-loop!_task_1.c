#include <assert.h> 
extern int __mark(int );

int main()
{
// (First) sequential code segment
  int i_s = 0;
  while(__mark(42) & i_s <= 10){
    i_s++;
  }
  int i = 1;
  while(__mark(42) & i <= 10){
    i++;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && i_s == i;
  assert(equal);
  return 0;
}
