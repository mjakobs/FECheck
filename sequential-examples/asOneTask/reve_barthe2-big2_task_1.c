#include <assert.h> 
extern int __VERIFIER_nondet_int();
extern int __mark(int );

int main()
{
  int n;
  n = __VERIFIER_nondet_int();
// (First) sequential code segment
  int i_s = 1;
  int x_s = 1;
  while(__mark(1) & i_s <= n){
    x_s = x_s * 1;
    i_s++;
  }
  i_s = 0;
  int mark_s = __mark(2);
  while(mark_s & i_s <= n){
    x_s = x_s + i_s;
    i_s++;
    mark_s = __mark(2);
  }
  i_s = 1;
  while(__mark(3) & i_s <= n){
    x_s = x_s * 2;
    i_s++;
  }
  int i = 1;
  int x = 1;
  while(__mark(1) & i <= n){
    x = x * 1;
    i++;
  }
  i = 1;
  int mark = __mark(2);
  while(mark & i <= n){
    x = x + i;
    i++;
    mark = __mark(2);
  }
  i = 1;
  while(__mark(3) & i <= n){
    x = x * 2;
    i++;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  return 0;
}
