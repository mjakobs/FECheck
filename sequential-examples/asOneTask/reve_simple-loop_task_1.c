#include <assert.h> 

int main()
{
// (First) sequential code segment
  int i_s = 0;
  while(i_s <= 10){
    i_s++;
  }
  int i = 1;
  while(i <= 10){
    i++;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && i_s == i;
  assert(equal);
  return 0;
}
