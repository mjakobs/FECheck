#include <assert.h> 

int main()
{
// (First) sequential code segment
  int res_s;
  int x_s = 1;
  int y_s;
  while(x_s <= 9){
    y_s = x_s + 2;
    x_s = 2 * y_s;
  }
  res_s = x_s << 1;
  int res;
  int y;
  int x = 1;
  while(x < 10){
    y = 2 + x;
    x = y + y;
  }
  res = x * 2;
// Start equality check
  int equal;
  equal = 1;
  equal = equal && res_s == res;
  assert(equal);
  return 0;
}
