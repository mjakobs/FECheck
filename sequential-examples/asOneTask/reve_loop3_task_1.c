#include <assert.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int n;
  int n_s;
  n_s = __VERIFIER_nondet_int();
  n = n_s;
// (First) sequential code segment
  int i_s = 1;
  if (n_s < 1) {
    n_s = 1;
  }
  int j_s = 0;
  while(i_s <= n_s){
    j_s = j_s + 2;
    i_s++;
  }
  int i = 1;
  if (n < 1) {
    n = 1;
  }
  int j = 2;
  while(i < n){
    j = j + 2;
    i++;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && j_s == j;
  assert(equal);
  return 0;
}
