//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

extern int __VERIFIER_nondet_int();

int main () {
  int sum = 0;
  int N = __VERIFIER_nondet_int();

  #pragma scope_1
  #pragma omp parallel for reduction(+: sum)
  for (int i = 0; i < N; i++)
    sum += i+1;
  #pragma epocs_1
  return sum;
}
