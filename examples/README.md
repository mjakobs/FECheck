# Description
This folder contains our own test programs.

To perform the equivalence checking, we require a sequential and a parallelized version (suffixes _seq.c and _par.c, respectively) that are both annotated with pragmas that indicate the beginning and end of a parallelized code region. Programs with suffix _e_par.c are incorrectly parallelized, while all other programs are correctly parallelized.

The files ending on task_*.c are the program verification task generated by PEQcheck.

The subfolder `reduction` references all examples that applied the reduction pattern or contains variants of our examples that only contain the parts relevant for the reduction pattern.

The subfolder `asOneTask` contains all examples, but uses a single code segment that represents the complete program. For our example, the complete program is identical to the complete function.


# Licensing 
All files are released under the Apache 2.0 license. For more details see [../LICENSE-Apache-2.0.txt](../LICENSE-Apache-2.0.txt).


