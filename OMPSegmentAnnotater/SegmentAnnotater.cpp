//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "rose.h"
#include <string>
#include <assert.h>

int main( int argc, char * argv[] ){
    assert(argc>=3);
   

    // Initialize and check compatibility. See Rose::initialize
    ROSE_INITIALIZE;

    // Build the AST used by ROSE
    SgProject* project = frontend(argc,argv);
    ROSE_ASSERT (project != NULL);

    // Run internal consistency tests on AST
    AstTests::runAllTests(project);

    // Insert your own manipulation of the AST here...
    SgSourceFile* file = isSgSourceFile(project->operator[](0));
    
    std::vector<SgPragmaDeclaration*> ompPragmas;
    
    Rose_STL_Container<SgNode*> pragmaStmts = NodeQuery::querySubTree (file, V_SgPragmaDeclaration);
    for (Rose_STL_Container<SgNode*>::iterator i = pragmaStmts.begin(); i != pragmaStmts.end(); i++)
    {
      if(isSgPragmaDeclaration(*i)->get_pragma()->get_pragma().rfind("omp ", 0) == 0 || isSgPragmaDeclaration(*i)->get_pragma()->get_pragma().rfind("omp\t", 0) == 0)
      {
        ompPragmas.push_back(isSgPragmaDeclaration(*i));
      }
    }

    bool isSegment;
    int index=1;
    for(std::vector<SgPragmaDeclaration*>::iterator itInsert = ompPragmas.begin(); itInsert != ompPragmas.end(); itInsert++)
    {
      isSegment = true;
      
      for(std::vector<SgPragmaDeclaration*>::iterator itAncestorCandidate = ompPragmas.begin(); isSegment && itAncestorCandidate != ompPragmas.end(); itAncestorCandidate++)
      {
        if(*itAncestorCandidate != *itInsert && SageInterface::isAncestor(*itAncestorCandidate, *itInsert))
        {
          isSegment = false;
        }
      }
      
      if(isSegment)
      {
        SageBuilder::pushScopeStack((*itInsert)->get_scope());
        SageInterface::insertStatementBefore(*itInsert, SageBuilder::buildPragmaDeclaration("scope_"+  std::to_string(index)));
        SageInterface::insertStatementAfter(SageInterface::getNextStatement(*itInsert), SageBuilder::buildPragmaDeclaration("epocs_"+  std::to_string(index)));
        SageBuilder::popScopeStack();
        index++;
      }
    }

    file->set_unparse_output_filename(file->getFileName()); 
    // Generate source code from AST, ensure that project okay
    AstTests::runAllTests(project);
    file->unparse();


    delete project;

    return 0; 
}


