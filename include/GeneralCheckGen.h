//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef PEC_GeneralCheckGen_H
#define PEC_GeneralCheckGen_H

#include "AbstractCheckGen.h"
#include "IncludeManager.h"
#include "PrototypeManager.h"
#include "PECHelperManager.h"
#include "TypeManager.h"
#include "VariableClassification.h"
#include "rose.h"
#include <string.h> 

  class GeneralCheckGen : public AbstractCheckGen {
    public:
      GeneralCheckGen(bool requireOutputVars) : AbstractCheckGen(requireOutputVars){}
      void extract_nonshared_vars(std::set<std::string>& non_shared, SgSourceFile* seqProg, SgSourceFile* parProg);
      void generate_checker_programs(SgSourceFile* seqProg, SgSourceFile* parProg, std::string checkFilePrefix);
      void generate_single_checker_program(SgSourceFile* seqProg, SgSourceFile* parProg,  std::set<std::string>& non_shared_vars, std::string fileName, int checkID);


    private:
      unsigned int countIndex = 0;
      PECHelperManager helper;
      
      SgProject* project;
      SgBasicBlock* deallocate_block;
      std::set<std::string> inStdLib {"abs", "malloc", "div"};
      std::set<std::string> inMath {"fabs", "pow", "sqrt"};
      std::set<std::string> inComplex {"cexp", "conj"};

      void add_array_init_vars(SgArrayType* arrType, std::set<SgInitializedName*>& arrInitVars);
      std::map<SgInitializedName*, VariableClassification>* add_code_between_pragmas(std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*>& startEnd, std::vector<std::vector<SgFunctionCallExp*>>& nondetCalls, bool isInSeq);
      void add_const_init_vars(SgInitializer* initializer, std::set<SgInitializedName*>& constInitVars);
      void add_declaration(std::string& name,SgType* type, SgInitializer* initStmt, SgScopeStatement* scope, std::map<std::string, std::string>& pVarNameToSizeVar, std::vector<SgVariableDeclaration*>& sizeVarDeclForTypeUpdates, bool isSeq);
      void add_declarations(std::map<SgInitializedName*, VariableClassification>* varsSeq,  std::map<SgInitializedName*, VariableClassification>* varsPar, std::map<std::string, SgInitializedName*>& reqForArrayDecl, std::map<std::string, std::string>& pVarNameToSizeVar, std::vector<SgVariableDeclaration*>& sizeVarDeclForTypeUpdates);
      void add_declarations_for(std::map<SgInitializedName*, VariableClassification>* vars, std::map<std::string, SgInitializedName*>& reqForArrayDecl, std::map<std::string, std::string>& pVarNameToSizeVar, std::vector<SgVariableDeclaration*>& sizeVarDeclForTypeUpdates, bool isSeq);
      void add_declarations_for_and_init(std::map<std::string, SgInitializedName*>& reqForArrayDecl, PrototypeManager& protoMgr, SgStatement* beforeStmt, std::map<std::string, std::string>& pVarNameToSizeVar, std::vector<SgVariableDeclaration*>& sizeVarDeclForTypeUpdates, std::string seqFileName);
      void allocate_memory_and_save_sizes(std::string& name, SgType* type, std::map<std::string, std::string>& pVarNameToSizeVar, std::vector<SgVariableDeclaration*>& sizeVarDeclForTypeUpdates, bool inSeq);
      bool analyze_function_calls_in(SgStatement* stmt, std::set<SgFunctionDeclaration*>& analyzedFun, std::vector<std::vector<SgFunctionCallExp*>>& nondetCalls, std::map<SgInitializedName*, VariableClassification>& vars, SgSourceFile* file, bool isInSeq, bool nonRecursiveCall, std::set<SgFunctionCallExp*>& seen);
      std::vector<SgInitializedName*>* build_main(SgSourceFile* file, SgSourceFile* seqProg, SgSourceFile* parProg, std::set<std::string>& non_shared_vars, std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*>& startEndSeq, std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*>& startEndPar);
      SgExpression* build_array_subscript(std::string& arrName, SgExpression* subscript);
      std::vector<SgInitializedName*>* build_main_for_id(SgSourceFile* file, SgSourceFile* seqProg, SgSourceFile* parProg, std::set<std::string>& non_shared_vars, int checkID);
      SgStatement* build_nondet_assign(SgExpression* lhs, SgFunctionDeclaration* nondetFun);
      void build_nondet_replace_body(SgBasicBlock* body, SgFunctionDeclaration* nondetFun);
      void copy_function(SgFunctionDeclaration* funDecl, std::map<SgInitializedName*, VariableClassification>& vars, bool isInSeq);
      void deallocate_memory(std::string& name, SgType* type, std::map<std::string, std::string>& pVarNameToSizeVar, bool inSeq);
      SgInitializedName* findVar(SgScopeStatement* funScope, std::string pName);
      void gen_and_add_eq_check(std::vector<std::tuple<std::string, std::string, SgType*>>& varPairsEq, std::map<std::string, std::string>& pVarNameToSizeVar);
      void gen_and_add_var_init(std::vector<std::tuple<std::string, std::string, SgType*>>& varPairs, SgStatement& addBeforeStmt, PrototypeManager& protoMgr, std::map<std::string, SgInitializedName*>& reqForArrayDecl, std::map<std::string, std::string>& pVarNameToSizeVar);
      SgFunctionCallExp* gen_nondet_replace_call(SgFunctionDeclaration* nondetFun, bool isSeq);
      void handle_nondet_calls(std::vector<std::vector<SgFunctionCallExp*>>& nondetCallsSeq, std::vector<std::vector<SgFunctionCallExp*>>& nondetCallsPar, SgGlobal* globalScope);
      void insert_all_global_vars(SgGlobal* global_scope, std::map<SgInitializedName*, VariableClassification>& vars);
      void insert_modified_var(SgInitializedName* decl, std::map<SgInitializedName*, VariableClassification>& vars);
      void insert_vars_modified_internally_before_function_calls_in(std::map<SgInitializedName*, VariableClassification>& vars, std::set<std::pair<SgInitializedName*, SgNode*>>& defsBefore, DefUseAnalysis* defuse, std::vector<SgNode*>& stmtsBetweenPragmas);
      void insert_vars_modified_internally(std::set<std::pair<SgInitializedName*, SgNode*>>& defsBefore, std::vector<std::pair<SgInitializedName*, SgNode*>>& defsAfter, std::map<SgInitializedName*, VariableClassification>& vars, std::vector<SgNode*>& stmtsBetweenPragmas);
      void inspect_types_for_vars_declared_in_segment(std::map<SgInitializedName*, VariableClassification>* varsSeq,  std::map<SgInitializedName*, VariableClassification>* varsPar);
      bool is_known_pure_external(std::string funName);
      bool is_pointer_free(SgType* type, bool inSeq);
      bool is_same_type(SgType* type1, SgType* type2);
      std::vector<SgInitializedName*>* match_vars(std::map<SgInitializedName*, VariableClassification>* varsSeq, std::map<SgInitializedName*, VariableClassification>* varsPar, SgScopeStatement* seqEndScope, SgScopeStatement* parEndScope, std::vector<std::tuple<std::string, std::string, SgType*>>& reqInit, std::vector<std::tuple<std::string, std::string, SgType*>>& reqEqCheck, std::map<std::string, SgInitializedName*>& reqForArrayDeclOrConst, std::map<std::string, std::string>& pVarNameToSizeVar, std::set<std::string>& non_shared_vars);
      void rename_variables(std::vector<SgInitializedName*>* vars_to_rename);
      void rename_variable(SgInitializedName* decl, std::string new_name);
      void store_size_variable(std::string& name, SgType* type, std::map<std::string, std::string>& pVarNameToSizeVar, bool isSeq);
      void replace_function_call_for_seq(SgFunctionCallExp* call, std::set<SgFunctionCallExp*>& seen, std::map<SgInitializedName*, VariableClassification>& vars);
      void undo_renaming(std::vector<SgInitializedName*>* vars_to_rename);
      SgArrayType* update_array_type(SgArrayType* origType, std::string& sizeVar, int id);
      bool use_same_dimensions(SgArrayType* type1, SgArrayType* type2);
      void write_checker_file(SgSourceFile* file, std::string fileName, SgProject* seqProject);


  };
#endif
