//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef PEC_VariableClassification_H
#define PEC_VariableClassification_H

#include "rose.h"

class VariableClassification {
    public:
      SgInitializedName* get_decl();
      bool is_live();
      bool is_used_before();
      bool is_written();
      bool must_compare();
      bool must_declare();
      bool must_initialize();
      bool must_rename();
      void set_decl(SgInitializedName* pDecl);
      void set_live_after();
      void set_modified();
      void set_rename();
      void set_used_before(); 
      void unset_declaration();
      void unset_used_before();
    
    private:
      bool isLiveAfter = false; 
      bool isUsedBefore = false;
      bool isModified = false;
      bool isRename = false;
      bool requiresDecl = true;
      SgInitializedName* decl = NULL;

  };
#endif
