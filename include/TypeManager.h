//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef PEC_TypeManager_H
#define PEC_TypeManager_H

#include <string>
#include "rose.h"
#include "PECHelperManager.h"

class TypeManager {
    public:
      void init_type_Mgr(SgSourceFile* seqProg, SgSourceFile* parProg);
      SgClassDeclaration* getClassDeclaration(std::string className, bool isSeq);
      SgFunctionDeclaration* getFunDeclaration(std::string funName, bool isSeq);
      void addFunDeclaration(std::string funName, SgFunctionDeclaration* funDecl, bool inSeq);
      void insert_types(SgSourceFile* file);
      void inspect_type_for_definition(SgType* pType, bool isSeq);
      void reset_types();

    private:
      PECHelperManager helper;
      std::map<std::string, SgClassDeclaration*> seqClassNameToDecl;
      std::map<std::string, SgClassDeclaration*> parClassNameToDecl;
      std::map<std::string, SgFunctionDeclaration*> seqFunNameToDeclOrig;
      std::map<std::string, SgFunctionDeclaration*> seqFunNameToDeclCopy;
      std::map<std::string, SgFunctionDeclaration*> parFunNameToDecl;
      std::set<std::string> seenTypes;
      std::set<SgDeclarationStatement*> typesToDefine;


      void add_defining_classes(SgGlobal* globalScope, std::map<std::string, SgClassDeclaration*>& classNameToDecl);
      void add_defining_fun(SgGlobal* globalScope, std::map<std::string, SgFunctionDeclaration*>& funNameToDecl);
      SgDeclarationStatement* generate_type_decl(SgDeclarationStatement* typeDecl);

  };
#endif
