//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef PEC_HelperManager_H
#define PEC_HelperManager_H

#include "rose.h"
#include "PECConstants.h"

class PECHelperManager {
    public:
      PECHelperManager();
      SgExprStatement* assign_nondet_val(SgExpression* lhs, NondetType nondet, SgType* type);
      SgStatement* constructForLoop(SgName& loopCounter, SgExpression* upperBound, SgBasicBlock* body, SgType* counterType);
      std::string add_rename_suffix(std::string name);

      SgStatement* free_stmt(std::string& varName, SgType* type);
      std::string get_nondet_fun_name(NondetType nondet);
      const char* get_nondet_fun_suffix(NondetType nondet);
      NondetType get_nondet_type(SgType* type);
      NondetType get_nondet_type(std::string funName);
      SgType* get_type(NondetType nondet);
      SgFunctionCallExp* generate_nondet_call(NondetType nondet, SgType* type);
      bool in_header(SgNode* node);
      bool is_const_type(SgType* type);
      bool is_global(SgScopeStatement* scope);
      SgStatement* malloc_assign(std::string& varName, std::string& varSize, SgType* type);
      std::string remove_rename_suffix(std::string varName);
      bool shouldIgnoreGlobal(std::string globalVarName);

    private:
      const char* nondetFunSuffix[NondetTypeSize] = {"bool", "char", "int", "float", "double", "long", "short", "uchar", "uint", "ulong", "ushort", ""};
      const char* ignored_globals_list[15] = {"_IO_2_1_stdin_", "_IO_2_1_stdout_", "_IO_2_1_stderr_", "stdin", "stdout", "stderr", "sys_nerr", "sys_errlist", "__PRETTY_FUNCTION__", "__environ", "optarg", "opterr", "optind", "optopt",  ""}; // need to end with empty string
      std::set<std::string> ignoredGlobals;

      unsigned int suffixLen = std::string(seqVarSuffix).length();
      const std::string nondetPrefix = std::string(vnondetPrefix);
      unsigned int vNondetPrefixLen = std::string(vnondetPrefix).length();

  };
#endif
