//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef PEC_InitializationGenerator_H
#define PEC_InitializationGenerator_H

#include <string>
#include "IncludeManager.h"
#include "PrototypeManager.h"
#include "PECHelperManager.h"
#include "PECConstants.h"
#include "rose.h"
#include "TypeManager.h"

class InitializationGen {
    public:
      InitializationGen(PrototypeManager* pProtoManager, IncludeManager* pIncludeMgr, TypeManager* pTypeMgr);
      InitializationGen() = delete;
      void add_init_of(std::vector<std::tuple<std::string, std::string, SgType*>>& varPairs, SgStatement& addBeforeStmt, std::map<std::string, SgInitializedName*>& reqForArrayDecl, std::map<std::string, std::string>& pVarNameToSizeVar);
      void add_init_stmt(std::string baseName, SgType* type, SgStatement* pBeforeStmt, std::map<std::string, std::string>& pVarNameToSizeVar); 

    private:
      IncludeManager* inclMgr;
      PrototypeManager* protoMgr;
      TypeManager* typeMgr;
      PECHelperManager helper;
      SgStatement* beforeStmt;
      std::map<std::string, std::string>* varNameToSizeVar;
      unsigned int count;

      void add_init_and_equal_stmts(SgExpression* varS, SgExpression* varP, SgType* type, bool shouldAppend);
      void add_init_and_equal_stmts_scalar(SgExpression* varS, SgExpression* varP, SgType* type, bool shouldAppend);
      void add_init_arrays(SgExpression* varS, SgExpression* varP, SgArrayType* type, bool shouldAppend, bool onlyEqualize);
      void add_init_ptr(SgExpression* varS, SgExpression* varP, SgType* type, bool shouldAppend, bool onlyEqualize);
      void add_init_stmt(SgExpression* var, SgType* type, bool shouldAppend);
      void add_init_stmt_scalar(SgExpression* var, SgType* type,bool shouldAppend);
      void add_init_struct_initList(SgName var, std::vector<SgDeclarationStatement*>& classMembers, SgType* type, bool shouldAppend);
      bool contains_const_elems(SgClassType* type);
      void equal_stmts(SgExpression* varS, SgExpression* varP, SgType* type, bool shouldAppend);
      void equal_stmts_scalar(SgExpression* varS, SgExpression* varP, bool shouldAppend);
      void equal_struct_initList(SgExpression* varS, SgName varP, std::vector<SgDeclarationStatement*>& classMembers, SgType* type, bool shouldAppend);
      SgExpression* get_scalar_init_expr(SgType* type);
      SgAggregateInitializer* get_struct_init_initializer(std::vector<SgDeclarationStatement*>& classMembers, SgType* type);
      SgAggregateInitializer* get_struct_equal_initializer(SgExpression* varS, std::vector<SgDeclarationStatement*>& classMembers, SgType* type);


  };
#endif
