//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef PEC_AbstractChecker_H
#define PEC_AbstractChecker_H

#include "rose.h"

  class AbstractChecker{
    public:
      AbstractChecker(bool requireOutputVars);

    protected:
      bool contains_openmp_synch_stmt(SgSourceFile* parProg);
      int get_num_checks(SgSourceFile* file);
      std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*> get_start_end_pragmas(SgSourceFile* file, int checkID);
      bool missed_by_LV_analysis(SgType* type);
      bool require_output_vars();

    private:
      std::set<std::string> openmp_synch_stmts;

      std::string extract_omp_directive_name(std::string pragmaText);
      
      bool require_output_variables;

  };
#endif
