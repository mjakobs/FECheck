//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2023 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef PEC_AbstractCheckGen_H
#define PEC_AbstractCheckGen_H

/* copied from here*/
#include "AbstractChecker.h"
#include "IncludeManager.h"
#include "PrototypeManager.h"
#include "PECHelperManager.h"
#include "TypeManager.h"
#include "VariableClassification.h"
#include "rose.h"
#include <string.h> 

  class AbstractCheckGen : public AbstractChecker {
    public:
      AbstractCheckGen(bool requireOutputVars) : AbstractChecker(requireOutputVars){}
      void init_include_Mgr(SgSourceFile* seqProg, SgSourceFile* parProg);

    protected:
      IncludeManager inclMgr;
      PrototypeManager protoMgr;
      TypeManager typeMgr;
      
      void delete_tmp_checker_file(std::string& checker_file);
      void write_checker_file(SgSourceFile* file, std::string fileName, SgProject* seqProject);
         
    private:
      



  };
#endif
