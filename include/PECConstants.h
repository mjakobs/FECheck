#ifndef PEC_Consts_H
#define PEC_Consts_H

#define ompPragma "omp "

#define dummyFilePrefix "temp_dummy_file_"
#define seqFilename "temp__peqcheck_seq.c"
#define parFilename "temp_peqcheck_par.c"

#define startMarkerPrefix "scope_"
#define endMarkerPrefix "epocs_"

#define equalVar "equal"
#define turnVar "__PEC_turn"
#define prefixForCounter "_f_ct"
#define prefixForSizeVar "_size_st"
#define seqVarSuffix "_s"
#define renameFunSuffix "_s"

#define turnSeq 0
#define turnPar 1

#define vnondetPrefix "__VERIFIER_nondet_"

#define arch 64

#define NondetTypeSize 12
enum NondetType { BOOLEAN, CHAR, INT, FLOAT, DOUBLE, LONG, SHORT, UCHAR, UINT, ULONG, USHORT, UNKNOWN}; // adapt NondetTypeSize, nondetFunSuffix in PECHelperManager if change enum
enum CheckType {DOALL, GENERAL, REDUCTIONPAT, WHOLE};

enum SharingType {FIRSTLASTPRIVATE, FIRSTPRIVATE, LASTPRIVATE, PRIVATE, REDUCTION, SHARED, UNSET}; // extend this enum if one want to support more OpenMP data-sharing attributes
enum ReductionOp {AND, B_AND, B_OR, MAXIMUM, MINIMUM, MINUS, MUL, OR, PLUS, INVALID, XOR};
enum LoopType {DECREASE, INCREASE, UNDETERMINED};

#endif
