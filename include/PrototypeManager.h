//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef PEC_PrototypeManager_H
#define PEC_PrototypeManager_H

#include "rose.h"
#include "PECConstants.h"
#include "PECHelperManager.h"

class PrototypeManager {
    public: 
      PrototypeManager();
      void add_prototype(SgFunctionDeclaration* prototype);
      void add_verifier_nondet(NondetType type);
      
      void insert_function_declarations();
      void reset();

    private:
      bool addedNondets[NondetTypeSize];
      std::set<SgFunctionDeclaration*> required_prototypes;
      std::set<std::string> seenFunProtoNames;
      PECHelperManager helper;

      SgType* get_type(NondetType nondet);

  };
#endif
